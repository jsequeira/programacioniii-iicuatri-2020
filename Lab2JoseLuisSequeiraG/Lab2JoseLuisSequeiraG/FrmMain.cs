﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab2JoseLuisSequeiraG
{
    public partial class FrmMain : Form
    {

        DocXML xml = new DocXML();
        public FrmMain()
        {
            InitializeComponent();
           
            definirDgvMenu();

            definirDgvReporte();

            cargarPlatillos();
           
            CenterToScreen();

            cargarReporte();


        }

        public void cargarPlatillos() {

            xml.LeerXml(dgvPlatillos);

        }

        public void cargarReporte()
        {

            xml.reporteXml(dgvReporte);

        }


        public void definirDgvMenu(){
        
            DataTable dt = new DataTable();

            dt.Columns.Add("Nombre");
            dt.Columns.Add("Precio");
            dt.Columns.Add("Descripcion");
            dt.Columns.Add("Calorias");

            dgvPlatillos.DataSource = dt;
        }


        public void definirDgvReporte()
        {

            DataTable dt = new DataTable();

            dt.Columns.Add("Nombre");
            dt.Columns.Add("Precio");
            dt.Columns.Add("Descripcion");
            dt.Columns.Add("Calorias");

            dgvReporte.DataSource = dt;
        }


        public bool validacion(string platillo) {
            bool booleana=true;

            int filas = dgvPlatillos.RowCount;

            for (int x = 0; x < filas; x++) {

                if (dgvPlatillos.Rows[x].Cells[0].Value != null) { 

                if (dgvPlatillos.Rows[x].Cells[0].Value.ToString().ToLower() == platillo.ToLower()) {

                        MessageBox.Show("Platillo ya registrado");
                        booleana =false;

                }
            }
            
            }

            return booleana;
                    
                    }

      
        private void button1_Click(object sender, EventArgs e)
        {
            if (validacion(txtNombre.Text)==true) {

                xml.agregarDatos(txtNombre.Text,txtPrecio.Text, txtDescripcion.Text, txtCalorias.Text);

                MessageBox.Show("Registrado!!");

                dgvPlatillos.Columns.Clear();
                dgvReporte.Columns.Clear();
                definirDgvMenu();
                definirDgvReporte();
                cargarPlatillos(); 
                cargarReporte();




            }
        }
    }


    }

