﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using xmldoc;

namespace frm
{
    public partial class Form1 : Form
    {

        string strcodigo = "";
        public Form1()
        {
            InitializeComponent();

            prodata();

            new xmldoc.xmldoc().crear();
            



        }

        private void btnRegistar_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = null;

            prodata();

            xmldoc.xmldoc doc = new xmldoc.xmldoc();
            string codigo = txtCodigo.Text;
            string director = txtDirector.Text;
            string titulo = txtTitulo.Text;
            string interprete = txtInterpretes.Text;
            string genero = txtGenero.Text;
            doc.agregarDatos2(codigo, titulo, director, interprete, genero);

            doc.LeerXml(dataGridView1);

            

        }

        public void prodata()
        {

            DataTable dataTable = new DataTable();

            dataTable.Columns.Add("codigo");
            dataTable.Columns.Add("director");
            dataTable.Columns.Add("titulo");
            dataTable.Columns.Add("interprete");
            dataTable.Columns.Add("genero");

            dataGridView1.DataSource = dataTable;



        }

 
        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {

            strcodigo = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();

            mcodigo.Text = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            mdirector.Text = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            mtitulo.Text = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
            minterprete.Text = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();
            mgenero.Text = dataGridView1.Rows[e.RowIndex].Cells[4].Value.ToString();
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = null;

            prodata();

            new xmldoc.xmldoc().ModificarXml(strcodigo,mtitulo.Text,mdirector.Text,minterprete.Text,mgenero.Text);

            xmldoc.xmldoc doc = new xmldoc.xmldoc();

            doc.LeerXml(dataGridView1);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = null;

            prodata();
            new xmldoc.xmldoc().EliminarXml(strcodigo);
            xmldoc.xmldoc doc = new xmldoc.xmldoc();
            doc.LeerXml(dataGridView1);
        }

    }
}
