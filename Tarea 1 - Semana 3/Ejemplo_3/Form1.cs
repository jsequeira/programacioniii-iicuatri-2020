﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejemplo_3
{
    public partial class cbxNot3 : Form
    {
        public cbxNot3()
        {
            InitializeComponent();

            lblMsj.Text = "Cómo desea recibir el pedido(Marcar una sola)";
            lblMsj2.Text = "Cómo desea ser notificado del envío?(Marcar todas las que apliquen))";
            rbtnEnvio1.Text = "Por Correo normal(50$)";
            rbtnEnvio2.Text = "Por paqueteria normal(100$)";
            rbtnEnvio3.Text = "Por paqueteria urgente(150$)";
            chkNot1.Text = "Por email(5$)";
            chkNot2.Text = "Por telefono(15$)";
            chkNot3.Text = "Por fax(20$)";

        }

        public int calcularEnvio() {
            int total = 0;

            if (rbtnEnvio1.Checked == true)
            {
                total = total + 50;

            }
            if (rbtnEnvio2.Checked == true)
            {
                total = total + 100;

            }
            if (rbtnEnvio3.Checked == true)
            {
                total = total + 150;

            }
            if (chkNot1.Checked==true) {

                total = total + 5;
            }
            if (chkNot2.Checked == true)
            {

                total = total + 15;
            }
            if (chkNot3.Checked == true)
            {

                total = total + 20;
            }


            return total;
        }






        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void lblAnunciado2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            txtResultado.Text = calcularEnvio().ToString();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
