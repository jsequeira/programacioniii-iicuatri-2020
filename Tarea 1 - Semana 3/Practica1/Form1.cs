﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
           
            label2.Text = "";
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
      
        }


        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked)
            {
                checkBox1.Checked = false;
                label2.Text = "==> Farenheit";
            }

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                checkBox2.Checked = false;
                label2.Text = "==> Celcius";
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double total;


            if (checkBox2.Checked)
            {

                total = (double.Parse(textBox1.Text)-32) * (5.0 / 9.0);
                label2.Text = "==> " +total.ToString()+"°F";

            }

            if (checkBox1.Checked)
            {
                total = double.Parse(textBox1.Text) * (9.0/ 5.0)+32.0;

                label2.Text = "==> " +total.ToString()+"°C";
            }

           

            }
    }
}
