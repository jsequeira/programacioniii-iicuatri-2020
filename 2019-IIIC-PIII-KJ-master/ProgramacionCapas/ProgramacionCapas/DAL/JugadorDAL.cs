﻿using ProgramacionCapas.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;

namespace ProgramacionCapas.DAL
{
    public class JugadorDAL
    {
        public void Insertar(EJugador jug)
        {
            string sql = "INSERT INTO dados.jugadores(correo, nombre, apellido, pin) " +
                " VALUES(@cor, @nom, @ape, @pin) returning id";

            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@cor", jug.Correo);
                cmd.Parameters.AddWithValue("@nom", jug.Nombre);
                cmd.Parameters.AddWithValue("@ape", jug.Apellido);
                cmd.Parameters.AddWithValue("@pin", jug.Pin);
                jug.Id = (int)cmd.ExecuteScalar();

            }
        }

        public void Actualizar(EJugador jug)
        {
            string sql = "UPDATE dados.jugadores SET correo =@cor, nombre =@nom, " +
                "apellido =@ape, pin =@pin, activo =@act WHERE id =@id";

            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@cor", jug.Correo);
                cmd.Parameters.AddWithValue("@nom", jug.Nombre);
                cmd.Parameters.AddWithValue("@ape", jug.Apellido);
                cmd.Parameters.AddWithValue("@pin", jug.Pin);
                cmd.Parameters.AddWithValue("@act", jug.Activo);
                cmd.Parameters.AddWithValue("@id", jug.Id);
                cmd.ExecuteNonQuery();
            }
        }

        public bool Cargar(EJugador jug)
        {
            string sql = "SELECT id, correo, nombre, apellido, pin, activo " +
                " FROM dados.jugadores WHERE correo = @cor and pin = @pin";

            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@cor", jug.Correo);
                cmd.Parameters.AddWithValue("@pin", jug.Pin);

                NpgsqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    EJugador temp = Cargar(reader);
                    jug.Nombre = temp.Nombre;
                    jug.Apellido = temp.Apellido;
                    jug.Activo = temp.Activo;
                    jug.Id = temp.Id;
                    Console.WriteLine(jug.Nombre);
                    return true;
                }
            }
            return false;

        }

        public void RegistrarPuntaje(EJugador jug)
        {
            string sql = "INSERT INTO dados.puntajes(id_jugador, puntaje, lanzamientos) " +
               " VALUES(@idj, @pun, @lan)";

            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@idj", jug.Id);
                cmd.Parameters.AddWithValue("@pun", jug.PuntajeActual.Puntaje);
                cmd.Parameters.AddWithValue("@lan", jug.PuntajeActual.Lanzamientos);
                cmd.ExecuteNonQuery();

            }
        }

        private EJugador Cargar(NpgsqlDataReader reader)
        {
            EJugador jug = new EJugador
            {
                Id = reader.GetInt32(0),
                Correo = reader.GetString(1),
                Nombre = reader.GetString(2),
                Apellido = reader.GetString(3),
                Pin = reader.GetInt32(4),
                Activo = reader.GetBoolean(5)
            };
            return jug;
        }
    }
}
