﻿using ProgramacionCapas.DAL;
using ProgramacionCapas.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgramacionCapas.BOL
{
    public class LogicaBOL
    {

        public EJugador Actual { get; set; }

        public EJugador JugadorUno { get; set; }
        public EJugador JugadorDos { get; set; }

        public const int MAX_INT = 10;

        public void Registrar(EJugador jugador)
        {
            Validar(jugador);
            if (jugador.Id > 0)
            {
                new JugadorDAL().Actualizar(jugador);
            }
            else
            {
                new JugadorDAL().Insertar(jugador);
                Console.WriteLine(jugador.Id);
            }
        }

        private void Validar(EJugador jugador)
        {
            if (string.IsNullOrWhiteSpace(jugador.Nombre))
            {
                throw new Exception("Nombre Requerido");
            }
            if (string.IsNullOrWhiteSpace(jugador.Correo))
            {
                throw new Exception("Correo Requerido");
            }
        }

        public void Rifar()
        {
            Random r = new Random();
            int ran = r.Next(20);
            Actual = ran <= 10 ? JugadorUno : JugadorDos;
        }

        public bool CargarJugador(EJugador jugador)
        {
            //Validar correo y pin
            if (string.IsNullOrWhiteSpace(jugador.Correo))
            {
                throw new Exception("Correo Requerido");
            }
            return new JugadorDAL().Cargar(jugador);
        }

        public void Revisar(int d1, int d2, int num)
        {
            Actual.PuntajeActual.Lanzamientos++;
            int sumD = d1 + d2;
            if (num == sumD)
            {
                Actual.PuntajeActual.Puntaje += 2;
            }
            else if (num == sumD + 1 || num == sumD - 1)
            {
                Actual.PuntajeActual.Puntaje += 1;
            }

            Actual = Actual == JugadorUno ? JugadorDos : JugadorUno;

        }

        public void RegistrarPuntaje()
        {
            EJugador ganador = JugadorUno.PuntajeActual.Puntaje > JugadorDos.PuntajeActual.Puntaje ? JugadorUno : JugadorDos;
            new JugadorDAL().RegistrarPuntaje(ganador);
        }

        public bool DiferenciaAbismal()
        {
            EJugador perdiendo = JugadorUno.PuntajeActual.Puntaje < JugadorDos.PuntajeActual.Puntaje ? JugadorUno : JugadorDos;
            EJugador ganando = perdiendo == JugadorUno ? JugadorDos : JugadorUno;
            int pun = ((MAX_INT - perdiendo.PuntajeActual.Lanzamientos) * 2) + perdiendo.PuntajeActual.Puntaje;
            return pun < ganando.PuntajeActual.Puntaje;
        }
    }
}
