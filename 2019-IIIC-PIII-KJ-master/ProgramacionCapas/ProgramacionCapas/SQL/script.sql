drop table dados.jugadores;

CREATE TABLE dados.jugadores
(
    id serial PRIMARY KEY,
    correo text NOT NULL UNIQUE,
    nombre text NOT NULL,
    apellido text,
    pin integer NOT NULL,
    activo boolean DEFAULT true
);

select * from dados.jugadores

drop table dados.puntajes

CREATE TABLE dados.puntajes
(
	id serial primary key,
	id_jugador int not null, 
	puntaje int default 0, 
	lanzamientos int default 0, 
	fecha timestamp default now()
)

alter table dados.puntajes add constraint fk_pun_jug foreign key(id_jugador)
references dados.jugadores(id)

insert into dados.puntajes(id_jugador, puntaje, lanzamientos) values (1, 50, 10)

select * from  dados.puntajes






