﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgramacionCapas.Entities
{
    public class EPuntaje
    {
        public int Id { get; set; }
        public int Puntaje { get; set; }
        public int Lanzamientos { get; set; }
        public DateTime Fecha { get; set; }
    }
}
