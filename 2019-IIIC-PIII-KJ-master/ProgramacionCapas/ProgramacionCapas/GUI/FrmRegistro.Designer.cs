﻿namespace ProgramacionCapas.GUI
{
    partial class FrmRegistro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnAceptarUno = new System.Windows.Forms.Button();
            this.txtPin1 = new System.Windows.Forms.MaskedTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtApellido1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCorreo1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNombre1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnAceptarDos = new System.Windows.Forms.Button();
            this.txtPin2 = new System.Windows.Forms.MaskedTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtApellido2 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtCorreo2 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtNombre2 = new System.Windows.Forms.TextBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btnCargarUno = new System.Windows.Forms.Button();
            this.btnCargarDos = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.groupBox1.Controls.Add(this.btnCargarUno);
            this.groupBox1.Controls.Add(this.btnAceptarUno);
            this.groupBox1.Controls.Add(this.txtPin1);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtApellido1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtCorreo1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtNombre1);
            this.groupBox1.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(41, 88);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(6);
            this.groupBox1.Size = new System.Drawing.Size(319, 463);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Jugador #1";
            // 
            // btnAceptarUno
            // 
            this.btnAceptarUno.Location = new System.Drawing.Point(146, 410);
            this.btnAceptarUno.Name = "btnAceptarUno";
            this.btnAceptarUno.Size = new System.Drawing.Size(102, 44);
            this.btnAceptarUno.TabIndex = 10;
            this.btnAceptarUno.Text = "Aceptar";
            this.btnAceptarUno.UseVisualStyleBackColor = true;
            this.btnAceptarUno.Click += new System.EventHandler(this.Button1_Click);
            // 
            // txtPin1
            // 
            this.txtPin1.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPin1.Location = new System.Drawing.Point(32, 359);
            this.txtPin1.Mask = "####";
            this.txtPin1.Name = "txtPin1";
            this.txtPin1.Size = new System.Drawing.Size(216, 36);
            this.txtPin1.TabIndex = 9;
            this.txtPin1.TextChanged += new System.EventHandler(this.TxtPin1_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(26, 317);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 29);
            this.label5.TabIndex = 8;
            this.label5.Text = "PIN";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(26, 226);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 29);
            this.label4.TabIndex = 6;
            this.label4.Text = "Apellido";
            // 
            // txtApellido1
            // 
            this.txtApellido1.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtApellido1.Location = new System.Drawing.Point(26, 268);
            this.txtApellido1.Name = "txtApellido1";
            this.txtApellido1.Size = new System.Drawing.Size(222, 36);
            this.txtApellido1.TabIndex = 3;
            this.txtApellido1.TextChanged += new System.EventHandler(this.TxtPin1_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(31, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 29);
            this.label3.TabIndex = 4;
            this.label3.Text = "Correo";
            // 
            // txtCorreo1
            // 
            this.txtCorreo1.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCorreo1.Location = new System.Drawing.Point(31, 86);
            this.txtCorreo1.Name = "txtCorreo1";
            this.txtCorreo1.Size = new System.Drawing.Size(222, 36);
            this.txtCorreo1.TabIndex = 1;
            this.txtCorreo1.TextChanged += new System.EventHandler(this.TxtPin1_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 135);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 29);
            this.label2.TabIndex = 2;
            this.label2.Text = "Nombre";
            // 
            // txtNombre1
            // 
            this.txtNombre1.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombre1.Location = new System.Drawing.Point(31, 177);
            this.txtNombre1.Name = "txtNombre1";
            this.txtNombre1.Size = new System.Drawing.Size(222, 36);
            this.txtNombre1.TabIndex = 2;
            this.txtNombre1.TextChanged += new System.EventHandler(this.TxtPin1_TextChanged);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label1.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(-1, 23);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(785, 45);
            this.label1.TabIndex = 1;
            this.label1.Text = "DADOS - UTN v0.1";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.groupBox2.Controls.Add(this.btnCargarDos);
            this.groupBox2.Controls.Add(this.btnAceptarDos);
            this.groupBox2.Controls.Add(this.txtPin2);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.txtApellido2);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.txtCorreo2);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.txtNombre2);
            this.groupBox2.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(409, 88);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(6);
            this.groupBox2.Size = new System.Drawing.Size(319, 463);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Jugador #2";
            // 
            // btnAceptarDos
            // 
            this.btnAceptarDos.Location = new System.Drawing.Point(166, 413);
            this.btnAceptarDos.Name = "btnAceptarDos";
            this.btnAceptarDos.Size = new System.Drawing.Size(102, 44);
            this.btnAceptarDos.TabIndex = 18;
            this.btnAceptarDos.Text = "Aceptar";
            this.btnAceptarDos.UseVisualStyleBackColor = true;
            this.btnAceptarDos.Click += new System.EventHandler(this.Button2_Click);
            // 
            // txtPin2
            // 
            this.txtPin2.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPin2.Location = new System.Drawing.Point(52, 366);
            this.txtPin2.Mask = "####";
            this.txtPin2.Name = "txtPin2";
            this.txtPin2.Size = new System.Drawing.Size(216, 36);
            this.txtPin2.TabIndex = 17;
            this.txtPin2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPin2.TextChanged += new System.EventHandler(this.TxtPin2_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(52, 324);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(48, 29);
            this.label6.TabIndex = 16;
            this.label6.Text = "PIN";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(46, 233);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(97, 29);
            this.label7.TabIndex = 15;
            this.label7.Text = "Apellido";
            // 
            // txtApellido2
            // 
            this.txtApellido2.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtApellido2.Location = new System.Drawing.Point(46, 275);
            this.txtApellido2.Name = "txtApellido2";
            this.txtApellido2.Size = new System.Drawing.Size(222, 36);
            this.txtApellido2.TabIndex = 13;
            this.txtApellido2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtApellido2.TextChanged += new System.EventHandler(this.TxtPin2_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(47, 44);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(82, 29);
            this.label8.TabIndex = 14;
            this.label8.Text = "Correo";
            // 
            // txtCorreo2
            // 
            this.txtCorreo2.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCorreo2.Location = new System.Drawing.Point(46, 93);
            this.txtCorreo2.Name = "txtCorreo2";
            this.txtCorreo2.Size = new System.Drawing.Size(222, 36);
            this.txtCorreo2.TabIndex = 10;
            this.txtCorreo2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCorreo2.TextChanged += new System.EventHandler(this.TxtPin2_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(47, 135);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(96, 29);
            this.label9.TabIndex = 11;
            this.label9.Text = "Nombre";
            // 
            // txtNombre2
            // 
            this.txtNombre2.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombre2.Location = new System.Drawing.Point(46, 184);
            this.txtNombre2.Name = "txtNombre2";
            this.txtNombre2.Size = new System.Drawing.Size(222, 36);
            this.txtNombre2.TabIndex = 12;
            this.txtNombre2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNombre2.TextChanged += new System.EventHandler(this.TxtPin2_TextChanged);
            // 
            // toolTip1
            // 
            this.toolTip1.IsBalloon = true;
            this.toolTip1.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.toolTip1.ToolTipTitle = "Test";
            // 
            // btnCargarUno
            // 
            this.btnCargarUno.Location = new System.Drawing.Point(32, 410);
            this.btnCargarUno.Name = "btnCargarUno";
            this.btnCargarUno.Size = new System.Drawing.Size(102, 44);
            this.btnCargarUno.TabIndex = 11;
            this.btnCargarUno.Text = "Cargar";
            this.btnCargarUno.UseVisualStyleBackColor = true;
            this.btnCargarUno.Click += new System.EventHandler(this.BtnCargarUno_Click);
            // 
            // btnCargarDos
            // 
            this.btnCargarDos.Location = new System.Drawing.Point(51, 413);
            this.btnCargarDos.Name = "btnCargarDos";
            this.btnCargarDos.Size = new System.Drawing.Size(102, 44);
            this.btnCargarDos.TabIndex = 19;
            this.btnCargarDos.Text = "Cargar";
            this.btnCargarDos.UseVisualStyleBackColor = true;
            this.btnCargarDos.Click += new System.EventHandler(this.BtnCargarDos_Click);
            // 
            // FrmRegistro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(17F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 566);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "FrmRegistro";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmRegistro";
            this.Load += new System.EventHandler(this.FrmRegistro_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtNombre1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtCorreo1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtApellido1;
        private System.Windows.Forms.MaskedTextBox txtPin1;
        private System.Windows.Forms.MaskedTextBox txtPin2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtApellido2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtCorreo2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtNombre2;
        private System.Windows.Forms.Button btnAceptarUno;
        private System.Windows.Forms.Button btnAceptarDos;
        private System.Windows.Forms.Button btnCargarUno;
        private System.Windows.Forms.Button btnCargarDos;
    }
}