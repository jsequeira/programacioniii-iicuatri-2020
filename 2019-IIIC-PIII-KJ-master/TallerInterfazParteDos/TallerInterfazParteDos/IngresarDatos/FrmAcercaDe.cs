﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TallerInterfazParteDos.IngresarDatos
{
    public partial class FrmAcercaDe : Form
    {
        public FrmAcercaDe()
        {
            InitializeComponent();
        }

        public String Nombre
        {
            set { lblNombre.Text = value; }
        }
        public String Correo
        {
            set { lblCorreo.Text = value; }
        }
    }
}
