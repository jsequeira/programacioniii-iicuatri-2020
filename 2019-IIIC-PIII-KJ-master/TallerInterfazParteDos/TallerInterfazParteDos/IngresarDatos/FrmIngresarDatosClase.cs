﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TallerInterfazParteDos.IngresarDatos
{
    public partial class FrmIngresarDatosClase : Form
    {
        public Persona Persona { get; set; }
        public FrmIngresarDatosClase()
        {
            InitializeComponent();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            Persona.Correo = txtCorreo.Text.Trim();
            Persona.Telefono = txtTele.Text.Trim();
            Persona.Provincia = txtProvincia.Text.Trim();
            Persona.Canton = txtCanton.Text.Trim();
            Persona.Distrito = txtDistrito.Text.Trim();
            Persona.CodigoPostal = Convert.ToInt32(txtCodigo.Text.Trim());
            Close();
        }
    }
}
