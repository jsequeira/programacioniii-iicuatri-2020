﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TallerInterfazParteDos.IngresarDatos;
using TallerInterfazParteDos.Matrices;

namespace TallerInterfazParteDos
{
    public partial class FrmMenu : Form
    {
        public FrmMenu()
        {
            InitializeComponent();
        }

        private void IngresoDeDatosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmPrincipal frm = new FrmPrincipal();
            frm.Owner = this;
            Hide();
            frm.Show();
        }

        private void MatricesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmMatriz frm = new FrmMatriz();
            Hide();
            frm.ShowDialog();
            Show();
        }
    }
}
