﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TallerInterfazParteDos.Matrices
{
    public partial class FrmMatriz : Form
    {
        private int dim;
        private int[,] m1;
        private int[,] m2;


        public FrmMatriz()
        {
            InitializeComponent();
        }

        private void FrmMatriz_Load(object sender, EventArgs e)
        {

        }

        private void TxtCorreo_TextChanged(object sender, EventArgs e)
        {
            btnGenerar.Enabled = false;
            btnSumar.Enabled = false;
            if (!String.IsNullOrEmpty(txtDim.Text.Trim()))
            {
                if (!Int32.TryParse(txtDim.Text.Trim(), out dim))
                {
                    txtDim.Clear();
                }
                else
                {
                    btnGenerar.Enabled = true;
                }
            }
        }

        private void BtnGenerar_Click(object sender, EventArgs e)
        {
            dgvTabla1.Columns.Clear();
            dgvTabla2.Columns.Clear();
            dgvResultado.Columns.Clear();

            int col = 0;
            while (col < dim)
            {
                DataGridViewColumn column = new DataGridViewColumn(new DataGridViewTextBoxCell());
                column.Width = 25;
                dgvTabla1.Columns.Add(column);

                DataGridViewColumn column2 = new DataGridViewColumn(new DataGridViewTextBoxCell());
                column2.Width = 25;
                dgvTabla2.Columns.Add(column2);

                DataGridViewColumn column3 = new DataGridViewColumn(new DataGridViewTextBoxCell());
                column3.Width = 25;
                dgvResultado.Columns.Add(column3);
                col++;
            }
            m1 = new int[dim, dim];
            m2 = new int[dim, dim];
            int[,] m3 = new int[dim, dim];

            dgvTabla1.Rows.Add(dim);
            dgvTabla2.Rows.Add(dim);
            dgvResultado.Rows.Add(dim);

            Random r = new Random();

            for (int f = 0; f < m1.GetLength(0); f++)
            {
                for (int c = 0; c < m1.GetLength(1); c++)
                {
                    m1[f, c] = r.Next(10);
                    m2[f, c] = r.Next(10);
                    //m3[f, c] = 0;
                    dgvTabla1[c, f].Value = m1[f, c];
                    dgvTabla2[c, f].Value = m2[f, c];
                    dgvResultado[c, f].Value = m3[f, c];

                }
            }
            btnSumar.Enabled = true;
        }

        private void BtnSumar_Click(object sender, EventArgs e)
        {
            for (int f = 0; f < m1.GetLength(0); f++)
            {
                for (int c = 0; c < m1.GetLength(1); c++)
                {
                    dgvResultado[c, f].Value = m1[f, c] + m2[f, c];
                }
            }
        }

        private void DgvTabla1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvTabla1.SelectedCells.Count > 0)
            {
                int row = dgvTabla1.SelectedCells[0].RowIndex;
                int col = dgvTabla1.SelectedCells[0].ColumnIndex;

                dgvTabla2.ClearSelection();
                dgvResultado.ClearSelection();

                dgvTabla2[col, row].Selected = true;
                dgvResultado[col, row].Selected = true;
            }

        }

        private void CambiarColor(object sender, EventArgs e)
        {
            foreach (ToolStripMenuItem x in fondoToolStripMenuItem.DropDownItems)
            {
                Console.WriteLine(x.Checked);
                Console.WriteLine(x.Name);
            }

            ToolStripMenuItem item = (ToolStripMenuItem)sender;

            if (item.Tag.ToString() == "Gris" && item.Checked)
            {
                BackColor = SystemColors.Control;
                menuStrip1.BackColor = SystemColors.Control;
            }
            else
            {
                tsiGris.Checked = false;
            }

            if (item.Tag.ToString() == "Amarillo" && item.Checked)
            {
                BackColor = SystemColors.Info;
                menuStrip1.BackColor = SystemColors.Info;
                Console.WriteLine("aqui xD");
            }
            else
            {
                tsiAmarillo.Checked = false;
            }
        }
    }
}
