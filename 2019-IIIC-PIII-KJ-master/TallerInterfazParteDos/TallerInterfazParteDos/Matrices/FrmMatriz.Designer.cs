﻿namespace TallerInterfazParteDos.Matrices
{
    partial class FrmMatriz
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dgvTabla1 = new System.Windows.Forms.DataGridView();
            this.dgvTabla2 = new System.Windows.Forms.DataGridView();
            this.dgvResultado = new System.Windows.Forms.DataGridView();
            this.txtDim = new System.Windows.Forms.TextBox();
            this.btnGenerar = new System.Windows.Forms.Button();
            this.btnSumar = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fondoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsiGris = new System.Windows.Forms.ToolStripMenuItem();
            this.tsiAmarillo = new System.Windows.Forms.ToolStripMenuItem();
            this.tsiAzul = new System.Windows.Forms.ToolStripMenuItem();
            this.tsiRojo = new System.Windows.Forms.ToolStripMenuItem();
            this.tsiMorado = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTabla1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTabla2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvResultado)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(344, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(119, 29);
            this.label6.TabIndex = 42;
            this.label6.Text = "Dimensión";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(90, 107);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 29);
            this.label1.TabIndex = 43;
            this.label1.Text = "Matriz 1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(356, 107);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 29);
            this.label2.TabIndex = 44;
            this.label2.Text = "Matriz 2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(575, 107);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(188, 29);
            this.label3.TabIndex = 45;
            this.label3.Text = "Matriz Resultante";
            // 
            // dgvTabla1
            // 
            this.dgvTabla1.AllowUserToAddRows = false;
            this.dgvTabla1.AllowUserToDeleteRows = false;
            this.dgvTabla1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTabla1.Location = new System.Drawing.Point(12, 147);
            this.dgvTabla1.Name = "dgvTabla1";
            this.dgvTabla1.ReadOnly = true;
            this.dgvTabla1.RowHeadersWidth = 51;
            this.dgvTabla1.RowTemplate.Height = 24;
            this.dgvTabla1.Size = new System.Drawing.Size(250, 250);
            this.dgvTabla1.TabIndex = 46;
            this.dgvTabla1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvTabla1_CellClick);
            // 
            // dgvTabla2
            // 
            this.dgvTabla2.AllowUserToAddRows = false;
            this.dgvTabla2.AllowUserToDeleteRows = false;
            this.dgvTabla2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTabla2.Location = new System.Drawing.Point(278, 147);
            this.dgvTabla2.Name = "dgvTabla2";
            this.dgvTabla2.ReadOnly = true;
            this.dgvTabla2.RowHeadersWidth = 51;
            this.dgvTabla2.RowTemplate.Height = 24;
            this.dgvTabla2.Size = new System.Drawing.Size(250, 250);
            this.dgvTabla2.TabIndex = 47;
            // 
            // dgvResultado
            // 
            this.dgvResultado.AllowUserToAddRows = false;
            this.dgvResultado.AllowUserToDeleteRows = false;
            this.dgvResultado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvResultado.Location = new System.Drawing.Point(544, 147);
            this.dgvResultado.Name = "dgvResultado";
            this.dgvResultado.ReadOnly = true;
            this.dgvResultado.RowHeadersWidth = 51;
            this.dgvResultado.RowTemplate.Height = 24;
            this.dgvResultado.Size = new System.Drawing.Size(250, 250);
            this.dgvResultado.TabIndex = 48;
            // 
            // txtDim
            // 
            this.txtDim.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDim.Location = new System.Drawing.Point(366, 56);
            this.txtDim.Name = "txtDim";
            this.txtDim.Size = new System.Drawing.Size(75, 36);
            this.txtDim.TabIndex = 49;
            this.txtDim.TextChanged += new System.EventHandler(this.TxtCorreo_TextChanged);
            // 
            // btnGenerar
            // 
            this.btnGenerar.Enabled = false;
            this.btnGenerar.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenerar.Location = new System.Drawing.Point(79, 24);
            this.btnGenerar.Name = "btnGenerar";
            this.btnGenerar.Size = new System.Drawing.Size(116, 65);
            this.btnGenerar.TabIndex = 50;
            this.btnGenerar.Text = "Generar";
            this.btnGenerar.UseVisualStyleBackColor = true;
            this.btnGenerar.Click += new System.EventHandler(this.BtnGenerar_Click);
            // 
            // btnSumar
            // 
            this.btnSumar.Enabled = false;
            this.btnSumar.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSumar.Location = new System.Drawing.Point(617, 24);
            this.btnSumar.Name = "btnSumar";
            this.btnSumar.Size = new System.Drawing.Size(104, 65);
            this.btnSumar.TabIndex = 51;
            this.btnSumar.Text = "Sumar";
            this.btnSumar.UseVisualStyleBackColor = true;
            this.btnSumar.Click += new System.EventHandler(this.BtnSumar_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fondoToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(806, 28);
            this.menuStrip1.TabIndex = 52;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fondoToolStripMenuItem
            // 
            this.fondoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsiGris,
            this.tsiAmarillo,
            this.tsiAzul,
            this.tsiRojo,
            this.tsiMorado});
            this.fondoToolStripMenuItem.Name = "fondoToolStripMenuItem";
            this.fondoToolStripMenuItem.Size = new System.Drawing.Size(65, 24);
            this.fondoToolStripMenuItem.Text = "Fondo";
            // 
            // tsiGris
            // 
            this.tsiGris.Checked = true;
            this.tsiGris.CheckOnClick = true;
            this.tsiGris.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsiGris.Name = "tsiGris";
            this.tsiGris.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.G)));
            this.tsiGris.Size = new System.Drawing.Size(224, 26);
            this.tsiGris.Tag = "Gris";
            this.tsiGris.Text = "Gris";
            this.tsiGris.Click += new System.EventHandler(this.CambiarColor);
            // 
            // tsiAmarillo
            // 
            this.tsiAmarillo.CheckOnClick = true;
            this.tsiAmarillo.Name = "tsiAmarillo";
            this.tsiAmarillo.Size = new System.Drawing.Size(224, 26);
            this.tsiAmarillo.Tag = "Amarillo";
            this.tsiAmarillo.Text = "Amarillo";
            this.tsiAmarillo.Click += new System.EventHandler(this.CambiarColor);
            // 
            // tsiAzul
            // 
            this.tsiAzul.CheckOnClick = true;
            this.tsiAzul.Name = "tsiAzul";
            this.tsiAzul.Size = new System.Drawing.Size(224, 26);
            this.tsiAzul.Text = "Azul";
            this.tsiAzul.Click += new System.EventHandler(this.CambiarColor);
            // 
            // tsiRojo
            // 
            this.tsiRojo.CheckOnClick = true;
            this.tsiRojo.Name = "tsiRojo";
            this.tsiRojo.Size = new System.Drawing.Size(224, 26);
            this.tsiRojo.Text = "Rojo";
            this.tsiRojo.Click += new System.EventHandler(this.CambiarColor);
            // 
            // tsiMorado
            // 
            this.tsiMorado.CheckOnClick = true;
            this.tsiMorado.Name = "tsiMorado";
            this.tsiMorado.Size = new System.Drawing.Size(224, 26);
            this.tsiMorado.Text = "Morado";
            this.tsiMorado.Click += new System.EventHandler(this.CambiarColor);
            // 
            // FrmMatriz
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(806, 413);
            this.Controls.Add(this.btnSumar);
            this.Controls.Add(this.btnGenerar);
            this.Controls.Add(this.txtDim);
            this.Controls.Add(this.dgvResultado);
            this.Controls.Add(this.dgvTabla2);
            this.Controls.Add(this.dgvTabla1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FrmMatriz";
            this.Text = "FrmMatriz";
            this.Load += new System.EventHandler(this.FrmMatriz_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTabla1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTabla2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvResultado)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dgvTabla1;
        private System.Windows.Forms.DataGridView dgvTabla2;
        private System.Windows.Forms.DataGridView dgvResultado;
        private System.Windows.Forms.TextBox txtDim;
        private System.Windows.Forms.Button btnGenerar;
        private System.Windows.Forms.Button btnSumar;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fondoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsiGris;
        private System.Windows.Forms.ToolStripMenuItem tsiAmarillo;
        private System.Windows.Forms.ToolStripMenuItem tsiAzul;
        private System.Windows.Forms.ToolStripMenuItem tsiRojo;
        private System.Windows.Forms.ToolStripMenuItem tsiMorado;
    }
}