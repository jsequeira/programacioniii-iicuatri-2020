﻿using Reproductor.BOL;
using Reproductor.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reproductor
{
    public partial class FrmAgregar : Form
    {
        public FrmAgregar()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MemoryStream ms = new MemoryStream();
            string url = @"d:\skillet-hero-official-video.mp3";

            using (FileStream file = new FileStream(url, FileMode.Open, FileAccess.Read))
            {
                file.CopyTo(ms);
            }

            ECancion can = new ECancion
            {
                Numero = 1,
                Cancion = "Hero",
                Album = "Feel Invincible",
                Artista = "Skillet",
                Categoria = "Rock",
                Datos = ms.ToArray()
            };

            CancionBOL bol = new CancionBOL();
            bol.Guardar(can);

        }
    }
}
