﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AxWMPLib;
using Reproductor.BOL;
using Reproductor.Entities;
using WMPLib;

namespace Reproductor
{
    public partial class Form1 : Form
    {
        private WindowsMediaPlayer player;
        private CancionBOL bol;
        private bool pausa;
        private List<ECancion> listaReproduccion;
        List<ECancion> canciones;

        public Form1()
        {
            InitializeComponent();
            bol = new CancionBOL();
            player = new WMPLib.WindowsMediaPlayer();
            listaReproduccion = new List<ECancion>();
            canciones = new List<ECancion>();
            Config();
        }

        private void Config()
        {
            player.PlayStateChange +=
                new WMPLib._WMPOCXEvents_PlayStateChangeEventHandler(Player_PlayStateChange);
            player.MediaError +=
                new WMPLib._WMPOCXEvents_MediaErrorEventHandler(Player_MediaError);
        }

        private void PlayFile(String url)
        {
            player.URL = url;
            player.controls.play();
        }

        private void Player_PlayStateChange(int NewState)
        {
            if ((WMPLib.WMPPlayState)NewState == WMPLib.WMPPlayState.wmppsStopped)
            {
                Close();
            }
        }

        private void Player_MediaError(object pMediaObject)
        {
            MessageBox.Show("Cannot play media file.");
            Close();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ECancion filtro = new ECancion();
            canciones = bol.Cargar(filtro);
            CrearTree();
            lbxCanciones.DataSource = listaReproduccion;
        }

        private void CrearTree()
        {
            TreeNode n1 = new TreeNode("Categorías");
            treeView1.Nodes.Add(n1);
            TreeNode cat = new TreeNode();
            TreeNode art = new TreeNode();
            TreeNode alb = new TreeNode();
            TreeNode can = new TreeNode();
            String catAct = "";
            String artAct = "";
            String albAct = "";
            String canAct = "";



            foreach (var item in canciones)
            {
                if (!catAct.Equals(item.Categoria.Trim()))
                {
                    catAct = item.Categoria;
                    cat = new TreeNode(item.Categoria);
                    n1.Nodes.Add(cat);
                }

                if (!artAct.Equals(item.Artista.Trim()))
                {
                    artAct = item.Artista;
                    art = new TreeNode(item.Artista);
                    cat.Nodes.Add(art);
                }

                if (!albAct.Equals(item.Album.Trim()))
                {
                    albAct = item.Album;
                    alb = new TreeNode(item.Album);
                    art.Nodes.Add(alb);
                }

                if (!canAct.Equals(item.Cancion.Trim()))
                {
                    canAct = item.Cancion;
                    can = new TreeNode(item.Cancion);
                    alb.Nodes.Add(can);
                    can.Tag = item;
                }

            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            FrmAgregar frm = new FrmAgregar();
            frm.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Play();

        }

        private void Play()
        {
            timer1.Start();
            if (pausa)
            {
                player.controls.play();
            }
            else
            {
                player.close();
                ECancion cancion = lbxCanciones.SelectedItem as ECancion;
                if (cancion != null)
                {
                    MemoryStream ms = new MemoryStream(cancion.Datos);
                    using (FileStream file = new FileStream("d:/temp.mp3", FileMode.Create, FileAccess.Write))
                    {
                        ms.WriteTo(file);
                    }

                    PlayFile(@"d:/temp.mp3");
                }
            }
            pausa = false;
            button2.Enabled = pausa;
            button3.Enabled = !pausa;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            player.controls.pause();
            pausa = true;
            button2.Enabled = pausa;
            button3.Enabled = !pausa;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Stop();
        }

        private void Stop()
        {
            player.controls.pause();
            pausa = false;
            button2.Enabled = !pausa;
            button3.Enabled = pausa;
            timer1.Stop();
            progressBar1.Value = 0;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Stop();
            lbxCanciones.SelectedIndex = lbxCanciones.Items.Count - 1 == lbxCanciones.SelectedIndex ?
                0 : lbxCanciones.SelectedIndex + 1;
            Play();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Stop();
            lbxCanciones.SelectedIndex = lbxCanciones.SelectedIndex == 0 ?
                 lbxCanciones.Items.Count - 1
                : lbxCanciones.SelectedIndex - 1;
            Play();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            progressBar1.Maximum = (int)Math.Round(player.controls.currentItem.duration, 0);
            progressBar1.Value = (int)Math.Round(player.controls.currentPosition, 0);

        }

        private void button7_Click(object sender, EventArgs e)
        {
            lbxCanciones.DataSource = null;
            if (treeView1.SelectedNode.Tag != null && treeView1.SelectedNode.Tag is ECancion)
            {
                listaReproduccion.Add(treeView1.SelectedNode.Tag as ECancion);
            }
            else
            {
                string textoSeleccionado = treeView1.SelectedNode.Text;
                foreach (var item in canciones)
                {
                    if (item.Categoria.Equals(textoSeleccionado) || item.Artista.Equals(textoSeleccionado))
                    {
                        listaReproduccion.Add(item);
                    }
                }
            }
            lbxCanciones.DataSource = listaReproduccion;

        }
    }
}
