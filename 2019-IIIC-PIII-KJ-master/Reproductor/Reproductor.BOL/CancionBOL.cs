﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Reproductor.DAL;
using Reproductor.Entities;

namespace Reproductor.BOL
{
    public class CancionBOL
    {
        public void Guardar(ECancion can)
        {
            //TODO: Validar requerido
            new CancionDAL().Insertar(can);
        }

        public List<ECancion> Cargar(ECancion filtro)
        {
           return new CancionDAL().Cargar(filtro);
        }
    }
}
