﻿using System.Drawing;

namespace VisorImagenes.Entities
{
    public class EImagen
    {
        public int Id { get; set; }
        public Image Imagen { get; set; }
        public string Titulo { get; set; }
        public string Descripcion { get; set; }
        public string Tipo { get; set; }

    }
}
