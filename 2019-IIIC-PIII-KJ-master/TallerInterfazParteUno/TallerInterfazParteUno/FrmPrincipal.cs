﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TallerInterfazParteUno
{
    public partial class FrmPrincipal : Form
    {
        public FrmPrincipal()
        {
            InitializeComponent();
        }

        private void MultiplicarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form1 frm = new Form1();
            frm.ShowDialog();
        }

        private void TemperaturaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmTemp frm = new FrmTemp();
            Hide();
            frm.ShowDialog();
            Console.WriteLine("Test 2");
            Show();

        }

        private void MessageBoxToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Mensajeria frm = new Mensajeria();
            frm.Owner = this;
            Hide();
            frm.Show();
            Console.WriteLine("Test 1");
        }
    }
}
