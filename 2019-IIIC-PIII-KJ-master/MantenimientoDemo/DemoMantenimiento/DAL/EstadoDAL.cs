﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MantenimientoDemo.Entities;
using Npgsql;

namespace DemoMantenimiento.DAL
{
    class EstadoDAL
    {
        public EEstado CargarEstado(int id)
        {
            string sql = "select id, estado, activo, id_genero from ganaderia.estados where id = @id";

            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", id);

                NpgsqlDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    return CargarEstado(reader);
                }
            }
            return null;

        }

        public List<EEstado> CargarTodo(EGenero gen)
        {

            List<EEstado> estados = new List<EEstado>();

            string sql = "select id, estado, activo, id_genero from ganaderia.estados where id_genero=@idg";

            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@idg", gen.Id);

                NpgsqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    estados.Add(CargarEstado(reader));
                }
            }
            return estados;
        }

        private EEstado CargarEstado(NpgsqlDataReader reader)
        {
            
            EEstado e = new EEstado
            {
                Id = reader.GetInt32(0),
                Estado= reader.GetString(1),
                Activo = reader.GetBoolean(2),
                Genero = new GeneroDAL().CargarGenero(reader.GetInt32(3))
            };
            return e;
        }

    }
}
