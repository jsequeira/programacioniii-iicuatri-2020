﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MantenimientoDemo.Entities;
using Npgsql;

namespace DemoMantenimiento.DAL
{
    public class BovinoDAL
    {
        public List<EBovino> CargarBovinos(string filtro)
        {
            List<EBovino> bovinos = new List<EBovino>();
            string sql = "select id, numero, id_bovino, id_ganaderia, fecha_nacimiento, id_imagen, id_genero, id_estado, activo" +
                " from ganaderia.bovinos";
            sql += !String.IsNullOrEmpty(filtro) ? " where text(numero) like @fil" : "";

            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                if (!String.IsNullOrEmpty(filtro))
                {
                    cmd.Parameters.AddWithValue("@fil", "%" + filtro + "%");
                }
                NpgsqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    bovinos.Add(CargarBovinos(reader));
                }
            }
            return bovinos;
        }

        public void Modificar(EBovino b)
        {
            try
            {

                string sql = "UPDATE ganaderia.bovinos  SET numero = @num, id_bovino = @bov, id_ganaderia = @gan, " +
                    " fecha_nacimiento = @fec, id_imagen = @ima, id_genero = @gen, id_estado = @est, activo = @act " +
                    " WHERE id = @id; ";

                using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
                {
                    con.Open();
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                    cmd.Parameters.AddWithValue("@num", b.Numero);

                    if (b.Trazabilidad != null)
                    {
                        cmd.Parameters.AddWithValue("@bov", b.Trazabilidad.Id);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@bov", DBNull.Value);
                    }
                    cmd.Parameters.AddWithValue("@gan", b.Ganaderia.Id);
                    cmd.Parameters.AddWithValue("@fec", b.FechaNacimiento);
                    ImagenDAL idal = new ImagenDAL();
                    cmd.Parameters.AddWithValue("@ima", idal.InsertarImagen(b.Foto));
                    cmd.Parameters.AddWithValue("@gen", b.Genero.Id);
                    cmd.Parameters.AddWithValue("@est", b.Estado.Id);
                    cmd.Parameters.AddWithValue("@act", b.Activo);
                    cmd.Parameters.AddWithValue("@id", b. Id);

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("bovinos_numero_key"))
                {
                    throw new Exception("Número de bovino duplicado");
                }
                throw;
            }
        }

        public void Insertar(EBovino b)
        {
            NpgsqlTransaction tran = null;

            try
            {
                string sql = "insert into ganaderia.bovinos(numero, id_ganaderia, fecha_nacimiento, id_imagen, id_genero, id_estado, activo, id_bovino) values" +
                    " (@num, @gan, @fec, @img, @gen, @est, @act, @bov) ";

                using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
                {
                    con.Open();
                    tran = con.BeginTransaction();
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, con, tran);
                    cmd.Parameters.AddWithValue("@num", b.Numero);

                    if (b.Trazabilidad != null)
                    {
                        cmd.Parameters.AddWithValue("@bov", b.Trazabilidad.Id);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@bov", DBNull.Value);
                    }
                    cmd.Parameters.AddWithValue("@gan", b.Ganaderia.Id);
                    cmd.Parameters.AddWithValue("@fec", b.FechaNacimiento);
                    cmd.Parameters.AddWithValue("@img", new ImagenDAL().InsertarImagen(b.Foto,con, tran));
                    cmd.Parameters.AddWithValue("@gen", b.Genero.Id);
                    cmd.Parameters.AddWithValue("@est", b.Estado.Id);
                    cmd.Parameters.AddWithValue("@act", b.Activo);
                    cmd.ExecuteNonQuery();
                    tran.Commit();
                }
            }
            catch (Exception ex)
            {
                try
                {
                    if (tran != null)
                    {
                        tran.Rollback();
                    }
                }
                catch (Exception)
                { }
                
                if (ex.Message.Contains("bovinos_numero_key")) 
                {
                    throw new Exception("Número de bovino duplicado");
                }
                throw;
            }
        }

        private EBovino CargarBovinos(NpgsqlDataReader reader)
        {
            //id, numero, id_bovino, id_ganaderia, fecha_nacimiento, id_imagen, id_genero, id_estado, activo
            EBovino temp = new EBovino
            {
                Id = reader.GetInt32(0),
                Numero = reader.GetInt32(1),
                Trazabilidad = !reader.IsDBNull(2) ? CargarBovinos(reader.GetInt32(2)) : null,
                Ganaderia = !reader.IsDBNull(3) ? new GanaderiaDAL().CargarGanaderia(reader.GetInt32(3)) : null,
                FechaNacimiento = reader.GetDateTime(4),
                Foto = !reader.IsDBNull(5) ? new ImagenDAL().CargarImagen(reader.GetInt32(5)) : null,
                Genero = !reader.IsDBNull(6) ? new GeneroDAL().CargarGenero(reader.GetInt32(6)) : null,
                Estado = !reader.IsDBNull(7) ? new EstadoDAL().CargarEstado(reader.GetInt32(7)) : null,
                Activo = reader.GetBoolean(8)
            };
            Console.WriteLine(new GanaderiaDAL().CargarGanaderia(reader.GetInt32(3)));
            return temp;

        }

        public EBovino CargarBovinos(int id)
        {
            string sql = "select id, numero, id_bovino, id_ganaderia, fecha_nacimiento, id_imagen, id_genero, id_estado, activo from ganaderia.bovinos where id = @id";
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", id);

                NpgsqlDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    return CargarBovinos(reader);
                }
            }
            return null;
        }
    }
}
