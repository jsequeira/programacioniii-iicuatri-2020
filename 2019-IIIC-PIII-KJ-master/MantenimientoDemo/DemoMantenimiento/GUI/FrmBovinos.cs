﻿using DemoMantenimiento.BOL;
using MantenimientoDemo.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DemoMantenimiento.GUI
{
    public partial class FrmBovinos : Form
    {
        private BovinoBOL bovBOL;
        private GanaderiaBOL ganBOL;
        private EstadoBOL estBOL;
        private GeneroBOL genBOL;

        public FrmBovinos()
        {
            InitializeComponent();
            bovBOL = new BovinoBOL();
            ganBOL = new GanaderiaBOL();
            estBOL = new EstadoBOL();
            genBOL = new GeneroBOL();
        }

        private void txtFiltro_TextChanged(object sender, EventArgs e)
        {
            CargarDatos();

        }

        private void CargarDatos()
        {
            String filtro = txtFiltro.Text.Trim();
            if (filtro.Length >= 3)
            {
                dataGridView1.DataSource = bovBOL.CargarBovinos(filtro);
            }
            else {
                dataGridView1.DataSource = null;
            }
        }

        private void FrmBovinos_Load(object sender, EventArgs e)
        {
            cbxGanaderia.DataSource = ganBOL.CargarTodo();
            cbxGenero.DataSource = genBOL.CargarTodo();
        }

        private void cbxGenero_SelectedIndexChanged(object sender, EventArgs e)
        {
            EGenero gen = cbxGenero.SelectedItem as EGenero;
            Console.WriteLine(gen.Genero);
            cbxEstado.DataSource = gen != null ? estBOL.CargarTodo(gen) : null;

        }

        private void btnTrazabilidad_Click(object sender, EventArgs e)
        {
            DlgBuscarBovino bb = new DlgBuscarBovino();
            DialogResult result = bb.ShowDialog(this);
            if (result == DialogResult.OK)
            {
                txtTrazabilidad.Text = bb.Bovino.Texto;
                txtTrazabilidad.Tag = bb.Bovino;
            }
            else
            {
                txtTrazabilidad.Text = String.Empty;
                txtTrazabilidad.Tag = null;
            }
        }

        private void pbxFoto_Click(object sender, EventArgs e)
        {
            DialogResult rs = dlgFile.ShowDialog();
            if (rs == DialogResult.OK)
            {
                pbxFoto.Image = Image.FromFile(dlgFile.FileName);
            }
        }

        private void btnGuadar_Click(object sender, EventArgs e)
        {
            try
            {
                lblMensaje.Text = "";
                EBovino b = new EBovino
                {
                    Id = (int)txtNumero.Tag,
                    Numero = Int32.Parse(txtNumero.Text.Trim()),
                    Trazabilidad = txtTrazabilidad.Tag as EBovino,
                    Ganaderia = cbxGanaderia.SelectedItem as EGanaderia,
                    Estado = cbxEstado.SelectedItem as EEstado,
                    Genero = cbxGenero.SelectedItem as EGenero,
                    FechaNacimiento = dtpFecha.Value,
                    Foto = new EImagen { Imagen = pbxFoto.Image },
                    Activo = chkActivo.Checked
                };
                bovBOL.guardar(b);
                lblMensaje.ForeColor = Color.RoyalBlue;
                lblMensaje.Text = "Bovino agregado con éxito";
                txtFiltro.Text = b.Numero.ToString();
                CargarDatos();
            }
            catch (Exception ex)
            {
                lblMensaje.ForeColor = Color.IndianRed;
                lblMensaje.Text = ex.Message;
            }
        }


        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                lblMensaje.Text = "";
                CargarBovino();
            }
            catch (Exception ex)
            {
                lblMensaje.ForeColor = Color.IndianRed;
                lblMensaje.Text = ex.Message;
            }
        }

        private void CargarBovino()
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                DataGridViewRow row = dataGridView1.SelectedRows[0];
                int id = (int)row.Cells[0].Value;
                Console.WriteLine(id);
                EBovino e = bovBOL.CargarBovino(id);
                txtNumero.Text = e.Numero.ToString();
                txtNumero.Tag = e.Id;
                txtTrazabilidad.Text = e.Trazabilidad?.Texto;
                txtTrazabilidad.Tag = e.Trazabilidad;
                cbxGanaderia.SelectedItem = e.Ganaderia;
                cbxGenero.SelectedItem = e.Genero;
                cbxEstado.SelectedItem = e.Estado;
                dtpFecha.Value = e.FechaNacimiento;
                chkActivo.Checked = e.Activo;
                pbxFoto.Image = e.Foto?.Imagen;

            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            txtNumero.Text = String.Empty;
            txtTrazabilidad.Tag = null;
            txtTrazabilidad.Text = "";
            txtNumero.Tag = 0;
            txtNumero.Text = "";
            chkActivo.Checked = true;
            dtpFecha.Value = DateTime.Today;
        }
    }
}
