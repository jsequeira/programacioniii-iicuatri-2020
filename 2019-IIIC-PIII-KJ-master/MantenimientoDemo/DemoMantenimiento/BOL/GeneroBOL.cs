﻿using DemoMantenimiento.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoMantenimiento.BOL
{
    public class GeneroBOL
    {
        public object CargarTodo()
        {
            return new GeneroDAL().CargarTodo();
        }
    }
}
