﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TareaUno.EjercicioDiez
{
    class Mesa
    {
        public int NumMesa { get; set; }
        public Producto[] Productos { get; set; }
        public int[] Cantidad { get; set; }



        public Mesa()
        {
            Productos = new Producto[]
            {
                new Producto{ Nombre = "Hamburguesa Sencilla", Precio = 15 },
                new Producto{ Nombre = "Hamburguesa con Queso", Precio = 18 },
                new Producto{ Nombre = "Hamburguesa Especial", Precio = 20 },
                new Producto{ Nombre = "Papas Fritas", Precio = 8 },
                new Producto{ Nombre = "Refresco", Precio = 5 },
                new Producto{ Nombre = "Postre", Precio = 6 }
            };
        }

        internal string Menu(bool total)
        {
            string menu = "Restaurante UTN - v0.1\nOrden Actual: \n";
            int cont = 1;
            int t = 0;
            foreach (Producto pro in Productos)
            {
                menu += "  " + cont++ + ". " + pro + "\n";
                if (total)
                {
                    t += pro.Cantidad * pro.Precio;
                }
            }
            return menu + (!total
                ? "Seleccione una opción o -1 para regresar"
                : (String.Format("TOTAL: ${0}", t))).PadLeft(40, ' ');
        }

        public int Total()
        {
            int t = 0;
            foreach (Producto pro in Productos)
            {
                t += pro.Cantidad * pro.Precio;
            }
            return t;
        }

        public override string ToString()
        {
            return String.Format("{0}. Mesa #{0}", NumMesa);
        }
    }
}
