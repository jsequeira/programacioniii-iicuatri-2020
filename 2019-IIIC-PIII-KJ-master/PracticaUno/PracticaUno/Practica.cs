﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaUno
{
    class Practica
    {


        internal static String EjercicioUno()
        {
            string nombre = Environment.UserName;
            string hora = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
            return String.Format("Saludos, {0}\nFecha: {1}", nombre, hora);
        }

        internal static int EjercicioDos(int n1, int n2, int n3)
        {
            if (n1 > 0)
            {
                return n2 * n3;
            }
            else
            {
                return n2 + n3;
            }
        }

        internal static int EjercicioCuatro(int n)
        {
            int nv = n;
            int nn = 0;
            while (nv > 0)
            {
                int ult = nv % 10;
                nn = nn * 10 + ult;
                nv = nv / 10;
            }
            return nn;
        }

        internal static String Ejercicio5(int[] arr)
        {
            int may = arr[0];
            int men = arr[0];
            int med = 0;
            foreach (int item in arr)
            {
                if (item > may)
                {
                    may = item;
                }
                if (item < men)
                {
                    men = item;
                }
                med += item;

            }
            return String.Format("Mayor: {0}\nMenor:{1}\nMedia: {2}", may, men, ((double)med / arr.Length));
        }

        internal static int Ejercicio6(int n1, int n2)
        {
            int men = n1 < n2 ? n1 : n2;
            int may = n2 < n1 ? n1 : n2;

            if (may % men == 0)
            {
                return men;
            }

            int div = men / 2;
            while (div > 1)
            {
                if (men % div == 0 && may % div == 0)
                {
                    return div;
                }
                div--;
            }
            return 1;
        }

        internal static int Ejercicio71(int n1)
        {
            int adultos = 2;
            int crias = 0;
            int cont = 0;
            while (cont < n1)
            {
                int adultosB = adultos;
                adultos = adultos + crias;
                crias = adultosB;
                cont++;
            }
            return adultos + crias;
        }

        internal static int Ejercicio72(int n1)
        {
            int cont = 0;
            while (true)
            {
                int can = Ejercicio71(cont);
                if (can >= n1)
                {
                    return cont;
                }
                cont++;
            }
        }

        internal static bool EsPrimo(int n1)
        {
            int div = 2;
            int cont = 2;
            while (cont < n1)
            {
                if (n1 % cont == 0)
                {
                    return false;
                }
                cont++;
            }
            return div == 2;
        }

        internal static double Ejercicio9(int n1)
        {
            DateTime antes = DateTime.Now;
            int contPri = 0;
            int cont = 1;
            while (contPri < n1)
            {
                if (EsPrimo(cont))
                {
                    Console.WriteLine(cont + " --> " + contPri);
                    contPri++;
                }
                cont++;
            }
            DateTime despues = DateTime.Now;
            double ts = despues.Subtract(antes).TotalSeconds;
            return ts;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="n1"></param>
        /// <returns></returns>
        internal static bool EsPerfecto(int n1)
        {
            int div = 0;
            int cont = 1;
            while (cont < n1)
            {
                if (n1 % cont == 0)
                {
                    div += cont;
                }
                cont++;
            }
            return div == n1;
        }
    }
}
