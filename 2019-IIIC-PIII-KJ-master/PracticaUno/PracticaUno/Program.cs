﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaUno
{
    class Program
    {
        static int LeerInt(string msj)
        {
            int num = 0;

        REP:
            Console.Write(msj + ": ");
            string dato = Console.ReadLine();
            if (!Int32.TryParse(dato, out num))
            {
                goto REP;
            }

            return num;
        }
        static void Main(string[] args)
        {
            string menu = "Practica Uno - UTN v0.1\n" +
               " 1. Saludo\n" +
               " 2. Tres números\n" +
               " 3. Leer Entero\n" +
               " 4. Invertir\n" +
               " 5. Mayor, Menor y Media\n" +
               " 6. MCD\n" +
               " 7. Conejos\n" +
               " 8. Es Primo\n" +
               " 9. Benchmark\n" +
               "10. Número Perfecto\n" +
               "11. Salir " +
               "Seleccione una opción";

            while (true)
            {
                int op = LeerInt(menu);
                switch (op)
                {
                    case 1:
                        Console.WriteLine(Practica.EjercicioUno());
                        break;
                    case 2:
                        int n1 = LeerInt("1er número");
                        int n2 = LeerInt("2do número");
                        int n3 = LeerInt("3er número");
                        Console.WriteLine(Practica.EjercicioDos(n1, n2, n3));
                        break;
                    case 3:
                        int n = LeerInt("Leer número para el ejercicio 3");
                        Console.WriteLine(n);
                        break;
                    case 4:
                        n = LeerInt("Numero a invertir");
                        Console.WriteLine(Practica.EjercicioCuatro(n));
                        break;
                    case 5:
                        n = LeerInt("Cantidad de números");
                        int[] arr = new int[n];
                        for (int i = 0; i < n; i++)
                        {
                            arr[i] = LeerInt("#" + (i + 1));
                        }
                        Console.WriteLine(Practica.Ejercicio5(arr));
                        break;
                    case 6:
                        n1 = LeerInt("1er número");
                        n2 = LeerInt("2do número");
                        Console.WriteLine("MCD({0},{1})={2}", n1, n2, Practica.Ejercicio6(n1, n2));
                        break;
                    case 7:
                        string menu2 = "1. Por meses\n2. Por Cantidad\nSeleccione una opción";
                        op = LeerInt(menu2);
                        if (op == 2)
                        {
                            goto case 72;
                        }
                        goto case 71;
                    case 71:
                        n1 = LeerInt("Meses");
                        Console.WriteLine(Practica.Ejercicio71(n1));
                        break;
                    case 72:
                        n1 = LeerInt("Cantidad");
                        Console.WriteLine(Practica.Ejercicio72(n1));
                        break;
                    case 8:
                        n1 = LeerInt("Númmero");
                        Console.WriteLine("El número {0} {1}es primo",n1, Practica.EsPrimo(n1) ? "" : "no ");
                        break;
                    case 9:
                        n1 = LeerInt("Cantidad de Primos");
                        Console.WriteLine("En sacar {0} tarda {1} segundos", n1, Practica.Ejercicio9(n1));
                        break;
                    case 10:
                        n1 = LeerInt("Númmero");
                        Console.WriteLine("El número {0} {1}es perfecto", n1, Practica.EsPerfecto(n1) ? "" : "no ");
                        break;
                    case 11:

                        goto APP;
                    default:
                        Console.WriteLine("fail!!");
                        break;
                }
                Console.ReadKey();
                Console.Clear();
            }
        APP:
            Console.WriteLine("Gracias por utilizar la aplicación");
            Console.ReadKey();
        }
    }
}
