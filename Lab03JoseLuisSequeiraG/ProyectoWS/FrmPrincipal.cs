﻿using System;
using ProyectoWS.IMN_WS;
using System.Windows.Forms;
using DemoWebService;
using System.IO;
using System.Drawing;

namespace ProyectoWS
{
    public partial class FrmPrincipal : Form
    {
        public FrmPrincipal()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            CargarRegiones();
            CargarCiudades();
            cargarTemperaturas();
            cargarefemerides();
            cargarImagenes();
        }

        public void cargarImagenes() {

            string startupPath1 = Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent.FullName, @"img\luna.png");
            string startupPath2 = Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent.FullName, @"img\sol.png");
            string startupPath3 = Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent.FullName, @"img\efe.png");

            Image image1 = Image.FromFile(startupPath1);
            Image image2 = Image.FromFile(startupPath2);
            Image image3 = Image.FromFile(startupPath3);

            sol.BackgroundImage = image1;
            sol.BackgroundImageLayout = ImageLayout.Stretch;

            luna.BackgroundImage = image2;
            luna.BackgroundImageLayout = ImageLayout.Stretch;

            faseLunar.BackgroundImage = image3;
            faseLunar.BackgroundImageLayout = ImageLayout.Stretch;

        }
        private void CargarRegiones()
        {
            int indexRegion = (cbRegion.SelectedIndex);
            WSMeteorologicoClient ws = new WSMeteorologicoClient();
            PRONOSTICO_REGIONAL reg = ws.pronosticoRegional(new pronosticoRegion()).ParseXML<PRONOSTICO_REGIONAL>();
            cbRegion.DataSource = reg.REGIONES;
        }

        private void CargarCiudades()
        {

            int indexRegion = (cbRegion.SelectedIndex);
            WSMeteorologicoClient ws = new WSMeteorologicoClient();
            PRONOSTICO_REGIONAL reg = ws.pronosticoRegional(new pronosticoRegion()).ParseXML<PRONOSTICO_REGIONAL>();
            cbCiudad.DataSource = reg.REGIONES[indexRegion].CIUDADES;





        }

        public void cargarTemperaturas() {

            int indexRegion = (cbRegion.SelectedIndex);
            int indexCiudad = cbCiudad.SelectedIndex;
            WSMeteorologicoClient ws = new WSMeteorologicoClient();
            PRONOSTICO_REGIONAL reg = ws.pronosticoRegional(new pronosticoRegion()).ParseXML<PRONOSTICO_REGIONAL>();
            lbltemMin.Text = reg. REGIONES[indexRegion].CIUDADES[indexCiudad].TEMPMIN.ToString();

            lbltemMax.Text = reg.REGIONES [indexRegion].CIUDADES[indexCiudad].TEMPMAX.ToString();


        }

        public void cargarefemerides() {

            
            WSMeteorologicoClient ws = new WSMeteorologicoClient();

            EFEMERIDES reg = ws.efemerides(new efemerides()).ParseXML<EFEMERIDES>();

            lblFase.Text =  reg.FASELUNAR.Value;

            lblponeLuna.Text = reg.EFEMERIDE_LUNA.SEPONE;
            lblsaleLuna.Text = reg.EFEMERIDE_LUNA.SALE;


            lblponeSol.Text = reg.EFEMERIDE_SOL.SEPONE;
            lblsaleSol.Text = reg.EFEMERIDE_SOL.SALE;

        }


        private void boxRegiones_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargarCiudades();

            WSMeteorologicoClient ws = new WSMeteorologicoClient();
            if (cbRegion.SelectedIndex >= 0)
            {
                int id = (cbRegion.SelectedItem as PRONOSTICO_REGIONALREGION)?.idRegion ?? -1;
                if (id != -1)
                {
                    PRONOSTICO_REGIONAL reg = ws.pronosticoRegionalxID(id).ParseXML<PRONOSTICO_REGIONAL>();
                    if (reg.REGIONES[0].ESTADOMANANA != null)
                    {
                        pbxImagenM.ImageLocation = String.Format("https://www.imn.ac.cr{0}", reg.REGIONES[0].ESTADOMANANA.imgPath);
                        lblComentarioM.Text = reg.REGIONES[0].COMENTARIOMANANA;
                        lblTituloM.Text = reg.REGIONES[0].ESTADOMANANA.Value;
                        pnlMañanaR.Show();
                    }

                    if (reg.REGIONES[0].ESTADOTARDE != null)
                    {
                        pbxImagenT.ImageLocation = String.Format("https://www.imn.ac.cr{0}", reg.REGIONES[0].ESTADOTARDE.imgPath);
                        lblComentarioT.Text = reg.REGIONES[0].COMENTARIOTARDE;
                        lblTituloT.Text = reg.REGIONES[0].ESTADOTARDE.Value;
                        pnlTardeR.Show();
                    }
                    if (reg.REGIONES[0].ESTADONOCHE != null)
                    {
                        pbxImagenN.ImageLocation = String.Format("https://www.imn.ac.cr{0}", reg.REGIONES[0].ESTADONOCHE.imgPath);
                        lblComentarioN.Text = reg.REGIONES[0].COMENTARIONOCHE;
                        lblTituloN.Text = reg.REGIONES[0].ESTADONOCHE.Value;
                        pnlNocheR.Show();
                    }
                }
            }
        }

  

        private void cbCiudad_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargarTemperaturas();
        }

        private void groupBox4_Enter(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
