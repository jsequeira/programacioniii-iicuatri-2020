﻿namespace CapaPresentacion
{
    partial class FrmJuegoPalabras
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pJuego = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.btnPista = new System.Windows.Forms.Button();
            this.btnPuntos = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPais = new System.Windows.Forms.TextBox();
            this.btnImg = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSegundos = new System.Windows.Forms.Button();
            this.btnMinutos = new System.Windows.Forms.Button();
            this.pOpciones = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.cbContinentes = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.rb1 = new System.Windows.Forms.RadioButton();
            this.rb2 = new System.Windows.Forms.RadioButton();
            this.rb3 = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.btnComenzar = new System.Windows.Forms.Button();
            this.tbMinutos = new System.Windows.Forms.TextBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.btnAtras = new System.Windows.Forms.Button();
            this.pJuego.SuspendLayout();
            this.pOpciones.SuspendLayout();
            this.SuspendLayout();
            // 
            // pJuego
            // 
            this.pJuego.BackColor = System.Drawing.Color.Transparent;
            this.pJuego.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pJuego.Controls.Add(this.label7);
            this.pJuego.Controls.Add(this.btnPista);
            this.pJuego.Controls.Add(this.btnPuntos);
            this.pJuego.Controls.Add(this.label5);
            this.pJuego.Controls.Add(this.label4);
            this.pJuego.Controls.Add(this.txtPais);
            this.pJuego.Controls.Add(this.btnImg);
            this.pJuego.Controls.Add(this.label3);
            this.pJuego.Controls.Add(this.btnSegundos);
            this.pJuego.Controls.Add(this.btnMinutos);
            this.pJuego.Enabled = false;
            this.pJuego.Location = new System.Drawing.Point(12, 189);
            this.pJuego.Name = "pJuego";
            this.pJuego.Size = new System.Drawing.Size(697, 253);
            this.pJuego.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 181);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 17);
            this.label7.TabIndex = 11;
            this.label7.Text = "Pista:";
            // 
            // btnPista
            // 
            this.btnPista.Location = new System.Drawing.Point(52, 175);
            this.btnPista.Name = "btnPista";
            this.btnPista.Size = new System.Drawing.Size(293, 29);
            this.btnPista.TabIndex = 10;
            this.btnPista.UseVisualStyleBackColor = true;
            // 
            // btnPuntos
            // 
            this.btnPuntos.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPuntos.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnPuntos.Location = new System.Drawing.Point(483, 30);
            this.btnPuntos.Name = "btnPuntos";
            this.btnPuntos.Size = new System.Drawing.Size(129, 101);
            this.btnPuntos.TabIndex = 9;
            this.btnPuntos.Text = "puntos";
            this.btnPuntos.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(503, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 17);
            this.label5.TabIndex = 8;
            this.label5.Text = "Puntuación";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(351, 221);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(304, 17);
            this.label4.TabIndex = 7;
            this.label4.Text = "Nota:Presionar Enter para confirmar respuesta";
            // 
            // txtPais
            // 
            this.txtPais.Location = new System.Drawing.Point(6, 218);
            this.txtPais.Name = "txtPais";
            this.txtPais.Size = new System.Drawing.Size(339, 22);
            this.txtPais.TabIndex = 6;
            this.txtPais.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPais_KeyDown);
            // 
            // btnImg
            // 
            this.btnImg.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnImg.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImg.Location = new System.Drawing.Point(6, 92);
            this.btnImg.Name = "btnImg";
            this.btnImg.Size = new System.Drawing.Size(339, 64);
            this.btnImg.TabIndex = 5;
            this.btnImg.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(117, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(107, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "Cronómetro";
            // 
            // btnSegundos
            // 
            this.btnSegundos.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSegundos.ForeColor = System.Drawing.SystemColors.Highlight;
            this.btnSegundos.Location = new System.Drawing.Point(177, 30);
            this.btnSegundos.Name = "btnSegundos";
            this.btnSegundos.Size = new System.Drawing.Size(67, 37);
            this.btnSegundos.TabIndex = 3;
            this.btnSegundos.Text = "s";
            this.btnSegundos.UseVisualStyleBackColor = true;
            // 
            // btnMinutos
            // 
            this.btnMinutos.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMinutos.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btnMinutos.Location = new System.Drawing.Point(96, 30);
            this.btnMinutos.Name = "btnMinutos";
            this.btnMinutos.Size = new System.Drawing.Size(70, 37);
            this.btnMinutos.TabIndex = 2;
            this.btnMinutos.Text = "m";
            this.btnMinutos.UseVisualStyleBackColor = true;
            // 
            // pOpciones
            // 
            this.pOpciones.BackColor = System.Drawing.Color.Transparent;
            this.pOpciones.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pOpciones.Controls.Add(this.label6);
            this.pOpciones.Controls.Add(this.cbContinentes);
            this.pOpciones.Controls.Add(this.label1);
            this.pOpciones.Controls.Add(this.rb1);
            this.pOpciones.Controls.Add(this.rb2);
            this.pOpciones.Controls.Add(this.rb3);
            this.pOpciones.Controls.Add(this.label2);
            this.pOpciones.Controls.Add(this.btnComenzar);
            this.pOpciones.Controls.Add(this.tbMinutos);
            this.pOpciones.Location = new System.Drawing.Point(12, 12);
            this.pOpciones.Name = "pOpciones";
            this.pOpciones.Size = new System.Drawing.Size(697, 171);
            this.pOpciones.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(328, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(91, 17);
            this.label6.TabIndex = 14;
            this.label6.Text = "Continente:";
            // 
            // cbContinentes
            // 
            this.cbContinentes.FormattingEnabled = true;
            this.cbContinentes.Location = new System.Drawing.Point(331, 36);
            this.cbContinentes.Name = "cbContinentes";
            this.cbContinentes.Size = new System.Drawing.Size(121, 24);
            this.cbContinentes.TabIndex = 16;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 17);
            this.label1.TabIndex = 8;
            this.label1.Text = "Escoger tiempo:";
            // 
            // rb1
            // 
            this.rb1.AutoSize = true;
            this.rb1.Location = new System.Drawing.Point(124, 36);
            this.rb1.Name = "rb1";
            this.rb1.Size = new System.Drawing.Size(90, 21);
            this.rb1.TabIndex = 3;
            this.rb1.TabStop = true;
            this.rb1.Text = "3 minutos";
            this.rb1.UseVisualStyleBackColor = true;
            this.rb1.Click += new System.EventHandler(this.rb1_Click);
            // 
            // rb2
            // 
            this.rb2.AutoSize = true;
            this.rb2.Location = new System.Drawing.Point(124, 63);
            this.rb2.Name = "rb2";
            this.rb2.Size = new System.Drawing.Size(90, 21);
            this.rb2.TabIndex = 4;
            this.rb2.TabStop = true;
            this.rb2.Text = "5 minutos";
            this.rb2.UseVisualStyleBackColor = true;
            this.rb2.Click += new System.EventHandler(this.rb2_Click);
            // 
            // rb3
            // 
            this.rb3.AutoSize = true;
            this.rb3.Location = new System.Drawing.Point(124, 90);
            this.rb3.Name = "rb3";
            this.rb3.Size = new System.Drawing.Size(90, 21);
            this.rb3.TabIndex = 5;
            this.rb3.TabStop = true;
            this.rb3.Text = "7 minutos";
            this.rb3.UseVisualStyleBackColor = true;
            this.rb3.Click += new System.EventHandler(this.rb3_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 122);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(116, 17);
            this.label2.TabIndex = 9;
            this.label2.Text = "Personalizado:";
            // 
            // btnComenzar
            // 
            this.btnComenzar.Location = new System.Drawing.Point(273, 143);
            this.btnComenzar.Name = "btnComenzar";
            this.btnComenzar.Size = new System.Drawing.Size(100, 23);
            this.btnComenzar.TabIndex = 10;
            this.btnComenzar.Text = "Comenzar";
            this.btnComenzar.UseVisualStyleBackColor = true;
            this.btnComenzar.Click += new System.EventHandler(this.btnComenzar_Click);
            // 
            // tbMinutos
            // 
            this.tbMinutos.Location = new System.Drawing.Point(125, 117);
            this.tbMinutos.Name = "tbMinutos";
            this.tbMinutos.Size = new System.Drawing.Size(100, 22);
            this.tbMinutos.TabIndex = 6;
            this.tbMinutos.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // btnAtras
            // 
            this.btnAtras.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAtras.Location = new System.Drawing.Point(12, 457);
            this.btnAtras.Name = "btnAtras";
            this.btnAtras.Size = new System.Drawing.Size(75, 23);
            this.btnAtras.TabIndex = 13;
            this.btnAtras.Text = "Atras";
            this.btnAtras.UseVisualStyleBackColor = true;
            this.btnAtras.Click += new System.EventHandler(this.btnAtras_Click);
            // 
            // FrmJuegoPalabras
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::CapaPresentacion.Properties.Resources.background1;
            this.ClientSize = new System.Drawing.Size(724, 492);
            this.Controls.Add(this.btnAtras);
            this.Controls.Add(this.pJuego);
            this.Controls.Add(this.pOpciones);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmJuegoPalabras";
            this.Text = "Juego";
            this.pJuego.ResumeLayout(false);
            this.pJuego.PerformLayout();
            this.pOpciones.ResumeLayout(false);
            this.pOpciones.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pJuego;
        private System.Windows.Forms.Button btnPuntos;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtPais;
        private System.Windows.Forms.Button btnImg;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnSegundos;
        private System.Windows.Forms.Button btnMinutos;
        private System.Windows.Forms.Panel pOpciones;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbContinentes;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rb1;
        private System.Windows.Forms.RadioButton rb2;
        private System.Windows.Forms.RadioButton rb3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnComenzar;
        private System.Windows.Forms.TextBox tbMinutos;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnPista;
        private System.Windows.Forms.Button btnAtras;
    }
}