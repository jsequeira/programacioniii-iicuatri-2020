﻿namespace CapaPresentacion
{
    partial class FrmRecordGlobal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvBandera = new System.Windows.Forms.DataGridView();
            this.btnAtras = new System.Windows.Forms.Button();
            this.dgvPalabra = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBandera)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPalabra)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvBandera
            // 
            this.dgvBandera.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvBandera.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBandera.Location = new System.Drawing.Point(13, 43);
            this.dgvBandera.Name = "dgvBandera";
            this.dgvBandera.ReadOnly = true;
            this.dgvBandera.RowHeadersWidth = 51;
            this.dgvBandera.RowTemplate.Height = 24;
            this.dgvBandera.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvBandera.Size = new System.Drawing.Size(365, 199);
            this.dgvBandera.TabIndex = 0;
            this.dgvBandera.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvBandera_CellClick);
            // 
            // btnAtras
            // 
            this.btnAtras.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAtras.Location = new System.Drawing.Point(12, 539);
            this.btnAtras.Name = "btnAtras";
            this.btnAtras.Size = new System.Drawing.Size(75, 23);
            this.btnAtras.TabIndex = 1;
            this.btnAtras.Text = "Atras";
            this.btnAtras.UseVisualStyleBackColor = true;
            this.btnAtras.Click += new System.EventHandler(this.btnAtras_Click);
            // 
            // dgvPalabra
            // 
            this.dgvPalabra.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvPalabra.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPalabra.Location = new System.Drawing.Point(12, 301);
            this.dgvPalabra.Name = "dgvPalabra";
            this.dgvPalabra.ReadOnly = true;
            this.dgvPalabra.RowHeadersWidth = 51;
            this.dgvPalabra.RowTemplate.Height = 24;
            this.dgvPalabra.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPalabra.Size = new System.Drawing.Size(365, 199);
            this.dgvPalabra.TabIndex = 2;
            this.dgvPalabra.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPalabra_CellClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(123, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(138, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Juegos de banderas";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(123, 268);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(133, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Juegos de palabras";
            // 
            // FrmRecordGlobal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::CapaPresentacion.Properties.Resources.background1;
            this.ClientSize = new System.Drawing.Size(390, 583);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgvPalabra);
            this.Controls.Add(this.btnAtras);
            this.Controls.Add(this.dgvBandera);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmRecordGlobal";
            this.Text = "FrmRecordGlobal";
            this.Load += new System.EventHandler(this.FrmRecordGlobal_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBandera)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPalabra)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvBandera;
        private System.Windows.Forms.Button btnAtras;
        private System.Windows.Forms.DataGridView dgvPalabra;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}