﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaNegocio;
using WebService;
using System.Diagnostics;
using System.Net;
using CapaNegocioh;
using System.Runtime.CompilerServices;
using System.Net.NetworkInformation;
using CapaPresentacion;

namespace _3_ProyectoJoseLuisSequeiraG
{
    public partial class FrmJuegoBanderas: Form
    {
        FrmMenu parent;

        int incorrrectas = 0;
        int correctas = 0;
        int puntos = 0;
        int minutos = 0;

        Stopwatch osw = new Stopwatch();

        List<string> datosUsuario;

        Dictionary<string, string> dicContinentes;
        Dictionary<string, string> dicPaises;
        Dictionary<string, string> dicurl;
        string pais;


        public FrmJuegoBanderas(FrmMenu parent, List<string> datosUsuario)
        {
            InitializeComponent();
            CenterToScreen();
            cargarDatosDB();
            cargarComponentes();
            cbContinentes.SelectedIndex = 0;
            this.parent = parent;
            this.datosUsuario = datosUsuario;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {

            TimeSpan ts = new TimeSpan(0, 0, 0, 0, (int)osw.ElapsedMilliseconds);
            btnMinutos.Text = ts.Minutes.ToString();
            btnSegundos.Text = ts.Seconds.ToString();
            if (ts.Minutes == this.minutos && ts.Minutes != 0)
            {

                timer1.Enabled = false;
                osw.Restart();
                MessageBox.Show("Tiempo agotado!!!!");
              
                btnMinutos.Text = "0";
                btnSegundos.Text = "0";
                pJuego.Enabled = false;
                pOpciones.Enabled = true;

                btnImg.BackgroundImage = null;
                btnPuntos.Text = "";

                new CNRecord().registrarRecord(datosUsuario[1], "Bandera", this.correctas, this.incorrrectas, this.puntos, this.minutos);
                incorrrectas = 0;
                correctas = 0;
                puntos = 0;
                minutos = 0;
            }

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            rb1.Checked = false;
            rb2.Checked = false;
            rb3.Checked = false;

            try
            {

                int verificacion = int.Parse(tbMinutos.Text) * 0;


            }
            catch
            {

                MessageBox.Show("Por favor digite solo enteros!");
                tbMinutos.Text = "";
            }
        }
        private void rb3_Click(object sender, EventArgs e)
        {

            tbMinutos.Text = "";
            rb3.Checked = true;
        }

        private void rb2_Click(object sender, EventArgs e)
        {
            tbMinutos.Text = "";
            rb2.Checked = true;
        }

        private void rb1_Click(object sender, EventArgs e)
        {
            tbMinutos.Text = "";
            rb1.Checked = true;
        }

        private void btnComenzar_Click(object sender, EventArgs e)
        {
            if (rb1.Checked == true) {
                this.minutos = 3;
            }
            if (rb2.Checked == true) {
                this.minutos = 5;
            }
            if (rb3.Checked == true) {
                this.minutos = 7;
            }
            if (tbMinutos.Text != "") {

                this.minutos = int.Parse(tbMinutos.Text);

            }

            timer1.Enabled = false;
            osw.Restart();

            btnMinutos.Text = "0";
            btnSegundos.Text = "0";

            timer1.Enabled = true;
            osw.Start();

            pJuego.Enabled = true;

            pOpciones.Enabled = false;

            cargarPaises();
            cargarImagen();
        }


        public void cargarDatosDB() {

            new CNCargarDatos().cargarDatosBD();

        }

        public void cargarComponentes() {


            dicContinentes = new Dictionary<string, string>();
            new CNContinente().cargarcbContinentes(cbContinentes, dicContinentes);
            dicurl = new Dictionary<string, string>();
            new CNImagen().cargarurlImagen(dicurl);
        }

        public void cargarPaises() {

            string isoContinente = "";
            string continente = cbContinentes.SelectedItem.ToString();

            foreach (var c in dicContinentes) {

                if (c.Key == continente) {

                    isoContinente = c.Value;
                }
                dicPaises = new Dictionary<string, string>();

                new CNPais().cargarPaises(dicPaises, isoContinente);

            }

        }

        public void cargarImagen (){
          

            int numero = new Random().Next(0, (dicPaises.Count)-1);

            var dic = dicPaises.ElementAt(numero);

            this.pais = dic.Key;

            System.Console.WriteLine(pais);


            string isoPais = dic.Value;

            string url = dicurl[isoPais];

            var request = WebRequest.Create(url);

            using (var response = request.GetResponse())
            using (var stream = response.GetResponseStream())
            {
                btnImg.BackgroundImage = Bitmap.FromStream(stream);
            }
        }

        private void txtPais_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtPais.Text == this.pais) {

                    this.puntos = this.puntos + 10;
                    btnPuntos.Text = puntos.ToString();
                    this.correctas = this.correctas + 1;
                }
                else {

                    this.incorrrectas = this.incorrrectas + 1;
                
                
                }
                cargarImagen();
                txtPais.Text = "";

            }
            
        }

        private void btnImg_Click(object sender, EventArgs e)
        {

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.parent.Visible = true;
            this.Hide();
        }
    }
}