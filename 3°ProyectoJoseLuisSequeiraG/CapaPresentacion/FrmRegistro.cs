﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaNegocio;

namespace CapaPresentacion
{
    public partial class FrmRegistro : Form
    {

        Dictionary<string, string> dicContinentes;
        Dictionary<string, string> dicPaises;
        string isoPais="";
        public FrmRegistro()
        {
            InitializeComponent();
            CenterToScreen();
            dicContinentes = new Dictionary<string, string>();
            new CNContinente().cargarcbContinentes(cbContinente, dicContinentes);
            establecerFilas();
        }

        public void cargarPaises()
        {

            string isoContinente = "";
            string continente = cbContinente.SelectedItem.ToString();

            foreach (var c in dicContinentes)
            {

                if (c.Key == continente)
                {

                    isoContinente = c.Value;
                }
                dicPaises = new Dictionary<string, string>();

                new CNPais().cargarPaises(dicPaises, isoContinente);

            }
        }
        public void definirDgv()
        {

            DataTable dataTable = new DataTable();
            dataTable.Columns.Add("Iso");
            dataTable.Columns.Add("Pais");
            dgv.DataSource = dataTable;


        }
        public void establecerFilas()
        {

            dgv.DefaultCellStyle.SelectionBackColor = Color.White;
            dgv.DefaultCellStyle.SelectionForeColor = Color.Black;

        }


        private void cbContinente_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargarPaises();
            dgv.Columns.Clear();
            definirDgv();

            DataTable datatable = (DataTable)dgv.DataSource;


            foreach (var dic in dicPaises)
            {

                DataRow datarow1 = datatable.NewRow();


                string iso = dic.Value;
                string nombre = dic.Key;

                datarow1["Iso"] = iso;
                datarow1["Pais"] = nombre;

                datatable.Rows.Add(datarow1);

                dgv.DataSource = datatable;

            }
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            if (dgv.DataSource!=null) {
                DataTable dt = (DataTable)dgv.DataSource;

                dt.DefaultView.RowFilter = string.Format($"Pais LIKE '%{txtBuscar.Text}%'");
            }
           
        }
        private void btnRegistro_Click(object sender, EventArgs e)
        {
            string fecha = dtpFecha.Value.ToString("yyyy-MM-dd");

            if(txtNombre.Text=="" || txtCedula.Text=="" || txtContrasenna.Text=="" || fecha =="" || isoPais=="")
            {
                 MessageBox.Show("Tiene campos vacíos");
            }
            else
            {
                new CNUsuario().registrarUsuario(txtNombre.Text,txtCedula.Text,fecha,txtContrasenna.Text,this.isoPais);
                MessageBox.Show("Completo");
                this.Hide();
                FrmInicio frm = new FrmInicio();
                frm.Visible = true;
                    


            }

           
        }

        private void dgv_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex>=0  && e.ColumnIndex>=0) {

                this.isoPais = dgv.Rows[e.RowIndex].Cells[0].Value.ToString();
                dgv.DefaultCellStyle.SelectionBackColor = Color.BlueViolet;
                dgv.DefaultCellStyle.SelectionForeColor = Color.White;

            }

          }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            FrmInicio frm = new FrmInicio();
            frm.Visible = true;
            this.Hide();
        }

        private void FrmRegistro_Load(object sender, EventArgs e)
        {

        }

      
    }
}
