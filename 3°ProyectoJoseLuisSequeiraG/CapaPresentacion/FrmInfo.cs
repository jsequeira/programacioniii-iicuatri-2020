﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WebService;
using CapaNegocio;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using System.Net;

namespace CapaPresentacion
{
    public partial class FrmInfo : Form
    {
        FrmMenu parent;

        Dictionary<string, string> dicContinentes;
        Dictionary<string, string> dicPaises;
        public FrmInfo(FrmMenu parent)
        {
            InitializeComponent();
            dicContinentes = new Dictionary<string, string>();
            new CNContinente().cargarcbContinentes(cbContinente, dicContinentes);
            CenterToScreen();
            this.parent = parent;
            establecerFilas();

        }

        public void establecerFilas()
        {

            dgv.DefaultCellStyle.SelectionBackColor = Color.White;
            dgv.DefaultCellStyle.SelectionForeColor = Color.Black;

        }

        public void cargarPaises()
        {

            string isoContinente = "";
            string continente = cbContinente.SelectedItem.ToString();

            foreach (var c in dicContinentes)
            {

                if (c.Key == continente)
                {

                    isoContinente = c.Value;
                }
                dicPaises = new Dictionary<string, string>();

                new CNPais().cargarPaises(dicPaises, isoContinente);

            }
        }


        public void definirDgv() {

            DataTable dataTable = new DataTable();
            dataTable.Columns.Add("Iso");
            dataTable.Columns.Add("Pais");
            dgv.DataSource = dataTable;


        }


        private void cbContinente_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargarPaises();
            dgv.Columns.Clear();
            definirDgv();

            DataTable datatable = (DataTable)dgv.DataSource;

           
            foreach (var dic in dicPaises) {
             
            DataRow datarow1 = datatable.NewRow();
                
            
             string iso = dic.Value;
            string nombre = dic.Key;

            datarow1["Iso"] =iso;          
            datarow1["Pais"] =nombre;

            datatable.Rows.Add(datarow1);
           
            dgv.DataSource = datatable;

            }

        }

        private void dgv_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex>-1 && e.ColumnIndex>-1) {
                 dgv.DefaultCellStyle.SelectionBackColor = Color.BlueViolet;                                             
                 dgv.DefaultCellStyle.SelectionForeColor = Color.White;              
                 new Metodos().cargarPais(txtPais, txtCapital, txtMoneda, txtLenguaje, dgv.Rows[e.RowIndex].Cells[0].Value.ToString());
               
                string url= new Metodos().cargarImagen(dgv.Rows[e.RowIndex].Cells[0].Value.ToString());

                var request = WebRequest.Create(url);

                using (var response = request.GetResponse())
                using (var stream = response.GetResponseStream())
                {
                    btnImg.BackgroundImage = Bitmap.FromStream(stream);
                }
            }
            }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {

            DataTable dt = (DataTable)dgv.DataSource;

            dt.DefaultView.RowFilter = string.Format($"Pais LIKE '%{txtBuscar.Text}%'");
        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            this.parent.Visible = true;
            this.Hide ();
        }
    }
   
}