﻿namespace CapaPresentacion
{
    partial class FrmRecord
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.graficoBandera = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.graficoPalabra = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnbanMinutos = new System.Windows.Forms.Button();
            this.btnpalMinutos = new System.Windows.Forms.Button();
            this.btnbanPuntos = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.btnpalPuntos = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.btnpalPartidas = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.btnbanPartidas = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.btnAtras = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.graficoBandera)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.graficoPalabra)).BeginInit();
            this.SuspendLayout();
            // 
            // graficoBandera
            // 
            this.graficoBandera.BackColor = System.Drawing.Color.Transparent;
            chartArea1.AxisY.Maximum = 100D;
            chartArea1.AxisY.Minimum = 0D;
            chartArea1.Name = "ChartArea1";
            this.graficoBandera.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.graficoBandera.Legends.Add(legend1);
            this.graficoBandera.Location = new System.Drawing.Point(148, 21);
            this.graficoBandera.Name = "graficoBandera";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "Series2";
            this.graficoBandera.Series.Add(series1);
            this.graficoBandera.Series.Add(series2);
            this.graficoBandera.Size = new System.Drawing.Size(434, 225);
            this.graficoBandera.TabIndex = 1;
            this.graficoBandera.Text = "Mi Record";
            // 
            // graficoPalabra
            // 
            this.graficoPalabra.BackColor = System.Drawing.Color.Transparent;
            chartArea2.AxisY.Maximum = 100D;
            chartArea2.AxisY.Minimum = 0D;
            chartArea2.Name = "ChartArea1";
            this.graficoPalabra.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.graficoPalabra.Legends.Add(legend2);
            this.graficoPalabra.Location = new System.Drawing.Point(148, 274);
            this.graficoPalabra.Name = "graficoPalabra";
            series3.ChartArea = "ChartArea1";
            series3.Legend = "Legend1";
            series3.Name = "Series1";
            series4.ChartArea = "ChartArea1";
            series4.Legend = "Legend1";
            series4.Name = "Series2";
            this.graficoPalabra.Series.Add(series3);
            this.graficoPalabra.Series.Add(series4);
            this.graficoPalabra.Size = new System.Drawing.Size(434, 231);
            this.graficoPalabra.TabIndex = 2;
            this.graficoPalabra.Text = "chart1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 274);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Total de minutos";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Total de minutos";
            // 
            // btnbanMinutos
            // 
            this.btnbanMinutos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnbanMinutos.Location = new System.Drawing.Point(21, 41);
            this.btnbanMinutos.Name = "btnbanMinutos";
            this.btnbanMinutos.Size = new System.Drawing.Size(73, 50);
            this.btnbanMinutos.TabIndex = 5;
            this.btnbanMinutos.Text = "Minutos";
            this.btnbanMinutos.UseVisualStyleBackColor = true;
            // 
            // btnpalMinutos
            // 
            this.btnpalMinutos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnpalMinutos.Location = new System.Drawing.Point(29, 294);
            this.btnpalMinutos.Name = "btnpalMinutos";
            this.btnpalMinutos.Size = new System.Drawing.Size(73, 52);
            this.btnpalMinutos.TabIndex = 6;
            this.btnpalMinutos.Text = "Minutos";
            this.btnpalMinutos.UseVisualStyleBackColor = true;
            // 
            // btnbanPuntos
            // 
            this.btnbanPuntos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnbanPuntos.Location = new System.Drawing.Point(21, 114);
            this.btnbanPuntos.Name = "btnbanPuntos";
            this.btnbanPuntos.Size = new System.Drawing.Size(73, 50);
            this.btnbanPuntos.TabIndex = 8;
            this.btnbanPuntos.Text = "puntos";
            this.btnbanPuntos.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(107, 17);
            this.label3.TabIndex = 7;
            this.label3.Text = "Total de puntos";
            // 
            // btnpalPuntos
            // 
            this.btnpalPuntos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnpalPuntos.Location = new System.Drawing.Point(29, 369);
            this.btnpalPuntos.Name = "btnpalPuntos";
            this.btnpalPuntos.Size = new System.Drawing.Size(73, 50);
            this.btnpalPuntos.TabIndex = 10;
            this.btnpalPuntos.Text = "puntos";
            this.btnpalPuntos.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 349);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(107, 17);
            this.label4.TabIndex = 9;
            this.label4.Text = "Total de puntos";
            // 
            // btnpalPartidas
            // 
            this.btnpalPartidas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnpalPartidas.Location = new System.Drawing.Point(29, 446);
            this.btnpalPartidas.Name = "btnpalPartidas";
            this.btnpalPartidas.Size = new System.Drawing.Size(73, 50);
            this.btnpalPartidas.TabIndex = 12;
            this.btnpalPartidas.Text = "palabras";
            this.btnpalPartidas.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(18, 426);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(115, 17);
            this.label5.TabIndex = 11;
            this.label5.Text = "Total de partidas";
            // 
            // btnbanPartidas
            // 
            this.btnbanPartidas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnbanPartidas.Location = new System.Drawing.Point(21, 187);
            this.btnbanPartidas.Name = "btnbanPartidas";
            this.btnbanPartidas.Size = new System.Drawing.Size(73, 50);
            this.btnbanPartidas.TabIndex = 14;
            this.btnbanPartidas.Text = "palabras";
            this.btnbanPartidas.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 167);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(115, 17);
            this.label6.TabIndex = 13;
            this.label6.Text = "Total de partidas";
            // 
            // btnAtras
            // 
            this.btnAtras.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAtras.Location = new System.Drawing.Point(29, 520);
            this.btnAtras.Name = "btnAtras";
            this.btnAtras.Size = new System.Drawing.Size(75, 23);
            this.btnAtras.TabIndex = 15;
            this.btnAtras.Text = "Atras";
            this.btnAtras.UseVisualStyleBackColor = true;
            this.btnAtras.Click += new System.EventHandler(this.btnAtras_Click);
            // 
            // FrmRecord
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::CapaPresentacion.Properties.Resources.background1;
            this.ClientSize = new System.Drawing.Size(599, 566);
            this.Controls.Add(this.btnAtras);
            this.Controls.Add(this.btnbanPartidas);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnpalPartidas);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnpalPuntos);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnbanPuntos);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnpalMinutos);
            this.Controls.Add(this.btnbanMinutos);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.graficoPalabra);
            this.Controls.Add(this.graficoBandera);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmRecord";
            this.Text = "FrmRecord";
            this.Load += new System.EventHandler(this.FrmRecord_Load);
            ((System.ComponentModel.ISupportInitialize)(this.graficoBandera)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.graficoPalabra)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart graficoBandera;
        private System.Windows.Forms.DataVisualization.Charting.Chart graficoPalabra;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnbanMinutos;
        private System.Windows.Forms.Button btnpalMinutos;
        private System.Windows.Forms.Button btnbanPuntos;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnpalPuntos;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnpalPartidas;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnbanPartidas;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnAtras;
    }
}