﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaNegocio;
namespace CapaPresentacion
{
    public partial class FrmRecordPersona : Form
    {



        FrmMenu parent;
        
        public FrmRecordPersona(FrmMenu parent)
        {
            InitializeComponent();
            this.parent = parent;
            CenterToScreen();
            establecerFilas();
        }
        public void establecerFilas()
        {

            dgv.DefaultCellStyle.SelectionBackColor = Color.White;
            dgv.DefaultCellStyle.SelectionForeColor = Color.Black;

        }

        private void FrmRecordPersona_Load(object sender, EventArgs e)
        {
            definirDgv(); 
            new CNRecord().obtenerrecordPersonal(dgv);
        }

        public void definirDgv()
        {
       
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add("Nombre");
            dataTable.Columns.Add("Puntos");
            dataTable.Columns.Add("Tipo");
            dataTable.Columns.Add("Cedula");
            dgv.DataSource = dataTable;
            DataGridViewButtonColumn bcEliminar = new DataGridViewButtonColumn();

        }

        private void btnAtras_Click(object sender, EventArgs e)
        {

            this.Hide();
            parent.Visible = true;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)dgv.DataSource;

            dt.DefaultView.RowFilter = string.Format($"Nombre LIKE '%{txtBuscar.Text}%'");
        }

        private void dgv_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                dgv.DefaultCellStyle.SelectionBackColor = Color.BlueViolet;
                dgv.DefaultCellStyle.SelectionForeColor = Color.White;
                FrmRecord frm = new FrmRecord(this, dgv.Rows[e.RowIndex].Cells[3].Value.ToString());
                frm.Visible=true;
                this.Hide();

            }
        }
    }
}
