﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using static System.Net.Mime.MediaTypeNames;
using System.Net;

namespace CapaDatos
{
   public class CDRecord
    {




        public NpgsqlConnection conexionRetorno;
        Conexion conexion = new Conexion();

        public void registrarRecord(string cedula, string tipo, int correctas, int incorrectas, int puntos, int tiempo)
        {



            string cedulaBD;
            int correctasDB;
            int incorrectasDB;
            int puntosDB;
            int tiempoDB;
            int partidasDB;


            conexionRetorno = conexion.ConexionBD();
            conexion.conexion.Open();

            NpgsqlCommand cmd = new NpgsqlCommand($"SELECT correctas, incorrectas,puntos,tiempo,partidas from ip.record where cedula='{cedula}' and tipo='{tipo}' ", conexionRetorno);
            NpgsqlDataReader dr = cmd.ExecuteReader();
           

            

            if (dr.HasRows)
            {
                dr.Read();
                correctasDB = correctas + dr.GetInt32(0);
                incorrectasDB = incorrectas + dr.GetInt32(1);
                puntosDB = puntos + dr.GetInt32(2);
                tiempoDB = tiempo + dr.GetInt32(3);
                partidasDB = dr.GetInt32(4) + 1;
                dr.Close();

                cmd = new NpgsqlCommand($"DELETE FROM ip.record WHERE cedula ='{cedula}' and tipo='{tipo}';", conexionRetorno);
                cmd.ExecuteNonQuery();



                cmd = new NpgsqlCommand($"INSERT INTO ip.record (cedula,tipo,correctas,incorrectas,puntos,tiempo,partidas)VALUES('{cedula}','{tipo}','{correctasDB}','{incorrectasDB}','{puntosDB}','{tiempoDB}','{partidasDB}');", conexionRetorno);
                cmd.ExecuteNonQuery();


            }
            else
            {
                dr.Close();
                cmd = new NpgsqlCommand($"INSERT INTO ip.record (cedula,tipo,correctas,incorrectas,puntos,tiempo,partidas)VALUES('{cedula}','{tipo}','{correctas}','{incorrectas}','{puntos}','{tiempo}',1);", conexionRetorno);
                cmd.ExecuteNonQuery();
               
            }
            conexion.conexion.Close();
        }


        public void obtenerRecord(string cedula, string tipo, List<int> datos)
        {
            conexionRetorno = conexion.ConexionBD();
            conexion.conexion.Open();

            NpgsqlCommand cmd = new NpgsqlCommand($"SELECT correctas, incorrectas,puntos,tiempo,partidas from ip.record where cedula='{cedula}' and tipo='{tipo}' ", conexionRetorno);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                dr.Read();
                datos.Add(dr.GetInt32(0));
                datos.Add(dr.GetInt32(1));
                datos.Add(dr.GetInt32(2));
                datos.Add(dr.GetInt32(3));
                datos.Add(dr.GetInt32(4));
                dr.Close();
            }
            dr.Close();
            conexion.conexion.Close();
            
        }






        public void obtenerrecordGlobal(DataGridView dgv,string tipo)
        {
            conexionRetorno = conexion.ConexionBD();
            conexion.conexion.Open();

            System.Drawing.Image image;




            DataTable dt = (DataTable)dgv.DataSource;

            NpgsqlCommand cmd = new NpgsqlCommand($"SELECT us.nombrecompleto, pa.nombre, im.url, re.puntos,us.cedula FROM ip.usuario us, ip.pais pa, ip.imagen im, ip.record re WHERE us.cedula= re.cedula and us.isopais= pa.cdgoiso and im.isopais= pa.cdgoiso and re.tipo='{tipo}' order by (re.puntos) Desc LIMIT 3", conexionRetorno);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {


                while (dr.Read())
                {
                 
                    DataRow datarow1 = dt.NewRow();

                    datarow1["nombre"]=(dr.GetString(0));
                    datarow1["Pais"] = (dr.GetString(1));
                    datarow1["Puntos"] = (dr.GetString(3));
                    datarow1["Cedula"] = (dr.GetString(4));
                    dt.Rows.Add(datarow1);

                    dgv.DataSource =dt;

               
                }
            }
            dr.Close();
            conexion.conexion.Close();

        }

        public void obtenerrecordPersonal(DataGridView dgv)
        {
            conexionRetorno = conexion.ConexionBD();
            conexion.conexion.Open();

            System.Drawing.Image image;




            DataTable dt = (DataTable)dgv.DataSource;

            NpgsqlCommand cmd = new NpgsqlCommand($"SELECT us.nombrecompleto,re.tipo, re.puntos,us.cedula FROM ip.usuario us, ip.record re WHERE us.cedula= re.cedula  order by (re.puntos) DESC", conexionRetorno);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {

                while (dr.Read())
                {

                    DataRow datarow1 = dt.NewRow();
                    datarow1["Nombre"] = (dr.GetString(0));
                    datarow1["Tipo"] = (dr.GetString(1));
                    datarow1["Puntos"] = (dr.GetString(2));
                    datarow1["Cedula"] = (dr.GetString(3));
                    dt.Rows.Add(datarow1);

                    dgv.DataSource = dt;


                }
            }
            dr.Close();
            conexion.conexion.Close();

        }

    }

}
    
