﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Text;

namespace CapaDatos
{
   public class CDImagen
    {

        public NpgsqlCommand cmd;
        public NpgsqlConnection conexionRetorno;
        Conexion conexion = new Conexion();
        public void cargarurlImagen(Dictionary<string, string> dic)
        {
            conexionRetorno = conexion.ConexionBD();
            
            conexion.conexion.Open();

            NpgsqlCommand cmd = new NpgsqlCommand($"SELECT isopais,url  from ip.imagen", conexionRetorno);

            NpgsqlDataReader dr = cmd.ExecuteReader();


            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    dic.Add(dr.GetString(0), dr.GetString(1));

                }

            }
            conexion.conexion.Close();
        }
    }
}
