﻿
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaDatos
{
   public class CDUsuario
    {

        public NpgsqlConnection conexionRetorno;
        Conexion conexion = new Conexion();

        public List<string> seleccionarUsuario(string cedula, string contrasenna)
        {
            conexionRetorno = conexion.ConexionBD();
            conexion.conexion.Open();
            NpgsqlCommand cmd = new NpgsqlCommand($"SELECT  nombreCompleto,cedula,fecha,contrasenna, isoPais FROM ip.usuario WHERE cedula ='{cedula}'", conexionRetorno);

            NpgsqlDataReader dr = cmd.ExecuteReader();


            List<string> DatosUsuario = new List<string>();
           

            if (dr.HasRows)
            {
                while (dr.Read())
                {

                
                    if (cedula == dr.GetString(1) && contrasenna== dr.GetString(3))
                    {

                       DatosUsuario.Add( dr.GetString(0));
                       DatosUsuario.Add(dr.GetString(1));
                       DatosUsuario.Add( dr.GetString(2));
                       DatosUsuario.Add( dr.GetString(3));
                       DatosUsuario.Add(dr.GetString(4));
                    
                    }
                    else
                    {
                        MessageBox.Show("Contraseña no coincide con el usuario");
                       
                    }

                }

            }
            else {

                MessageBox.Show("Usuario NO encontrado");
             
            }
            conexion.conexion.Close();
            return DatosUsuario;
        }



        public void registrarUsuario(string nombre, string cedula, string fecha, string contrasenna, string isoPais)
        {
          
            conexionRetorno = conexion.ConexionBD();
            conexion.conexion.Open();
            NpgsqlCommand cmd = new NpgsqlCommand($"INSERT INTO ip.usuario (nombrecompleto,cedula,fecha,contrasenna,isoPais)VALUES('{nombre}','{cedula}','{fecha}','{contrasenna}','{isoPais}');", conexionRetorno);
            cmd.ExecuteNonQuery();
            conexion.conexion.Close();
        }


        }

    }

