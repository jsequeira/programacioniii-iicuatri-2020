﻿
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WebService;
namespace CapaDatos
{
   public class CDCargarDatos
    {


        public NpgsqlCommand cmd;
        public NpgsqlConnection conexionRetorno;
        Conexion conexion = new Conexion();


        public void cargarContinentesBD() {




            conexionRetorno = conexion.ConexionBD();
            conexion.conexion.Open();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT * FROM ip.continente ", conexionRetorno);
         
            NpgsqlDataReader dr = cmd.ExecuteReader();

       

            if (dr.HasRows)
            {
                Console.WriteLine("Los Datos de la base de datos estan cargados(Tabla Continente)");

                dr.Close();

            }
         
            else {
                dr.Close();
                Dictionary<string, string> listaContinentes=new Metodos().cargarContinentes();
                
               


                for (int x = 0; x < listaContinentes.Count; x++) {

                    var diccionario = listaContinentes.ElementAt(x);

                    string key = diccionario.Key;

                    string values = diccionario.Value;

                    cmd = new NpgsqlCommand($"INSERT INTO ip.continente(cdgoiso,nombre)VALUES('{key}', '{ values}')", conexionRetorno);

                    cmd.ExecuteNonQuery();
                }

                Console.WriteLine("Se han cargado los datos(Tabla Continente)");

            }
            conexion.conexion.Close();
            dr.Close();

        }

        public void cargarPaisesBD()
        {


            conexionRetorno = conexion.ConexionBD();
            conexion.conexion.Open();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT * FROM ip.pais ", conexionRetorno);

            NpgsqlDataReader dr = cmd.ExecuteReader();

         

            if (dr.HasRows)
            {
                Console.WriteLine("Los Datos de la base de datos estan cargados(Tabla Pais)");
                dr.Close();
            }
            else
            {
                MessageBox.Show("Por favor espere mientras se cargan los Datos","Mensaje de Carga");
                dr.Close();
                Dictionary<string, Dictionary<string,string>> dicPaises = new Metodos().cargarPaises();

               
                conexionRetorno = conexion.ConexionBD();
                
                string isoContinente = "";
                string isoPais = "";
                string nombrePais = "";
                string url;

             


                foreach (var dic in dicPaises)
                {


                    isoContinente = dic.Key;
                   
                    Dictionary<string, string> paises = dic.Value;

                    foreach (var pais in paises)
                    {

                        isoPais = pais.Key;

                        nombrePais = pais.Value.Replace("'","''");

                        url = new Metodos().cargarImagen(isoPais);
                        conexionRetorno = conexion.ConexionBD();
                        
                        conexion.conexion.Open();
                        cmd = new NpgsqlCommand($"INSERT INTO ip.pais(id_continente,cdgoiso,nombre )VALUES('{isoContinente}', '{isoPais}','{nombrePais}')", conexionRetorno);
                        cmd.ExecuteNonQuery();
                        conexion.conexion.Close();

                        conexion.conexion.Open();
                        cmd = new NpgsqlCommand($"INSERT INTO ip.imagen(isoPais,url )VALUES('{isoPais}','{url}')", conexionRetorno); 
                        cmd.ExecuteNonQuery();
                        conexion.conexion.Close();

                    }

                        

                   }

                Console.WriteLine("Se han cargado los datos(Tabla pais)");
                Console.WriteLine("Se han cargado los datos(Tabla imagen)");

            }
            conexion.conexion.Close();
            dr.Close();
        }

          

      }


}    
    

