﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace CapaDatos
{
   public class CDPais
    {
        public NpgsqlCommand cmd;
        public NpgsqlConnection conexionRetorno;
        Conexion conexion = new Conexion();

    
        public void cargarPaises(Dictionary<string, string> dic,string cdgoContinente)
        {
            conexionRetorno = conexion.ConexionBD();
            conexion.conexion.Open();
            NpgsqlCommand cmd = new NpgsqlCommand($"SELECT cdgoiso,nombre  from ip.pais where id_continente='{cdgoContinente}'", conexionRetorno);

            NpgsqlDataReader dr = cmd.ExecuteReader();

         


            if (dr.HasRows)
            {
                while (dr.Read())
                {
          
                    dic.Add(dr.GetString(1), dr.GetString(0));

                    Console.WriteLine(dr.GetString(1) + dr.GetString(0));                
                
                }

            }
            conexion.conexion.Close();
        }

    }
}
