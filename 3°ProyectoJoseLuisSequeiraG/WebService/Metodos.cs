﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WebService.org.oorsprong.webservices;

namespace WebService
{
    public class Metodos
    {

        Dictionary<string, string> listaContinentes;
        Dictionary<string, Dictionary<string, string>>diccontinentespaises;
        Dictionary<string, string> dicPaises;
        List<string> listaPaises;
        CountryInfoService cis = new CountryInfoService();

        public Dictionary<string, string> cargarContinentes()
        {


            listaContinentes = new Dictionary<string, string>();
            

            tContinent[] continentes = cis.ListOfContinentsByName();

            // org.oorsprong.webservices.tContinent con = new org.oorsprong.webservices.tContinent().sName();

            foreach (tContinent c in continentes)
            {

                listaContinentes.Add(c.sCode, c.sName);

            }

            return listaContinentes;
        }

        public Dictionary<string, Dictionary<string,string>> cargarPaises()
        {
           diccontinentespaises = new Dictionary<string, Dictionary<string, string>>();
          

            string continente = "";
            string cdgoPais = "";
            string nombrePais = "";


            tCountryCodeAndNameGroupedByContinent[] continentes2 = cis.ListOfCountryNamesGroupedByContinent();


            foreach (tCountryCodeAndNameGroupedByContinent c1 in continentes2)
            {


                continente = c1.Continent.sCode;

                dicPaises = new Dictionary<string, string>();
                foreach (tCountryCodeAndName c2 in c1.CountryCodeAndNames)
                {

                    cdgoPais = c2.sISOCode;
                   
                    nombrePais= c2.sName;
                   // MessageBox.Show(continente);
                    //MessageBox.Show(nombrePais);
                    dicPaises.Add(cdgoPais,nombrePais);
                }

                diccontinentespaises.Add(continente,dicPaises);
               
            }

            return diccontinentespaises;
        }

        public string cargarImagen(string isoPais) {
            string urlImagen=cis.CountryFlag(isoPais);
            return urlImagen;
        }


        public void cargarPais(TextBox nombre,TextBox capital,TextBox moneda, TextBox lenguaje,string iso) {


            try
            {
                tCountryInfo info = cis.FullCountryInfo(iso);

                nombre.Text = info.sName;

                capital.Text = info.sCapitalCity;

                string monedaIso = info.sCurrencyISOCode;



                string cur = cis.CurrencyName(monedaIso);

                moneda.Text = cur;

                tLanguage lenguajes = info.Languages.ElementAt(0);

                lenguaje.Text = lenguajes.sName;
            }
            catch {

                lenguaje.Text ="No encontrado";

            }
        }
    }
    
}

