﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Npgsql;

namespace CapaDatos
{
    public class CDAerolinea
    {

        public NpgsqlCommand cmd;
        public NpgsqlConnection conexionRetorno;
        Conexion conexion = new Conexion();

        public void seleccionarAerolineas(DataGridView dgv)
        {
            conexionRetorno = conexion.ConexionBD();

            NpgsqlCommand cmd = new NpgsqlCommand($"SELECT id_aerolinea,nombre,annio,tipo  FROM vl.aerolinea ", conexionRetorno);

            NpgsqlDataReader dr = cmd.ExecuteReader();

            DataTable datatable = (DataTable)dgv.DataSource;


            if (dr.HasRows)
            {
                while (dr.Read())
                {

 
                    DataRow datarow1 = datatable.NewRow();
                    datarow1["Id_aerolinea"] = dr.GetInt32(0).ToString();
                    datarow1["Nombre"] = dr.GetString(1);
                    datarow1["Año"] = dr.GetString(2);
                    datarow1["Tipo"] = dr.GetString(3);
                    datatable.Rows.Add(datarow1);
                    dgv.DataSource = datatable;


                }

            }
            dr.Close();
        }



        public void buscarAerolinea(DataGridView dgv,string palabra)
        {
            conexionRetorno = conexion.ConexionBD();

            NpgsqlCommand cmd = new NpgsqlCommand($"SELECT id_aerolinea,nombre,annio,tipo  FROM vl.aerolinea where LOWER(nombre) LIKE LOWER('{palabra}%') ", conexionRetorno);

            NpgsqlDataReader dr = cmd.ExecuteReader();

            DataTable datatable = (DataTable)dgv.DataSource;


            if (dr.HasRows)
            {
                while (dr.Read())
                {


                    DataRow datarow1 = datatable.NewRow();
                    datarow1["Id_aerolinea"] = dr.GetInt32(0).ToString();
                    datarow1["Nombre"] = dr.GetString(1);
                    datarow1["Año"] = dr.GetString(2);
                    datarow1["Tipo"] = dr.GetString(3);
                    datatable.Rows.Add(datarow1);
                    dgv.DataSource = datatable;


                }

            }
        }



        public void insertarAerolinea(string nombre,string annio,string tipo) {


            conexionRetorno = conexion.ConexionBD();

            NpgsqlCommand cmd = new NpgsqlCommand($"INSERT INTO vl.aerolinea ( nombre,annio,tipo)VALUES('{nombre}','{annio}','{tipo}');", conexionRetorno);

            cmd.ExecuteNonQuery();


        }



    }
}