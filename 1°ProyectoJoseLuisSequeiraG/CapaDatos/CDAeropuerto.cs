﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Npgsql;

namespace CapaDatos
{
   public class CDAeropuerto
    {


        public NpgsqlCommand cmd;
        public NpgsqlConnection conexionRetorno;
        Conexion conexion = new Conexion();

        public void insertarAeropuerto(string IATA, string nombre, string pais)
        {


            conexionRetorno = conexion.ConexionBD();

            NpgsqlCommand cmd = new NpgsqlCommand($"INSERT INTO vl.aeropuerto ( iata,nombre,pais)VALUES('{IATA}','{nombre}','{pais}');", conexionRetorno);

            cmd.ExecuteNonQuery();


        }

        public void seleccionarAeropuertos(DataGridView dgvSalida, DataGridView dgvEntrada)
        {
            conexionRetorno = conexion.ConexionBD();

            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id_aeropuerto, iata, nombre, pais  FROM vl.aeropuerto ", conexionRetorno);

            NpgsqlDataReader dr = cmd.ExecuteReader();

            DataTable datatablesda = (DataTable)dgvSalida.DataSource;

            DataTable datatablelda = (DataTable)dgvEntrada.DataSource;

            if (dr.HasRows)
            {
                while (dr.Read())
                {


                    DataRow datarow1 = datatablesda.NewRow();
                    datarow1["Id_aeropuerto"] = dr.GetInt32(0).ToString();
                    datarow1["IATA"] = dr.GetString(1);
                    datarow1["Nombre"] = dr.GetString(2);
                    datarow1["Pais"] = dr.GetString(3);

                    DataRow datarow2 = datatablelda.NewRow();
                    datarow2["Id_aeropuerto"] = dr.GetInt32(0).ToString();
                    datarow2["IATA"] = dr.GetString(1);
                    datarow2["Nombre"] = dr.GetString(2);
                    datarow2["Pais"] = dr.GetString(3);


                    datatablesda.Rows.Add(datarow1);

                    datatablelda.Rows.Add(datarow2);

                    dgvSalida.DataSource = datatablesda;

                    dgvEntrada.DataSource = datatablelda;


                }

            }
            dr.Close();
        }


    }
}
