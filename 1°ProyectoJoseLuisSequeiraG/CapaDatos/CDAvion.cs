﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Npgsql;
namespace CapaDatos
{
    public class CDAvion
    {
        int id_aerolinea=0;
        string modelo="";

        public NpgsqlCommand cmd;
        public NpgsqlConnection conexionRetorno;
        Conexion conexion = new Conexion();
      
        
        public void seleccionarAvion(DataGridView dgv)
        {
         
        
            conexionRetorno = conexion.ConexionBD();

            NpgsqlCommand cmd = new NpgsqlCommand($"SELECT id_avion,modelo,annio_construccion,id_aerolinea,capacidad,estado  FROM vl.avion ", conexionRetorno);

            NpgsqlDataReader dr = cmd.ExecuteReader();

            DataTable datatable = (DataTable)dgv.DataSource;


            if (dr.HasRows)
            {
                while (dr.Read())
                {
                  

                    DataRow datarow1 = datatable.NewRow();
                   
                    datarow1["Id_avion"] = dr.GetInt32(0).ToString();
                    datarow1["Modelo"] = dr.GetString(1);
                    datarow1["Annio_construccion"] = dr.GetString(2);
                    datarow1["Id_aerolinea"] = dr.GetInt32(3); 
                    datarow1["Capacidad"] = dr.GetString(4);
                    datarow1["Estado"] = dr.GetString(5);              
                    
                    datatable.Rows.Add(datarow1);
                    dgv.DataSource = datatable;

                }

            }
            dr.Close();
        }


        public void insertarAvion( string modelo, string annio,int id_aerolinea,int capacidad,string estado)
        {


            conexionRetorno = conexion.ConexionBD();

            NpgsqlCommand cmd = new NpgsqlCommand($"INSERT INTO vl.avion (modelo,annio_construccion,id_aerolinea,capacidad,estado)VALUES('{modelo}','{annio}','{id_aerolinea}','{capacidad}','{estado}');", conexionRetorno);

            cmd.ExecuteNonQuery();


        }

        public void seleccionarAvioncantidad(DataGridView dgv)
        {


            conexionRetorno = conexion.ConexionBD();

            NpgsqlCommand cmd = new NpgsqlCommand($"SELECT  a.modelo, count( a.modelo ) FROM vl.avion as ajoin vl.historial as hon h.id_avion = a.id_avionGROUP BY a.modeloORDER BY count(a.modelo)DESC LIMIT 3", conexionRetorno);

            NpgsqlDataReader dr = cmd.ExecuteReader();

            DataTable datatable = (DataTable)dgv.DataSource;


            if (dr.HasRows)
            {
                while (dr.Read())
                {


                    DataRow datarow1 = datatable.NewRow();

                    datarow1["Id_avion"] = dr.GetInt32(0).ToString();
                    datarow1["Modelo"] = dr.GetString(1);
                    datarow1["Annio_construccion"] = dr.GetString(2);
                    datarow1["Id_aerolinea"] = dr.GetInt32(3);
                    datarow1["Capacidad"] = dr.GetString(4);
                    datarow1["Estado"] = dr.GetString(5);

                    datatable.Rows.Add(datarow1);
                    dgv.DataSource = datatable;

                }

            }
            dr.Close();
        }




    }
}
