﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Npgsql;


namespace CapaDatos
{
    public class CDVuelo
    {

        public NpgsqlCommand cmd;
        public NpgsqlConnection conexionRetorno;
        Conexion conexion = new Conexion();

        int precioMnor = 0;
        

    


        public void insertarAerolinea(int id_aerolinea, int precio, string fechahora_sda, string aeropuerto_sda, string fechahora_lda, string aeropuerto_lda,int duracion ,int idPiloto1, int idPiloto2, int idServicio1, int idServicio2, int idServicio3, int id_avion)
        {


            conexionRetorno = conexion.ConexionBD();

            NpgsqlCommand cmd = new NpgsqlCommand($"INSERT INTO vl.vuelo (id_aerolinea,precio,fechahora_sda,aeropuerto_sda,fechahora_lda,aeropuerto_lda,duracion," +
                $"id_piloto1,id_piloto2,id_servicio1,id_servicio2,id_servicio3,id_avion)VALUES('{id_aerolinea}','{precio}','{fechahora_sda}','{aeropuerto_sda}','{fechahora_lda}','{aeropuerto_lda}','{duracion}','{idPiloto1}','{idPiloto2}','{idServicio1}','{idServicio2}','{idServicio3}','{ id_avion}');", conexionRetorno);

            cmd.ExecuteNonQuery();

        }

        public void reportePorfecha(DataGridView dgv, string fechaInicial, string fechaFinal)
        {
            conexionRetorno = conexion.ConexionBD();

            NpgsqlCommand cmd = new NpgsqlCommand($"select id_aerolinea,precio,fechahora_sda,aeropuerto_sda,fechahora_lda,aeropuerto_lda,id_avion,id_piloto1,id_piloto2,id_servicio1,id_servicio2,id_servicio3  from vl.vuelo where  fechahora_sda ::timestamp between '{fechaInicial}' and '{fechaFinal}'", conexionRetorno);

            NpgsqlDataReader dr = cmd.ExecuteReader();

            DataTable datatable = (DataTable)dgv.DataSource;


            if (dr.HasRows)
            {
                while (dr.Read())
                {


                    DataRow datarow1 = datatable.NewRow();
                    datarow1["Id_aerolinea"] = dr.GetInt32(0).ToString();
                    datarow1["Precio"] = dr.GetInt32(1).ToString();
                    datarow1["Fecha Salida"] = dr.GetString(2);
                    datarow1["Aeropuerto salida"] = dr.GetString(3).ToString();
                    datarow1["Fecha llegada"] = dr.GetString(4);
                    datarow1["Aeropuerto llegada"] = dr.GetString(5);
                    datarow1["Id_avion"] = dr.GetInt32(6).ToString();
                    datarow1["Id_piloto1"] = dr.GetInt32(7).ToString();
                    datarow1["Id_piloto2"] = dr.GetInt32(8).ToString();
                    datarow1["Id_servicio1"] = dr.GetInt32(9).ToString();
                    datarow1["Id_servicio2"] = dr.GetInt32(10).ToString();
                    datarow1["Id_servicio3"] = dr.GetInt32(11).ToString();


                    datatable.Rows.Add(datarow1);
                    dgv.DataSource = datatable;


                }

            }
            dr.Close();
        }
      
        public void seleccionarVuelos(DataGridView dgv)
        {
            conexionRetorno = conexion.ConexionBD();

            NpgsqlCommand cmd = new NpgsqlCommand($"select id_aerolinea,precio,fechahora_sda,aeropuerto_sda,fechahora_lda,aeropuerto_lda,id_avion,id_piloto1,id_piloto2,id_servicio1,id_servicio2,id_servicio3  from vl.vuelo ", conexionRetorno);

            NpgsqlDataReader dr = cmd.ExecuteReader();

            DataTable datatable = (DataTable)dgv.DataSource;


            if (dr.HasRows)
            {
                while (dr.Read())
                {


                    DataRow datarow1 = datatable.NewRow();

                    datarow1["Id_aerolinea"] = dr.GetString(0);
                    datarow1["Precio"] = dr.GetString(1);
                    datarow1["Fecha Salida"] = dr.GetString(2);
                    datarow1["Aeropuerto salida"] = dr.GetString(3);
                    datarow1["Fecha llegada"] = dr.GetString(4);
                    datarow1["Aeropuerto llegada"] = dr.GetString(5);
                    datarow1["Id_avion"] = dr.GetString(6);
                    datarow1["Id_piloto1"] = dr.GetString(7);
                    datarow1["Id_piloto2"] = dr.GetString(8);
                    datarow1["Id_servicio1"] = dr.GetString(9);
                    datarow1["Id_servicio2"] = dr.GetString(10);
                    datarow1["Id_servicio3"] = dr.GetString(11);

                    datatable.Rows.Add(datarow1);
                    dgv.DataSource = datatable;


                }

            }
            dr.Close();
        }

        public void seleccionarPorvuelos(DataGridView dgv)
        {
            conexionRetorno = conexion.ConexionBD();

            NpgsqlCommand cmd = new NpgsqlCommand($"SELECT a.id_avion, a.modelo,a.Annio_construccion,a.capacidad,a.estado, count( a.modelo )  FROM vl.avion as a join vl.historial as h on h.id_avion = a.id_avion GROUP BY a.modelo, a.id_avion,a.Annio_construccion,a.capacidad,a.estado ORDER BY count(a.modelo) DESC LIMIT 3 ", conexionRetorno);

            NpgsqlDataReader dr = cmd.ExecuteReader();

            DataTable datatable = (DataTable)dgv.DataSource;


            if (dr.HasRows)
            {
                while (dr.Read())
                {


                    DataRow datarow1 = datatable.NewRow();
                    datarow1["Id_avion"] = dr.GetString(0).ToString();
                    datarow1["Modelo"] = dr.GetString(1).ToString();
                    datarow1["Annio_construccion"] = dr.GetString(2);
                    datarow1["Capacidad"] = dr.GetInt32(3);
                    datarow1["Estado"] = dr.GetString(4).ToString();
                    datarow1["Numero de vuelos"] = dr.GetInt32(5).ToString();
                    datatable.Rows.Add(datarow1);
                    dgv.DataSource = datatable;


                }


            }
            dr.Close();
        }



        public void seleccionarVuelosBusqueda(CheckBox ckb, DataGridView dgvNormal, DataGridView dgvEscala1, DataGridView dgvEscala2, DataGridView dgvEscala3, Panel panelMain, Panel panelEscala, string aeroSda, string aeroLda, string fechaSalida, string fechaVuelta)
        {
           

            bool operacion = false;

            List<string> listaOrigen = new List<string>();
            List<string> listaDestino = new List<string>();

            List<string> listaO_r = new List<string>();
            List<string> listaR_d = new List<string>();
            List<string> listar_r = new List<string>();
            List<string> listaR_R = new List<string>();


            List<string> listaRuta_Destino = new List<string>();
            List<string> listaRuta_ruta1 = new List<string>();
            List<string> listaRuta_ruta2 = new List<string>();

            conexionRetorno = conexion.ConexionBD();

            NpgsqlCommand cmd = new NpgsqlCommand($"select id_aerolinea,precio,fechahora_sda,aeropuerto_sda,fechahora_lda,aeropuerto_lda ,duracion,id_avion from vl.vuelo Where aeropuerto_sda='{aeroSda}' and aeropuerto_lda='{aeroLda}' and fechahora_sda like '{fechaSalida}%' and fechahora_lda like '{fechaVuelta}%'", conexionRetorno);

            NpgsqlDataReader dr = cmd.ExecuteReader();

            DataTable datatable = (DataTable)dgvNormal.DataSource;


            if (dr.HasRows)
            {

                operacion = true;

                while (dr.Read())
                {

                    DataRow datarow1 = datatable.NewRow();

                    datarow1["Precio"] = dr.GetInt32(1);

                    datarow1["Fecha_Salida"] = dr.GetString(2);

                    datarow1["Aeropuerto salida"] = dr.GetString(3);

                    datarow1["Fecha llegada"] = dr.GetString(4);

                    datarow1["Aeropuerto llegada"] = dr.GetString(5);

                    datarow1["Duracion(hrs)"] = dr.GetString(6);

                    datarow1["id_Avion"] = dr.GetString(7);


                    datatable.Rows.Add(datarow1);

                    dgvNormal.DataSource = datatable;


                }

            }
            
              
                ckb.Checked = false;
                panelEscala.Visible = true;
                panelMain.Visible = false;

                dr.Close();



                NpgsqlCommand cmd2 = new NpgsqlCommand($"select id_aerolinea,precio,fechahora_sda,aeropuerto_sda,fechahora_lda,aeropuerto_lda,duracion,id_avion   from vl.vuelo Where  aeropuerto_lda='{aeroLda}'", conexionRetorno);

                NpgsqlDataReader dr2 = cmd2.ExecuteReader();


                if (dr2.HasRows)
                {
                    

                    while (dr2.Read())
                    {


                        listaOrigen.Add(dr2.GetString(3));


                    }

                    dr2.Close();



                    datatable = (DataTable)dgvEscala1.DataSource;
                    List<string> listaOrigenDistinct = listaOrigen.Distinct().ToList();

                    for (int x = 0; x < listaOrigenDistinct.Count; x++)
                    {

                        NpgsqlCommand cmd3 = new NpgsqlCommand($"select id_aerolinea,precio,fechahora_sda,aeropuerto_sda,fechahora_lda,aeropuerto_lda,duracion ,id_avion  from vl.vuelo Where  aeropuerto_sda='{aeroSda}' and  aeropuerto_lda='{listaOrigenDistinct[x]}'  and fechahora_sda like '{fechaSalida}%' and fechahora_lda like '{fechaVuelta}%'", conexionRetorno);

                        NpgsqlDataReader dr3 = cmd3.ExecuteReader();


                        if (dr3.HasRows)
                        {
                           

                            while (dr3.Read())
                            {
                                

                                DataRow datarow1 = datatable.NewRow();

                                datarow1["Precio"] = dr3.GetInt32(1);

                                datarow1["Fecha_Salida"] = dr3.GetString(2);

                                datarow1["Aeropuerto salida"] = dr3.GetString(3);

                                datarow1["Fecha llegada"] = dr3.GetString(4);

                                datarow1["Aeropuerto llegada"] = dr3.GetString(5);

                                datarow1["Duracion(hrs)"] = dr3.GetString(6);
                                 
                                datarow1["id_Avion"] = dr3.GetString(7);

                            datatable.Rows.Add(datarow1);

                                dgvEscala1.DataSource = datatable;

                                listaDestino.Add(dr3.GetString(5));


                            }
                        }


                        dr3.Close();
                    }



                    datatable = (DataTable)dgvEscala2.DataSource;
                    List<string> listaDestinoDistinct = listaDestino.Distinct().ToList(); //4700 ticks

                    for (int x = 0; x < listaDestinoDistinct.Count; x++)
                    {

                        NpgsqlCommand cmd4 = new NpgsqlCommand($"select id_aerolinea,precio,fechahora_sda,aeropuerto_sda,fechahora_lda,aeropuerto_lda,duracion,id_avion  from vl.vuelo Where  aeropuerto_sda='{listaDestinoDistinct[x]}' and  aeropuerto_lda='{aeroLda}'  and fechahora_sda like '{fechaSalida}%' and fechahora_lda like '{fechaVuelta}%'", conexionRetorno);

                        NpgsqlDataReader dr4 = cmd4.ExecuteReader();


                        if (dr4.HasRows)
                        {
                            operacion = true;

                            MessageBox.Show("Paso 4");
                            while (dr4.Read())
                            {


                                DataRow datarow1 = datatable.NewRow();

                                datarow1["Precio"] = dr4.GetInt32(1);

                                datarow1["Fecha_Salida"] = dr4.GetString(2);

                                datarow1["Aeropuerto salida"] = dr4.GetString(3);

                                datarow1["Fecha llegada"] = dr4.GetString(4);

                                datarow1["Aeropuerto llegada"] = dr4.GetString(5);

                                datarow1["Duracion(hrs)"] = dr4.GetString(6);

                                datarow1["id_Avion"] = dr4.GetString(7);

                            datatable.Rows.Add(datarow1);

                                dgvEscala2.DataSource = datatable;

                                listaDestino.Add(dr4.GetString(5));
                            }
                           
                        }
                    dr4.Close();
                }
          
            }
            dr2.Close();  

            if( operacion == false){

                generarVuelo(ckb, dgvNormal, dgvEscala1, dgvEscala2,dgvEscala3, panelMain, panelEscala, aeroSda, aeroLda,fechaSalida,fechaVuelta);


                }



            }


        public void generarVuelo(CheckBox ckb, DataGridView dgvNormal, DataGridView dgvEscala1, DataGridView dgvEscala2, DataGridView dvgEscala3, Panel panelNormal, Panel panelEscala, string aeroSda, string aeroLda, string fechaSalida, string fechaVuelta) {

            List<string> listaO_r = new List<string>();
            List<string> listaR_d = new List<string>();
            List<string> listar_r = new List<string>();
            List<string> listaR_R = new List<string>();


            List<string> listaRuta_Destino = new List<string>();
            List<string> listaRuta_ruta1 = new List<string>();
            List<string> listaRuta_ruta2 = new List<string>();





            NpgsqlCommand cmd5 = new NpgsqlCommand($"select id_aerolinea,precio,fechahora_sda,aeropuerto_sda,fechahora_lda,aeropuerto_lda,duracion,id_avion  from vl.vuelo Where  aeropuerto_Sda='{aeroSda}'", conexionRetorno);

            NpgsqlDataReader dr5 = cmd5.ExecuteReader();


            if (dr5.HasRows)
            {
               

                while (dr5.Read())
                {


                    listaO_r.Add(dr5.GetString(5));




                }
            }

            dr5.Close();


            /* List<string> DistinctO_r = listaO_r.Distinct().ToList();


             for (int x1 = 0; x1 < DistinctO_r.Count; x1++)
             {

                 NpgsqlCommand cmd6 = new NpgsqlCommand($"select id_aerolinea,precio,fechahora_sda,aeropuerto_sda,fechahora_lda,aeropuerto_lda,duracion  from vl.vuelo Where  aeropuerto_Sda='{ DistinctO_r[x1]}'", conexionRetorno);

                 NpgsqlDataReader dr6 = cmd6.ExecuteReader();


                 if (dr6.HasRows)
                 {
             MessageBox.Show("paso22");

             while (dr6.Read())
                     {


                         listar_r.Add(dr6.GetString(5));


                     }

                 }
         dr6.Close();

             }
            */

            NpgsqlCommand cmd7 = new NpgsqlCommand($"select id_aerolinea,precio,fechahora_sda,aeropuerto_sda,fechahora_lda,aeropuerto_lda,duracion,id_avion  from vl.vuelo Where  aeropuerto_lda='{aeroLda}'", conexionRetorno);

            NpgsqlDataReader dr7 = cmd7.ExecuteReader();


            if (dr7.HasRows)
            {
               

                while (dr7.Read())
                {


                    listaR_d.Add(dr7.GetString(3));


                }

            }
            dr7.Close();



          /* List<string> Distinctr_r = listar_r.Distinct().ToList();*/
           
             List<string> DistinctO_r = listaO_r.Distinct().ToList();
             List<string> DistinctR_d = listaR_d.Distinct().ToList(); 

                    for (int x2 = 0; x2 < DistinctR_d.Count; x2++)
                    {


                        for (int x3 = 0; x3 < DistinctO_r.Count; x3++)
                        {


                            NpgsqlCommand cmd8 = new NpgsqlCommand($"select id_aerolinea,precio,fechahora_sda,aeropuerto_sda,fechahora_lda,aeropuerto_lda,duracion,id_avion  from vl.vuelo Where  aeropuerto_Sda='{DistinctO_r[x3]}'  and aeropuerto_Lda='{DistinctR_d[x2]}' ", conexionRetorno);

                            NpgsqlDataReader dr8 = cmd8.ExecuteReader();


                            if (dr8.HasRows)
                            {
                      

                        while (dr8.Read())
                                {


                                    listaRuta_Destino.Add(dr8.GetString(5));
                                    listaRuta_ruta1.Add(dr8.GetString(3));


                                }

                            }
                    dr8.Close();
                    
                }

                    }


                    List<string> DistinctRuta_ruta1 = listaRuta_ruta1.Distinct().ToList();
            /*List<string> DistinctO_r = listaO_r.Distinct().ToList();*/

            List<string> DistinctlistaRuta_Destino = listaRuta_Destino.Distinct().ToList();


           /* for (int i = 0; i < DistinctRuta_ruta1.Count; i++)
                    {


                        for (int j = 0; j < DistinctO_r.Count; j++)
                        {


                            NpgsqlCommand cmd8 = new NpgsqlCommand($"select id_aerolinea,precio,fechahora_sda,aeropuerto_sda,fechahora_lda,aeropuerto_lda,duracion  from vl.vuelo Where  aeropuerto_Sda='{ DistinctO_r[j]}'  and aeropuerto_Lda='{ DistinctRuta_ruta1[i]}' ", conexionRetorno);

                            NpgsqlDataReader dr8 = cmd8.ExecuteReader();


                            if (dr8.HasRows)
                            {

                        MessageBox.Show("paso55");
                        while (dr8.Read())
                                {

                                    listaRuta_ruta2.Add(dr8.GetString(3));



                                }

                            }
                    dr8.Close();
                        }
                    }*/

                    List<string> DistinctRuta_ruta2 = listaRuta_ruta2.Distinct().ToList(); ;
                     DataTable datatable = (DataTable)dgvEscala1.DataSource;
               
                for (int a = 0; a < DistinctRuta_ruta1.Count; a++)
                    {

                        NpgsqlCommand newcmd = new NpgsqlCommand($"select id_aerolinea,precio,fechahora_sda,aeropuerto_sda,fechahora_lda,aeropuerto_lda,duracion,id_avion  from vl.vuelo Where  aeropuerto_sda='{aeroSda}' and  aeropuerto_lda='{DistinctRuta_ruta1[a]}'  and fechahora_sda like '{fechaSalida}%' and fechahora_lda like '{fechaVuelta}%'", conexionRetorno);

                        NpgsqlDataReader newdr = newcmd.ExecuteReader();


                        if (newdr.HasRows)
                        {
                   
                    while (newdr.Read())
                            {


                                DataRow datarow1 = datatable.NewRow();

                                datarow1["Precio"] = newdr.GetInt32(1);

                                datarow1["Fecha_Salida"] = newdr.GetString(2);

                                datarow1["Aeropuerto salida"] = newdr.GetString(3);

                                datarow1["Fecha llegada"] = newdr.GetString(4);

                                datarow1["Aeropuerto llegada"] = newdr.GetString(5);

                                datarow1["Duracion(hrs)"] = newdr.GetString(6);
                               
                                datarow1["id_Avion"] = newdr.GetString(7);

                        datatable.Rows.Add(datarow1);

                                dgvEscala1.DataSource = datatable;




                            }
                           
                        }
                newdr.Close();

                    }



                    for (int a1 = 0; a1 < DistinctRuta_ruta1.Count; a1++)
                    {
                        for (int a2 = 0; a2 < DistinctlistaRuta_Destino.Count; a2++)
                        {

                            NpgsqlCommand newcmd1 = new NpgsqlCommand($"select id_aerolinea,precio,fechahora_sda,aeropuerto_sda,fechahora_lda,aeropuerto_lda,duracion,id_avion  from vl.vuelo Where  aeropuerto_sda='{ DistinctRuta_ruta1[a1]}' and  aeropuerto_lda='{ DistinctlistaRuta_Destino[a2]}'  and fechahora_sda like '{fechaSalida}%' and fechahora_lda like '{fechaVuelta}%'", conexionRetorno);

                            NpgsqlDataReader newdr1 = newcmd1.ExecuteReader();
                           datatable = (DataTable)dgvEscala2.DataSource;

                        if (newdr1.HasRows)
                            {
                       
                        while (newdr1.Read())
                                {


                                    DataRow datarow1 = datatable.NewRow();

                                    datarow1["Precio"] = newdr1.GetInt32(1);

                                    datarow1["Fecha_Salida"] = newdr1.GetString(2);

                                    datarow1["Aeropuerto salida"] = newdr1.GetString(3);

                                    datarow1["Fecha llegada"] = newdr1.GetString(4);

                                    datarow1["Aeropuerto llegada"] = newdr1.GetString(5);

                                    datarow1["Duracion(hrs)"] = newdr1.GetString(6);
                           
                                    datarow1["id_Avion"] = newdr1.GetString(7);


                            datatable.Rows.Add(datarow1);

                                    dgvEscala2.DataSource = datatable;




                                }
                           
                            }

                    newdr1.Close();


                }

                    }





                    for (int a3 = 0; a3 < DistinctRuta_ruta1.Count; a3++)
                    {

                        NpgsqlCommand newcmd3 = new NpgsqlCommand($"select id_aerolinea,precio,fechahora_sda,aeropuerto_sda,fechahora_lda,aeropuerto_lda,duracion,id_avion  from vl.vuelo Where  aeropuerto_sda='{DistinctlistaRuta_Destino[a3]}' and  aeropuerto_lda='{aeroLda}'  and fechahora_sda like '{fechaSalida}%' and fechahora_lda like '{fechaVuelta}%' ", conexionRetorno);

                        NpgsqlDataReader newdr3 = newcmd3.ExecuteReader();

                        datatable = (DataTable)dvgEscala3.DataSource;

                    if (newdr3.HasRows)
                        {
                  
                    while (newdr3.Read())
                            {


                                DataRow datarow1 = datatable.NewRow();

                                datarow1["Precio"] = newdr3.GetInt32(1);

                                datarow1["Fecha_Salida"] = newdr3.GetString(2);

                                datarow1["Aeropuerto salida"] = newdr3.GetString(3);

                                datarow1["Fecha llegada"] = newdr3.GetString(4);

                                datarow1["Aeropuerto llegada"] = newdr3.GetString(5);

                                datarow1["Duracion(hrs)"] = newdr3.GetString(6);

                                datarow1["id_Avion"] = newdr3.GetString(7);

                        datatable.Rows.Add(datarow1);

                                 dvgEscala3.DataSource = datatable;




                            }
                            
                        }
                newdr3.Close();
            }




                }



       public void seleccionaVuelosInteligente(DataGridView dgv , string aeroSda, string aeroLda) {


            conexionRetorno = conexion.ConexionBD();

            NpgsqlCommand cmd = new NpgsqlCommand($"select precio, fechahora_sda, fechahora_lda  from vl.vuelo   Where  aeropuerto_sda='{aeroSda}' and aeropuerto_lda='{aeroLda}'", conexionRetorno);

           
            NpgsqlDataReader dr = cmd.ExecuteReader();


            DataTable datatable = (DataTable)dgv.DataSource;


            if (dr.HasRows)
            {


                while (dr.Read())
                {

                    DataRow datarow1 = datatable.NewRow();

                    datarow1["Precio"] = dr.GetInt32(0).ToString();

                    datarow1["Fecha_ini"] = dr.GetString(1).ToString();

                    datarow1["Fecha_fin"] = dr.GetString(2).ToString();
                   
                    datatable.Rows.Add(datarow1);

                    dgv.DataSource = datatable;


                }

               
            }
            dr.Close();

   
        }

        public int obtenerPrecioMenor() {

            return this.precioMnor;

        }

            }

        }
    
