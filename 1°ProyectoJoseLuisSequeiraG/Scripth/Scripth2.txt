CREATE DATABASE Vuelo;

CREATE SCHEMA vl;

CREATE table vl.usuario(

id_usuario serial  primary key not null,
cedula numeric not null,
nombre text not null,
edad numeric not null,
contrasenna text not null,
tipo text not null

);


CREATE table vl.aerolinea(

id_aerolinea serial  primary key  not null, 
nombre text not null,
annio text not null,
tipo text not null

);


CREATE table vl.tripulacion(

id_tripulacion serial  primary key  not null,
cedula numeric not null,
nombre text not null,
fecha_nacimiento date not null,
id_aerolinea integer not null,
rol text not null,
estado text not null

);



CREATE table vl.avion(

id_avion serial  primary key  not null,
modelo text not null,
annio_construccion text not null,
id_aerolinea integer not null,
capacidad integer not null,
estado text not null

);


CREATE table vl.aeropuerto(

id_aeropuerto serial primary key  not null,
IATA text not null,
nombre text not null,
pais text not null


);

CREATE table vl.vuelo(

id_vuelo serial  primary key  not null,
id_aerolinea integer not null,
precio numeric  not null,
fechahora_sda text not null,
aeropuerto_sda text not null,
fechahora_lda text not null,
aeropuerto_lda text not null,
duracion numeric  not null,
id_avion integer not null,
id_piloto1 integer not null,
id_piloto2 integer not null,
id_servicio1 integer not null,
id_servicio2 integer not null,
id_servicio3 integer not null
);


CREATE table vl.historial(

Id_historial serial  primary key ,
cedula integer  not null,
pais_salida text not null,
pais_llegada text not null,
pais_escala1 text not null,
pais_escala2 text not null,
fecha_compra text not null,
duracion_viaje text not null, 
costo numeric  not null,
id_avion integer not null

);

 alter table vl.historial
 add constraint FK_historial_id_avion
 foreign key (id_avion)
 references vl.avion (id_avion);

----------------------------INSERT VUELOS---------------------

INSERT INTO vl.tripulacion(
	 cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
	VALUES (1, 'jose', '21-04-1996',1, 'Piloto', 'Disponible');
	
INSERT INTO vl.tripulacion(
	 cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
	VALUES (2, 'jose', '21-04-1996',1, 'Piloto', 'Disponible');
	
	
INSERT INTO vl.tripulacion(
	 cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
	VALUES (3, 'Luis', '21-04-1996',1, 'Servicio al cliente', 'Disponible');
	
	

INSERT INTO vl.tripulacion(
	 cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
	VALUES (4, 'Luis', '21-04-1996',1, 'Servicio al cliente', 'Disponible');
	

INSERT INTO vl.tripulacion(
	 cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
	VALUES (5, 'Luis', '21-04-1996',1, 'Servicio al cliente', 'Disponible');


alter table vl.tripulacion
  add constraint FK_tripulacion_id_aerolinea
  foreign key (id_aerolinea)
  references vl.aerolinea (id_aerolinea);
  




INSERT into vl.usuario ( CEDULA,nombre,edad,contrasenna,tipo)VALUES(207530479,'Jose sequeira',24,123456,'Pasajero');
INSERT into vl.usuario ( CEDULA,nombre,edad,contrasenna,tipo)VALUES(207530479,'JoseLuis',24,123456,'Administrador');
-------------------------------------------------------------------------------------------

   
-------------------------------------------------------------------------------------------------------
1068; 506

1114; 1102

-----------------------------------------------------------------INSERT Aerolineas----------------------------------------------------
INSERT INTO vl.aerolinea(nombre, annio, tipo)
VALUES ('Aerolíneas Argentinas', '2000', 'Internacional');
	
	
INSERT INTO vl.aerolinea(nombre, annio, tipo)
VALUES ('Copa Airlines Colombia', '2001', 'Internacional');
	
INSERT INTO vl.aerolinea(nombre, annio, tipo)
VALUES ('SATENA', '2002', 'Internacional');
	
INSERT INTO vl.aerolinea(nombre, annio, tipo)
VALUES ('Sky Airline', '2003', 'Internacional');
	
	
		
INSERT INTO vl.aerolinea(nombre, annio, tipo)
VALUES ('Avianca', '2003', 'Internacional');
	
-----------------------------------------------------------------INSERT Tripulaciones----------------------------------------------------
INSERT INTO vl.tripulacion(
	 cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
	VALUES (281162474, 'juan Soto Sequeira', '21-03-1993',1, 'Piloto', 'Disponible');
	
INSERT INTO vl.tripulacion(
	 cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
	VALUES (215541578, 'PABLO ANGUITA PALENZUELA', '14-06-1989',1, 'Piloto', 'Disponible');
	
	
INSERT INTO vl.tripulacion(
	 cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
	VALUES ( 250776093, 'DANIEL LAGO MARZO', '14-06-1978',1, 'Servicio al cliente', 'Disponible');
	
	

INSERT INTO vl.tripulacion(
	 cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
	VALUES (257884798, 'LUIS MIGUEL CARRILLO AMORES', '05-05-1997',1, 'Servicio al cliente', 'Disponible');





INSERT INTO vl.tripulacion(
	 cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
	VALUES (289219796, 'LUIS MIGUEL CARRETERO FUENTES', '07-03-1993',1, 'Piloto', 'Disponible');


INSERT INTO vl.tripulacion(
	 cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
	VALUES (241563464, 'JOSE MARIA NARANJO HERRADOR', '11-07-1988',1, 'Piloto', 'Disponible');


INSERT INTO vl.tripulacion(
	 cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
	VALUES (278256110, 'ISABEL PARRA MEJIAS','08-08-1991',1, 'Servicio al cliente', 'Disponible');


	

INSERT INTO vl.tripulacion(
	 cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
	VALUES (276363556, 'MARIA CONCEPCION LLANO CASTILLEJO', '01-04-1993',1, 'Servicio al cliente', 'Disponible');


INSERT INTO vl.tripulacion(
	 cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado)
	VALUES (276363556, 'MARIA FERNANDA JUAREZ SOTO', '21-03-1976',1, 'Servicio al cliente', 'Disponible');





alter table vl.tripulacion
  add constraint FK_tripulacion_id_aerolinea
  foreign key (id_aerolinea)
  references vl.aerolinea (id_aerolinea);


-----------------------------------
INSERT INTO vl.aeropuerto(
	iata, nombre, pais)
	VALUES ('SJO', 'Aeropuerto Internacional Juan San María', 'Costa Rica');
	
INSERT INTO vl.aeropuerto(
	iata, nombre, pais)
	VALUES ('PTY', 'Aeropuerto Internacional de Tocumen', 'Panamá');
	
INSERT INTO vl.aeropuerto(
	iata, nombre, pais)
	VALUES ('UIO', 'Aeropuerto Internacional de Quito', 'Ecuador');
	
INSERT INTO vl.aeropuerto(
	iata, nombre, pais)
	VALUES ('AICM', 'Aeropuerto Internacional de la Ciudad de México', 'Mexico');
	
INSERT INTO vl.aeropuerto(
	iata, nombre, pais)
	VALUES ('ATL', 'Aeropuerto Internacional Hartsfield-Jackson', 'Estados Unidos');
	
	INSERT INTO vl.aeropuerto(
	iata, nombre, pais)
	VALUES ('KOA', 'Aeropuerto Internacional de Kona', 'Hawai');

---------------------------------------------
INSERT INTO vl.avion(
	modelo, annio_construccion, id_aerolinea, capacidad, estado)
	VALUES ('Airbus A310-324', '2015', 1, 100, 'En servicio');
	
INSERT INTO vl.avion(
	modelo, annio_construccion, id_aerolinea, capacidad, estado)
	VALUES ('Boeing 727-200Adv', '2014',2, 100, 'En servicio');
	
INSERT INTO vl.avion(
	modelo, annio_construccion, id_aerolinea, capacidad, estado)
	VALUES ('Airbus A310-325ET', '2016', 3, 100, 'En servicio');
	
INSERT INTO vl.avion(
	modelo, annio_construccion, id_aerolinea, capacidad, estado)
	VALUES ('Airbus B523-01', '2020',4, 100, 'En servicio');
	
INSERT INTO vl.avion(
	modelo, annio_construccion, id_aerolinea, capacidad, estado)
	VALUES ('Boeing 887-770Adv', '2019', 5, 100, 'En servicio');
-----------------------------------------------------------------------------------
INSERT INTO vl.usuario(
	 cedula, nombre, edad, contrasenna, tipo)
	VALUES (204350479,'Juan Castro Kell', 24, '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', 'Pasajero');
	
INSERT INTO vl.usuario(
	 cedula, nombre, edad, contrasenna, tipo)
	VALUES (204330464,'Ramiro Ramirez Shang', 24, 'e6757959da8eff84c42d4df125b44eb40143dff452afd56aea5cfa058f245028', 'Pasajero');
	
INSERT INTO vl.usuario(
	 cedula, nombre, edad, contrasenna, tipo)
	VALUES (202550479,'Pedro Soto Hang', 48, '3fb0a50e69a3bd10bd006726cf744fa50e779bd652b0dda9733137d78af42de5', 'Pasajero');
	
INSERT INTO vl.usuario(
	 cedula, nombre, edad, contrasenna, tipo)
	VALUES (207530479,'Admin', 24, 'ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f', 'Admin');
	
INSERT INTO vl.usuario(
	 cedula, nombre, edad, contrasenna, tipo)
	VALUES (203330444,'Jose Luis Sequeira Góngora', 24, '4006bedc516ba6d44c054d254d7ce04d9a483722f2ad955bee97462734b85036', 'Pasajero');
-------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO vl.historial(
	 cedula, pais_salida, pais_llegada, pais_escala1, pais_escala2, fecha_compra, duracion_viaje, costo, id_avion)
	VALUES ( 203330444, 'Costa Rica', 'Panama', 'Ninguna','Ninguna',21/05/2020, 1, 100000, 1);
	
	INSERT INTO vl.historial(
	 cedula, pais_salida, pais_llegada, pais_escala1, pais_escala2, fecha_compra, duracion_viaje, costo, id_avion)
	VALUES ( 203330444, 'Costa Rica', 'Panama', 'Ninguna','Ninguna',21/05/2020, 1, 100000, 2);
	
	INSERT INTO vl.historial(
	 cedula, pais_salida, pais_llegada, pais_escala1, pais_escala2, fecha_compra, duracion_viaje, costo, id_avion)
	VALUES ( 203330444, 'Costa Rica', 'Panama', 'Ninguna','Ninguna',21/05/2020, 1, 100000, 3);
	
	INSERT INTO vl.historial(
	 cedula, pais_salida, pais_llegada, pais_escala1, pais_escala2, fecha_compra, duracion_viaje, costo, id_avion)
	VALUES ( 203330444, 'Costa Rica', 'Panama', 'Ninguna','Ninguna',21/05/2020, 1, 100000, 4);
	
	INSERT INTO vl.historial(
	 cedula, pais_salida, pais_llegada, pais_escala1, pais_escala2, fecha_compra, duracion_viaje, costo, id_avion)
	VALUES ( 203330444, 'Costa Rica', 'Panama', 'Ninguna','Ninguna',21/05/2020, 1, 100000, 5);



----------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO vl.vuelo(
	 id_aerolinea, precio, fechahora_sda, aeropuerto_sda, fechahora_lda, aeropuerto_lda, duracion, id_avion, id_piloto1, id_piloto2, id_servicio1, id_servicio2, id_servicio3)
	VALUES ( 1, 100000, '26/07/2020 06:48', 'Aeropuerto Internacional Juan San María','Aeropuerto Internacional de Tocumen' , '26/07/2020 07:48', 1, 1, 3, 4, 5,6,3);
	
	INSERT INTO vl.vuelo(
	 id_aerolinea, precio, fechahora_sda, aeropuerto_sda, fechahora_lda, aeropuerto_lda, duracion, id_avion, id_piloto1, id_piloto2, id_servicio1, id_servicio2, id_servicio3)
	VALUES ( 2, 150000, '26/07/2020 12:48', 'Aeropuerto Internacional Juan San María','Aeropuerto Internacional de Tocumen' , '26/07/2020 1:48', 1, 1, 3, 4, 5,6,3);
	
	INSERT INTO vl.vuelo(
	 id_aerolinea, precio, fechahora_sda, aeropuerto_sda, fechahora_lda, aeropuerto_lda, duracion, id_avion, id_piloto1, id_piloto2, id_servicio1, id_servicio2, id_servicio3)
	VALUES ( 3, 100000, '28/07/2020 06:48', 'Aeropuerto Internacional Juan San María','Aeropuerto Internacional de Tocumen' , '30/07/2020 07:48', 1, 1, 3, 4, 5,6,3);
	
	INSERT INTO vl.vuelo(
	 id_aerolinea, precio, fechahora_sda, aeropuerto_sda, fechahora_lda, aeropuerto_lda, duracion, id_avion, id_piloto1, id_piloto2, id_servicio1, id_servicio2, id_servicio3)
	VALUES ( 4, 100000, '26/07/2020 06:48', 'Aeropuerto Internacional Juan San María','Aeropuerto Internacional de Tocumen' , '26/07/2020 07:48', 1, 1, 3, 4, 5,6,3);
	
	INSERT INTO vl.vuelo(
	 id_aerolinea, precio, fechahora_sda, aeropuerto_sda, fechahora_lda, aeropuerto_lda, duracion, id_avion, id_piloto1, id_piloto2, id_servicio1, id_servicio2, id_servicio3)
	VALUES ( 5, 750000, '26/07/2020 06:48', 'Aeropuerto Internacional Juan San María','Aeropuerto Internacional de Tocumen' , '26/07/2020 07:48', 1, 1, 3, 4, 5,6,3);