﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades;
using CapaNegocio;


namespace CapaPresentacion
{
    public partial class FrmPasajerorRegVuelo : Form
    {


        Usuario usuario;
        FrmPasajeroMenu frm;

        DataTable dataTableD;
        DataTable dataTableO;

        DataTable dataTablevuelo;
        DataTable dataTablevuelovuelta;


        DataTable dataTableescala1;
        DataTable dataTableescala2;
        DataTable dataTableescala3;


        string aeroSda;
        string aeroLda;
        string paisSda;
        string paisLda;


        string paisEscala1;
        string paisEscala2;


        bool viajeIda = false;
        bool viajeVuelta = false;

        //validaciones################################

        bool valiDgvnormal;

        bool valiDgvescala1;

        bool valiDgvescala2;

        bool valiDgvescala3;



        //datos para insertar########################

        //Datos datagridview normal

        string dgvNnombrepaissalida = "";

        string dgvNnombrepaisllegada = "";

        string dgvNhoy = DateTime.Today.ToString("dd/MM/yyyy hh:mm");

        int dgvNhoras = 0;

        double dgvNcosto = 0;

        int dgvNidAvion = 0;


        //Datos datagridview escala1
        string dgvE1nombrepaissalida = "";

        string dgvE1nombrepaisllegada = "";

        string dgvE1nombrepaisescala1 = "";

        string dgvE1nombrepaisescala2 = "";

        string dgvE1hoy = DateTime.Today.ToString("dd/MM/yyyy hh:mm");

        int dgvE1horas = 0;

        double dgvE1costo = 0;

        int dgvE1idAvion = 0;
        //Datos datagridview escala2

        string dgvE2nombrepaissalida = "";

        string dgvE2nombrepaisllegada = "";

        string dgvE2nombrepaisescala1 = "";

        string dgvE2nombrepaisescala2 = "";

        string dgvE2hoy = DateTime.Today.ToString("dd/MM/yyyy hh:mm");

        int dgvE2horas = 0;

        double dgvE2costo = 0;

        int dgvE2idAvion = 0;
        //Datos datagridview escala3
        string dgvE3nombrepaissalida = "";

        string dgvE3nombrepaisllegada = "";

        string dgvE3nombrepaisescala1 = "";

        string dgvE3nombrepaisescala2 = "";

        string dgvE3hoy = DateTime.Today.ToString("dd/MM/yyyy hh:mm");

        int dgvE3horas = 0;

        double dgvE3costo = 0;

        int dgvE3idAvion = 0;
        public FrmPasajerorRegVuelo(FrmPasajeroMenu frm, Usuario usuario)
        {
            InitializeComponent();

            establecerTamanno();

            this.frm = frm;

            this.usuario = usuario;

            CenterToParent();

            definirDataAeropuertoD();
            definirDataAeropuertoO();

            // definirdgvVueloVuelta();

            cargarDataAeropuerto();

            definirdgvVuelo();
            definirdgvEscala1();
            definirdgvEscala2();
            definirdgvEscala3();


            panelEscala.Visible = false;


        }

        public void establecerTamanno()
        {

            this.Size = new System.Drawing.Size(850, 500);

        }




        public void definirDataAeropuertoD()
        {

            this.dataTableD = new DataTable();

            dataTableD.Columns.Add("Id_aeropuerto");
            dataTableD.Columns.Add("IATA");
            dataTableD.Columns.Add("Nombre");
            dataTableD.Columns.Add("Pais");


            dgvAeropuetoD.DataSource = dataTableD;
        }



        public void definirDataAeropuertoO()
        {

            dataTableO = new DataTable();

            dataTableO.Columns.Add("Id_aeropuerto");
            dataTableO.Columns.Add("IATA");
            dataTableO.Columns.Add("Nombre");
            dataTableO.Columns.Add("Pais");

            dgvAeropuetoO.DataSource = dataTableO;

        }

        public void definirdgvVuelo()
        {


            dataTablevuelo = new DataTable();

            DataColumn precio = new DataColumn("Precio");
            precio.DataType = Type.GetType("System.Int32");



            dataTablevuelo.Columns.Add(precio);
            dataTablevuelo.Columns.Add("Fecha_Salida");
            dataTablevuelo.Columns.Add("Aeropuerto salida");
            dataTablevuelo.Columns.Add("Fecha llegada");
            dataTablevuelo.Columns.Add("Aeropuerto llegada");
            dataTablevuelo.Columns.Add("Duracion(hrs)");
            dataTablevuelo.Columns.Add("id_Avion");

            dgvVuelos.DataSource = dataTablevuelo;


        }

        /*  public void definirdgvVueloVuelta()
          {


              dataTablevuelovuelta = new DataTable();

              DataColumn precio = new DataColumn("Precio");
              precio.DataType = Type.GetType("System.Int32");



              dataTablevuelovuelta.Columns.Add(precio);
              dataTablevuelovuelta.Columns.Add("Fecha_Salida");
              dataTablevuelovuelta.Columns.Add("Aeropuerto salida");
              dataTablevuelovuelta.Columns.Add("Fecha llegada");
              dataTablevuelovuelta.Columns.Add("Aeropuerto llegada");
              dataTablevuelovuelta.Columns.Add("Duracion(hrs)");

              dgvVuelosVuelta.DataSource = dataTablevuelovuelta;


          }*/



        public void definirdgvEscala1()
        {


            dataTableescala1 = new DataTable();


            DataColumn precio = new DataColumn("Precio");
            precio.DataType = Type.GetType("System.Int32");

            dataTableescala1.Columns.Add(precio);
            dataTableescala1.Columns.Add("Fecha_Salida");
            dataTableescala1.Columns.Add("Aeropuerto salida");
            dataTableescala1.Columns.Add("Fecha llegada");
            dataTableescala1.Columns.Add("Aeropuerto llegada");
            dataTableescala1.Columns.Add("Duracion(hrs)");
            dataTableescala1.Columns.Add("id_Avion");

            dgvEscala1.DataSource = dataTableescala1;


        }

        public void definirdgvEscala2()
        {


            dataTableescala2 = new DataTable();


            DataColumn precio = new DataColumn("Precio");
            precio.DataType = Type.GetType("System.Int32");

            dataTableescala2.Columns.Add(precio);
            dataTableescala2.Columns.Add("Fecha_Salida");
            dataTableescala2.Columns.Add("Aeropuerto salida");
            dataTableescala2.Columns.Add("Fecha llegada");
            dataTableescala2.Columns.Add("Aeropuerto llegada");
            dataTableescala2.Columns.Add("Duracion(hrs)");
            dataTableescala2.Columns.Add("id_Avion");

            dgvEscala2.DataSource = dataTableescala2;


        }
        public void definirdgvEscala3()
        {

            dataTableescala3 = new DataTable();


            DataColumn precio = new DataColumn("Precio");
            precio.DataType = Type.GetType("System.Int32");

            dataTableescala3.Columns.Add(precio);
            dataTableescala3.Columns.Add("Fecha_Salida");
            dataTableescala3.Columns.Add("Aeropuerto salida");
            dataTableescala3.Columns.Add("Fecha llegada");
            dataTableescala3.Columns.Add("Aeropuerto llegada");
            dataTableescala3.Columns.Add("Duracion(hrs)");
            dataTableescala3.Columns.Add("id_Avion");

            dgvEscala3.DataSource = dataTableescala3;


        }





        public void cargarDataAeropuerto()
        {
            new CNAeropuerto().selecccionarAeropuerto(dgvAeropuetoO, dgvAeropuetoD);

        }


        public void cargarVuelos()
        {
            MessageBox.Show(dtpSalida.Value.ToString("dd/MM/yyyy") + dtpLlegada.Value.ToString("dd/MM/yyyy"));

            new CNVuelo().seleccionarVuelosBusqueda(ckbIdavuelta, dgvVuelos, dgvEscala1, dgvEscala2, dgvEscala3, panelMain, panelEscala, this.aeroSda, this.aeroLda, dtpSalida.Value.ToString("dd/MM/yyyy"), dtpLlegada.Value.ToString("dd/MM/yyyy")); ;

            this.Size = new System.Drawing.Size(950, 800);

            CenterToParent();


            // if (ckbIdavuelta.Checked) {

            // new CNVuelo().seleccionarVuelosBusqueda(null,dgvVuelosVuelta, null, null,null,null, null, this.aeroLda, this.aeroSda);

            //}
        }




        private void dateTimePicker1_MouseEnter(object sender, EventArgs e)
        {
            dtpFechaida.Enabled = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string fecha = dtpFechaida.Value.ToString("dd/MM/yyyy");

            MessageBox.Show(fecha);

            dataTablevuelo.DefaultView.RowFilter = $"Fecha_Salida LIKE '%{fecha}%'";
        }


        private void btnAtras_Click(object sender, EventArgs e)
        {
            panelEscala.Visible = false;

            panelMain.Visible = true;

            establecerTamanno();
            resetear();
        }

        private void btnAsc_Click(object sender, EventArgs e)
        {

            dataTablevuelo.DefaultView.Sort = $"Precio ASC";
        }


        private void button4_Click(object sender, EventArgs e)
        {
            dataTablevuelo.DefaultView.Sort = $"Precio  DESC";
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void btnHmenormayor_Click(object sender, EventArgs e)
        {

            dataTablevuelo.DefaultView.Sort = $"Duracion(hrs) ASC";

        }

        private void button3_Click(object sender, EventArgs e)
        {

            dataTablevuelo.DefaultView.Sort = $"Duracion(hrs) DESC";

        }



        private void txtAeroO_TextChanged(object sender, EventArgs e)

        {
            string buscar = txtAeroO.Text;

            dataTableO.DefaultView.RowFilter = $"IATA LIKE '%{buscar}%' or Nombre LIKE '%{buscar}%'";
        }

        private void txtAeroD_TextChanged(object sender, EventArgs e)
        {

            string buscar = txtAeroD.Text;

            dataTableD.DefaultView.RowFilter = $"IATA LIKE '%{buscar}%' or Nombre LIKE '%{buscar}%'";
        }

        private void dgvAeropuetoO_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {
            this.aeroSda = dgvAeropuetoO.Rows[e.RowIndex].Cells[2].Value.ToString();
            this.paisSda = dgvAeropuetoO.Rows[e.RowIndex].Cells[3].Value.ToString();
            MessageBox.Show(aeroSda);

            dataTableD.DefaultView.RowFilter = $"IATA NOT LIKE '%{dgvAeropuetoO.Rows[e.RowIndex].Cells[1].Value.ToString()}%'";
        }

        private void dgvAeropuetoD_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {
            // dgvVuelos.Columns.Clear();
            // dgvVuelosVuelta.Columns.Clear();

            //definirdgvVueloVuelta();


            this.aeroLda = dgvAeropuetoD.Rows[e.RowIndex].Cells[2].Value.ToString();
            this.paisLda = dgvAeropuetoD.Rows[e.RowIndex].Cells[3].Value.ToString();
            cargarVuelos();
        }

        private void btnprecioAscvuelta_Click(object sender, EventArgs e)
        {

        }

        private void btnVuelo_Inteligente_Click(object sender, EventArgs e)
        {
            frm.Visible = true;
            this.Hide();
        }

        //Validaciones###############################################################################################

        private void dgvVuelos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            valiDgvnormal = true;

            dgvNnombrepaissalida = dgvVuelos.Rows[e.RowIndex].Cells[2].Value.ToString();
            dgvNnombrepaisllegada = dgvVuelos.Rows[e.RowIndex].Cells[4].Value.ToString();
            dgvNhoras = int.Parse(dgvVuelos.Rows[e.RowIndex].Cells[5].Value.ToString());
            dgvNcosto = double.Parse(dgvVuelos.Rows[e.RowIndex].Cells[0].Value.ToString());
            dgvNidAvion = int.Parse(dgvVuelos.Rows[e.RowIndex].Cells[6].Value.ToString());



        }

        private void dgvEscala1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (valiDgvnormal == true)
            {
                MessageBox.Show("Previamente a escogido una ruta directa al destino Pulse el boton[Limpiar] para escoger escalas");

            }
            else
            {
                valiDgvescala1 = true;

                dgvE1nombrepaissalida = dgvEscala1.Rows[e.RowIndex].Cells[2].Value.ToString();
                dgvE1nombrepaisllegada = dgvEscala1.Rows[e.RowIndex].Cells[4].Value.ToString();
                dgvE1horas = int.Parse(dgvEscala1.Rows[e.RowIndex].Cells[5].Value.ToString());
                dgvE1costo = double.Parse(dgvEscala1.Rows[e.RowIndex].Cells[0].Value.ToString());
                dgvE1idAvion = int.Parse(dgvEscala1.Rows[e.RowIndex].Cells[6].Value.ToString());


            }



        }

        private void dgvEscala2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (valiDgvnormal == true)
            {
                MessageBox.Show("Previamente a escogido una ruta directa al destino Pulse el boton[Limpiar] para escoger escalas");

            }
            else if (valiDgvescala1 == false)
            {

                MessageBox.Show("Por favor");
            }
            else
            {
                valiDgvescala2 = true;

                dgvE2nombrepaissalida = dgvEscala2.Rows[e.RowIndex].Cells[2].Value.ToString();
                dgvE2nombrepaisllegada = dgvEscala2.Rows[e.RowIndex].Cells[4].Value.ToString();
                dgvE2horas = int.Parse(dgvEscala2.Rows[e.RowIndex].Cells[5].Value.ToString());
                dgvE2costo = double.Parse(dgvEscala2.Rows[e.RowIndex].Cells[0].Value.ToString());
                dgvE2idAvion = int.Parse(dgvEscala2.Rows[e.RowIndex].Cells[6].Value.ToString());
            }

        }

        private void dgvEscala3_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            if (valiDgvnormal == true)
            {
                MessageBox.Show("Previamente a escogido una ruta directa al destino Pulse el boton[Limpiar] para escoger viaje por escalas");

            }
            else
            {
                valiDgvescala3 = true;

                dgvE3nombrepaissalida = dgvEscala3.Rows[e.RowIndex].Cells[2].Value.ToString();
                dgvE3nombrepaisllegada = dgvEscala3.Rows[e.RowIndex].Cells[4].Value.ToString();
                dgvE3horas = int.Parse(dgvEscala3.Rows[e.RowIndex].Cells[5].Value.ToString());
                dgvE3costo = double.Parse(dgvEscala3.Rows[e.RowIndex].Cells[0].Value.ToString());
                dgvE3idAvion = int.Parse(dgvEscala3.Rows[e.RowIndex].Cells[6].Value.ToString());
            }

        }


        private void btnConfirmar_Click(object sender, EventArgs e)
        {

            int cedula = usuario.Cedula;
            string fecha_compra;
            string duracion;
            double costo;

            /*
            DialogResult oDlgRes;

            oDlgRes = MessageBox.Show("¿Desea comprar un un vuelo de vuelta?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);


            if (oDlgRes == DialogResult.Yes)
            {

                FrmPasajeroViajeVuelta frm = new FrmPasajeroViajeVuelta(this.aeroSda, aeroLda);

                frm.Visible = true;

                this.Enabled = false;

            }

             TimeSpan span = TimeSpan.FromMinutes(minutes); string label = span.ToString(@"hh\:mm");
            */


            if (valiDgvnormal == false && valiDgvescala1 == false && valiDgvescala2 == false && valiDgvescala3 == false)
            {

                MessageBox.Show("No ha escogido ninguna ruta");

            }
            else
            {

                if ((valiDgvnormal == true))
                {

                    new CNHistorial().insertarHistorial(usuario.Cedula, dgvNnombrepaissalida, dgvNnombrepaisllegada, "", "", dgvNhoy.ToString(), dgvNhoras, dgvNcosto, dgvNidAvion);


                }
                else if ((valiDgvescala1 == true && valiDgvescala2 == true && valiDgvescala3 == false))
                {
                    new CNHistorial().insertarHistorial(usuario.Cedula, dgvE1nombrepaissalida, dgvE1nombrepaisllegada, dgvE2nombrepaissalida, "", dgvE1hoy.ToString(), dgvE1horas, dgvE1costo, dgvE1idAvion);

                    new CNHistorial().insertarHistorial(usuario.Cedula, dgvE2nombrepaissalida, dgvE2nombrepaisllegada, dgvE2nombrepaissalida, "", dgvE2hoy.ToString(), dgvE2horas, dgvE2costo, dgvE2idAvion);
                }
                else if ((valiDgvescala1 == true && valiDgvescala2 == true && valiDgvescala3 == true))
                {
                    new CNHistorial().insertarHistorial(usuario.Cedula, dgvE1nombrepaissalida, dgvE1nombrepaisllegada, dgvE2nombrepaissalida, dgvE3nombrepaissalida, dgvE1hoy.ToString(), dgvE1horas, dgvE1costo, dgvE1idAvion);
                    new CNHistorial().insertarHistorial(usuario.Cedula, dgvE2nombrepaissalida, dgvE2nombrepaisllegada, dgvE2nombrepaissalida, dgvE3nombrepaissalida, dgvE2hoy.ToString(), dgvE2horas, dgvE2costo, dgvE2idAvion);
                    new CNHistorial().insertarHistorial(usuario.Cedula, dgvE3nombrepaissalida, dgvE3nombrepaisllegada, dgvE2nombrepaissalida, dgvE3nombrepaissalida, dgvE3hoy.ToString(), dgvE3horas, dgvE3costo, dgvE3idAvion);
                }
                else
                {


                    MessageBox.Show("Establesca las rutas correctamente");
                }
            }

        }


        public void resetear()
        {

            //Datos datagridview normal

            string dgvNnombrepaissalida = "";

            string dgvNnombrepaisllegada = "";

            DateTime dgvNhoy = DateTime.Today;

            int dgvNhoras = 0;

            double dgvNcosto = 0;

            int dgvNidAvion = 0;


            //Datos datagridview escala1
            string dgvE1nombrepaissalida = "";

            string dgvE1nombrepaisllegada = "";

            string dgvE1nombrepaisescala1 = "";

            string dgvE1nombrepaisescala2 = "";

            DateTime dgvE1hoy = DateTime.Today;

            int dgvE1horas = 0;

            double dgvE1costo = 0;

            int dgvE1idAvion = 0;
            //Datos datagridview escala2

            string dgvE2nombrepaissalida = "";

            string dgvE2nombrepaisllegada = "";

            string dgvE2nombrepaisescala1 = "";

            string dgvE2nombrepaisescala2 = "";

            DateTime dgvE2hoy = DateTime.Today;

            int dgvE2horas = 0;

            double dgvE2costo = 0;

            int dgvE2idAvion = 0;
            //Datos datagridview escala3
            string dgvE3nombrepaissalida = "";

            string dgvE3nombrepaisllegada = "";

            string dgvE3nombrepaisescala1 = "";

            string dgvE3nombrepaisescala2 = "";

            DateTime dgvE3hoy = DateTime.Today;

            int dgvE3horas = 0;

            double dgvE3costo = 0;

            int dgvE3idAvion = 0;


            dgvVuelos.Columns.Clear();
            dgvEscala1.Columns.Clear();
            dgvEscala2.Columns.Clear();
            dgvEscala3.Columns.Clear();

            definirdgvVuelo();
            definirdgvEscala1();
            definirdgvEscala2();
            definirdgvEscala3();

        }

        private void btndgvVuelos_Click(object sender, EventArgs e)
        {
            if (dgvVuelos.RowCount - 1 > 0)
            {
                string aeroSalida = dgvVuelos.Rows[0].Cells[2].Value.ToString();
                string aeroLlegada = dgvVuelos.Rows[0].Cells[4].Value.ToString();
                MessageBox.Show(aeroSalida + aeroLlegada);
                FrmVueloInteligente frm = new FrmVueloInteligente(dtpSalida.Value.ToString("dd/MM/yyyy"), dtpLlegada.Value.ToString("dd/MM/yyyy"), aeroSalida, aeroLlegada);

                MessageBox.Show(aeroSalida + aeroLlegada);

                frm.Visible = true;
            }
            else
            {

                MessageBox.Show("No hay vuelos");

            }
        }

        private void btndgvEscala1_Click(object sender, EventArgs e)
        {
            if (dgvEscala1.RowCount - 1 > 0)
            {
                string aeroSalida = dgvEscala1.Rows[0].Cells[2].Value.ToString();
                string aeroLlegada = dgvEscala1.Rows[0].Cells[4].Value.ToString();

                FrmVueloInteligente frm = new FrmVueloInteligente(dtpSalida.Value.ToString("dd/MM/yyyy"), dtpLlegada.Value.ToString("dd/MM/yyyy"), aeroSalida, aeroLlegada);
                frm.Visible = true;
            }
            else
            {

                MessageBox.Show("No hay vuelos");

            }
        }

        private void btndgvEscala2_Click(object sender, EventArgs e)
        {
            if (dgvEscala2.RowCount - 1 > 0)
            {
                string aeroSalida = dgvEscala2.Rows[0].Cells[2].Value.ToString();
                string aeroLlegada = dgvEscala2.Rows[0].Cells[4].Value.ToString();

                FrmVueloInteligente frm = new FrmVueloInteligente(dtpSalida.Value.ToString("dd/MM/yyyy"), dtpLlegada.Value.ToString("dd/MM/yyyy"), aeroSalida, aeroLlegada);
                frm.Visible = true;
            }
            else
            {

                MessageBox.Show("No hay vuelos");


            }
        }

        private void button15_Click(object sender, EventArgs e)
        {
            if (dgvEscala3.RowCount - 1 > 0)
            {
                string aeroSalida = dgvEscala3.Rows[0].Cells[2].Value.ToString();
                string aeroLlegada = dgvEscala3.Rows[0].Cells[4].Value.ToString();

                FrmVueloInteligente frm = new FrmVueloInteligente(dtpSalida.Value.ToString("dd/MM/yyyy"), dtpLlegada.Value.ToString("dd/MM/yyyy"), aeroSalida, aeroLlegada);
                frm.Visible = true;
            }
            else
            {

                MessageBox.Show("No hay vuelos");


            }
        }
    }
}

