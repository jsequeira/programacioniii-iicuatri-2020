﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaNegocio;

namespace CapaPresentacion
{
    public partial class FrmRegistro : Form
    {
        FrmInicioSesion inicioSesion;
        public FrmRegistro(FrmInicioSesion inicioSesion)

        {
            InitializeComponent();
            this.inicioSesion = inicioSesion;
            CenterToParent();
        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            inicioSesion.Visible = true;
            this.Hide();
        }

        private void btnRegistro_Click(object sender, EventArgs e)
        {
            string nombre = txtNombre.Text;
            int cedula =int.Parse(txtCedula.Text);
            int edad = int.Parse(txtEdad.Text);
            string contrasenna = txtContrasenna.Text;

            if (contrasenna.Length < 6)
            {

                MessageBox.Show("La contraseña debe contener  6 o más caracteres");

            }
            else {
                if (txtContrasenna.Text == txtValidacion.Text) {

                    new CNUsuario().insertarUsuario(nombre, cedula, edad, contrasenna);





                }
                else {

                    MessageBox.Show("Las contraseñas no coinciden");

                }
            
            
            }


        }
    }
}
