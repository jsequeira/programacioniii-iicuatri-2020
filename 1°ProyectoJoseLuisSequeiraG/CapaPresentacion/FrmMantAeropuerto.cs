﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaNegocio;


namespace CapaPresentacion
{
    public partial class FrmMantAeropuerto : Form
    {

        FrmMantenimiento mant;
        public FrmMantAeropuerto(FrmMantenimiento mant)
        {
            InitializeComponent();
            this.mant = mant;
            CenterToParent();
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            if (txtIATA.Text != "" && txtNombre.Text != "" && txtPais.Text != "")
            {
            new CNAeropuerto().insertarAeropuerto(txtIATA.Text, txtNombre.Text, txtPais.Text);

                txtIATA.Text = "";
                txtNombre.Text = "";
                txtPais.Text = "";
            }
            else {

                MessageBox.Show("Rellene todos los campos");
            }
        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            this.mant.Visible = true;
            this.Hide();
        }
    }
}
