﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Authentication.ExtendedProtection;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaNegocio;

namespace CapaPresentacion
{
    public partial class FrmMantAvioncs : Form
    {

        FrmMantenimiento mant;

        string modelo = "";

        int id_aerolinea =0;


        DataTable dataAerolinea = new DataTable();
        DataTable dataModelo= new DataTable();
        DataTable dataAvion = new DataTable();
        public FrmMantAvioncs(FrmMantenimiento mant)
        {
            InitializeComponent();
            this.mant = mant;
            CenterToParent();    
            
            definirAerolinea();
            definirAvion();

            cargarAerolinea();
            cargarModelos();

            cargarAvion();
           

        }



        public void definirAerolinea() {



                dataAerolinea = new DataTable();
                dataAerolinea.Columns.Add("Id_aerolinea");
                dataAerolinea.Columns.Add("Nombre");
                dataAerolinea.Columns.Add("Año");
                dataAerolinea.Columns.Add("Tipo");
                dgvAerolinea.DataSource = dataAerolinea;



        }


        public void definirAvion()
        {

            dataAvion = new DataTable();
            dataAvion.Columns.Add("Id_avion");
            dataAvion.Columns.Add("Modelo");
            dataAvion.Columns.Add("Annio_construccion");
            dataAvion.Columns.Add("Id_aerolinea");
            dataAvion.Columns.Add("Capacidad");
            dataAvion.Columns.Add("Estado");
            dgvAvion.DataSource = dataAvion;


        }

        public void cargarModelos()
        {

            dataModelo = new DataTable();

            dataModelo.Columns.Add("Modelo");


            dataModelo.Rows.Add(new object[] { "Airbus A310-324" });
            dataModelo.Rows.Add(new object[] { "Airbus A310-325ET" });
            dataModelo.Rows.Add(new object[] { "Airbus A320-212" });

            dataModelo.Rows.Add(new object[] { "Avro 748" });
            dataModelo.Rows.Add(new object[] { "Avro York" });
            dataModelo.Rows.Add(new object[] { "Boeing 707-320B" });

            dataModelo.Rows.Add(new object[] { "Boeing 707-320C" });
            dataModelo.Rows.Add(new object[] { "Boeing 727-23" });
            dataModelo.Rows.Add(new object[] { "Boeing 727-200" });
            dataModelo.Rows.Add(new object[] { "Boeing 727-200Adv" });
            dgvModelo.DataSource = dataModelo;

            //dgvModelo.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

        }

        public void cargarAerolinea()
        {

            new CNAerolinea().seleccionarAerolineas(dgvAerolinea);

        }

        public void cargarAvion() {

            new CNAvion().seleccionarAvion(dgvAvion);

        }



        public void btnBuscarmodelo_Click(object sender, EventArgs e)
        {

            dataModelo.DefaultView.RowFilter = $"Modelo LIKE '{txtBuscarmodelo.Text}%'";
        }




        private void btnBuscaraerolinea_Click(object sender, EventArgs e)
        {
            dataAerolinea.DefaultView.RowFilter = $"Nombre LIKE '{txBuscaraerolinea.Text}%'";
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtCapacidad.Text != "")
                {

                    string estado;

                    if (ckbDisponible.Checked == true)
                    {

                        estado = "Disponible";

                    }
                    else
                    {

                        estado = "En servicio";

                    }

                    string annio = dtpAnnio.Value.Year.ToString();

                    int capacidad = Int32.Parse(txtCapacidad.Text);




                    new CNAvion().insertarAvion(modelo, annio, id_aerolinea, capacidad, estado);

                    dgvAerolinea.Columns.Clear();
                    dgvAvion.Columns.Clear();

                    definirAvion();
                    cargarAvion();

                    definirAerolinea();
                    cargarAerolinea();

                }
                else
                {

                    MessageBox.Show("Rellene el campo[Capacidad]");
                }
            }
            catch {
                MessageBox.Show("Por favor digitar números enteros en el campo[Capacidad]");
            
            }
        }

        private void dgvModelo_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }

      
        private void btnBuscaravion_Click(object sender, EventArgs e)
        {
            dataAvion.DefaultView.RowFilter = $"Modelo LIKE '{txtBuscaravion.Text}%'";
        }

        private void dgvAerolinea_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            this.id_aerolinea = int.Parse(dgvAerolinea.Rows[e.RowIndex].Cells[0].Value.ToString());

            MessageBox.Show("hola2");
        }

        private void dgvModelo_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            this.modelo = dgvModelo.Rows[e.RowIndex].Cells[0].Value.ToString();
            MessageBox.Show("Hola1");
        }

        private void txBuscaraerolinea_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            this.mant.Visible = true;
            this.Hide();
        }

        private void txtBuscaravion_TextChanged(object sender, EventArgs e)
        {

        }
    }
    
}

