﻿namespace CapaPresentacion
{
    partial class FrmPasajeroViajeVuelta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelEscala = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.button22 = new System.Windows.Forms.Button();
            this.dgvEscala3 = new System.Windows.Forms.DataGridView();
            this.button23 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.dateTimePicker4 = new System.Windows.Forms.DateTimePicker();
            this.label18 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.button21 = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.button13 = new System.Windows.Forms.Button();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.label14 = new System.Windows.Forms.Label();
            this.button12 = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label13 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.btnAtras = new System.Windows.Forms.Button();
            this.btnConfirmarEscala = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.Vuelos = new System.Windows.Forms.Label();
            this.dgvEscala2 = new System.Windows.Forms.DataGridView();
            this.dgvEscala1 = new System.Windows.Forms.DataGridView();
            this.panelEscala.SuspendLayout();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEscala3)).BeginInit();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEscala2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEscala1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelEscala
            // 
            this.panelEscala.Controls.Add(this.panel9);
            this.panelEscala.Controls.Add(this.button13);
            this.panelEscala.Controls.Add(this.dateTimePicker2);
            this.panelEscala.Controls.Add(this.label14);
            this.panelEscala.Controls.Add(this.button12);
            this.panelEscala.Controls.Add(this.dateTimePicker1);
            this.panelEscala.Controls.Add(this.label13);
            this.panelEscala.Controls.Add(this.panel5);
            this.panelEscala.Controls.Add(this.panel6);
            this.panelEscala.Controls.Add(this.panel4);
            this.panelEscala.Controls.Add(this.panel3);
            this.panelEscala.Controls.Add(this.btnAtras);
            this.panelEscala.Controls.Add(this.btnConfirmarEscala);
            this.panelEscala.Controls.Add(this.label5);
            this.panelEscala.Controls.Add(this.Vuelos);
            this.panelEscala.Controls.Add(this.dgvEscala2);
            this.panelEscala.Controls.Add(this.dgvEscala1);
            this.panelEscala.Location = new System.Drawing.Point(12, 35);
            this.panelEscala.Name = "panelEscala";
            this.panelEscala.Size = new System.Drawing.Size(1071, 747);
            this.panelEscala.TabIndex = 8;
            // 
            // panel9
            // 
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel9.Controls.Add(this.button22);
            this.panel9.Controls.Add(this.dgvEscala3);
            this.panel9.Controls.Add(this.button23);
            this.panel9.Controls.Add(this.button1);
            this.panel9.Controls.Add(this.button20);
            this.panel9.Controls.Add(this.dateTimePicker4);
            this.panel9.Controls.Add(this.label18);
            this.panel9.Controls.Add(this.label16);
            this.panel9.Controls.Add(this.button21);
            this.panel9.Controls.Add(this.label17);
            this.panel9.Controls.Add(this.label19);
            this.panel9.Enabled = false;
            this.panel9.Location = new System.Drawing.Point(23, 437);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(1032, 208);
            this.panel9.TabIndex = 34;
            // 
            // button22
            // 
            this.button22.Location = new System.Drawing.Point(726, 25);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(122, 23);
            this.button22.TabIndex = 13;
            this.button22.Text = "Menor a mayor";
            this.button22.UseVisualStyleBackColor = true;
            // 
            // dgvEscala3
            // 
            this.dgvEscala3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEscala3.Location = new System.Drawing.Point(3, 52);
            this.dgvEscala3.Name = "dgvEscala3";
            this.dgvEscala3.RowHeadersWidth = 51;
            this.dgvEscala3.RowTemplate.Height = 24;
            this.dgvEscala3.Size = new System.Drawing.Size(1024, 148);
            this.dgvEscala3.TabIndex = 32;
            // 
            // button23
            // 
            this.button23.Location = new System.Drawing.Point(854, 25);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(122, 23);
            this.button23.TabIndex = 14;
            this.button23.Text = "Mayor a menor";
            this.button23.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(412, 16);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 37;
            this.button1.Text = "Buscar";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button20
            // 
            this.button20.Location = new System.Drawing.Point(854, -1);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(122, 23);
            this.button20.TabIndex = 17;
            this.button20.Text = "Mayor a menor";
            this.button20.UseVisualStyleBackColor = true;
            // 
            // dateTimePicker4
            // 
            this.dateTimePicker4.CustomFormat = "dd/MM/yyyy ";
            this.dateTimePicker4.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker4.Location = new System.Drawing.Point(206, 17);
            this.dateTimePicker4.Name = "dateTimePicker4";
            this.dateTimePicker4.Size = new System.Drawing.Size(200, 22);
            this.dateTimePicker4.TabIndex = 36;
            this.dateTimePicker4.Value = new System.DateTime(2020, 8, 20, 0, 0, 0, 0);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(651, 28);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(51, 17);
            this.label18.TabIndex = 15;
            this.label18.Text = "precio:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(108, 21);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(92, 17);
            this.label16.TabIndex = 35;
            this.label16.Text = "Fecha salida:";
            // 
            // button21
            // 
            this.button21.Location = new System.Drawing.Point(726, -1);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(122, 23);
            this.button21.TabIndex = 16;
            this.button21.Text = "Menor a mayor";
            this.button21.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(651, 5);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(69, 17);
            this.label17.TabIndex = 18;
            this.label17.Text = "Duracion:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(0, 24);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(99, 17);
            this.label19.TabIndex = 31;
            this.label19.Text = "vuelo escala 3";
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(446, 256);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(75, 23);
            this.button13.TabIndex = 30;
            this.button13.Text = "Buscar";
            this.button13.UseVisualStyleBackColor = true;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.CustomFormat = "dd/MM/yyyy ";
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker2.Location = new System.Drawing.Point(240, 257);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(200, 22);
            this.dateTimePicker2.TabIndex = 29;
            this.dateTimePicker2.Value = new System.DateTime(2020, 8, 20, 0, 0, 0, 0);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(142, 259);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(92, 17);
            this.label14.TabIndex = 28;
            this.label14.Text = "Fecha salida:";
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(433, 36);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(75, 23);
            this.button12.TabIndex = 27;
            this.button12.Text = "Buscar";
            this.button12.UseVisualStyleBackColor = true;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CustomFormat = "dd/MM/yyyy ";
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(227, 37);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 22);
            this.dateTimePicker1.TabIndex = 26;
            this.dateTimePicker1.Value = new System.DateTime(2020, 8, 20, 0, 0, 0, 0);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(129, 39);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(92, 17);
            this.label13.TabIndex = 25;
            this.label13.Text = "Fecha salida:";
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.button8);
            this.panel5.Controls.Add(this.button9);
            this.panel5.Controls.Add(this.label12);
            this.panel5.Location = new System.Drawing.Point(652, 252);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(377, 25);
            this.panel5.TabIndex = 24;
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(234, 2);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(122, 23);
            this.button8.TabIndex = 17;
            this.button8.Text = "Mayor a menor";
            this.button8.UseVisualStyleBackColor = true;
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(106, 3);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(122, 23);
            this.button9.TabIndex = 16;
            this.button9.Text = "Menor a mayor";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(26, 3);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(69, 17);
            this.label12.TabIndex = 18;
            this.label12.Text = "Duracion:";
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.button10);
            this.panel6.Controls.Add(this.button11);
            this.panel6.Controls.Add(this.label11);
            this.panel6.Location = new System.Drawing.Point(652, 222);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(377, 28);
            this.panel6.TabIndex = 23;
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(106, 3);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(122, 23);
            this.button10.TabIndex = 13;
            this.button10.Text = "Menor a mayor";
            this.button10.UseVisualStyleBackColor = true;
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(234, 3);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(122, 23);
            this.button11.TabIndex = 14;
            this.button11.Text = "Mayor a menor";
            this.button11.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(44, 9);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(51, 17);
            this.label11.TabIndex = 15;
            this.label11.Text = "precio:";
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.button6);
            this.panel4.Controls.Add(this.button7);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Location = new System.Drawing.Point(652, 33);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(377, 25);
            this.panel4.TabIndex = 21;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(234, 2);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(122, 23);
            this.button6.TabIndex = 17;
            this.button6.Text = "Mayor a menor";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(106, 3);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(122, 23);
            this.button7.TabIndex = 16;
            this.button7.Text = "Menor a mayor";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(26, 3);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(69, 17);
            this.label9.TabIndex = 18;
            this.label9.Text = "Duracion:";
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.button4);
            this.panel3.Controls.Add(this.button5);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Location = new System.Drawing.Point(652, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(377, 28);
            this.panel3.TabIndex = 20;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(106, 3);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(122, 23);
            this.button4.TabIndex = 13;
            this.button4.Text = "Menor a mayor";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(234, 3);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(122, 23);
            this.button5.TabIndex = 14;
            this.button5.Text = "Mayor a menor";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(44, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(51, 17);
            this.label8.TabIndex = 15;
            this.label8.Text = "precio:";
            // 
            // btnAtras
            // 
            this.btnAtras.Location = new System.Drawing.Point(23, 668);
            this.btnAtras.Name = "btnAtras";
            this.btnAtras.Size = new System.Drawing.Size(75, 23);
            this.btnAtras.TabIndex = 9;
            this.btnAtras.Text = "Atras";
            this.btnAtras.UseVisualStyleBackColor = true;
            // 
            // btnConfirmarEscala
            // 
            this.btnConfirmarEscala.Location = new System.Drawing.Point(980, 668);
            this.btnConfirmarEscala.Name = "btnConfirmarEscala";
            this.btnConfirmarEscala.Size = new System.Drawing.Size(75, 23);
            this.btnConfirmarEscala.TabIndex = 8;
            this.btnConfirmarEscala.Text = "Confirmar";
            this.btnConfirmarEscala.UseVisualStyleBackColor = true;
            this.btnConfirmarEscala.Click += new System.EventHandler(this.btnConfirmarEscala_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(21, 37);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(101, 17);
            this.label5.TabIndex = 6;
            this.label5.Text = "Vuelo escala 1";
            // 
            // Vuelos
            // 
            this.Vuelos.AutoSize = true;
            this.Vuelos.Location = new System.Drawing.Point(34, 264);
            this.Vuelos.Name = "Vuelos";
            this.Vuelos.Size = new System.Drawing.Size(99, 17);
            this.Vuelos.TabIndex = 5;
            this.Vuelos.Text = "vuelo escala 2";
            // 
            // dgvEscala2
            // 
            this.dgvEscala2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEscala2.Location = new System.Drawing.Point(23, 283);
            this.dgvEscala2.Name = "dgvEscala2";
            this.dgvEscala2.RowHeadersWidth = 51;
            this.dgvEscala2.RowTemplate.Height = 24;
            this.dgvEscala2.Size = new System.Drawing.Size(1032, 148);
            this.dgvEscala2.TabIndex = 5;
            // 
            // dgvEscala1
            // 
            this.dgvEscala1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEscala1.Location = new System.Drawing.Point(23, 70);
            this.dgvEscala1.Name = "dgvEscala1";
            this.dgvEscala1.RowHeadersWidth = 51;
            this.dgvEscala1.RowTemplate.Height = 24;
            this.dgvEscala1.Size = new System.Drawing.Size(1032, 146);
            this.dgvEscala1.TabIndex = 4;
            // 
            // FrmPasajeroViajeVuelta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1087, 773);
            this.Controls.Add(this.panelEscala);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FrmPasajeroViajeVuelta";
            this.Text = "FrmPasajeroViajeVuelta";
            this.panelEscala.ResumeLayout(false);
            this.panelEscala.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEscala3)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEscala2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEscala1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelEscala;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.DataGridView dgvEscala3;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.DateTimePicker dateTimePicker4;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnAtras;
        private System.Windows.Forms.Button btnConfirmarEscala;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label Vuelos;
        private System.Windows.Forms.DataGridView dgvEscala2;
        private System.Windows.Forms.DataGridView dgvEscala1;
    }
}