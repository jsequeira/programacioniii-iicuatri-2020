﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaNegocio;
using Entidades;

namespace CapaPresentacion
{



    public partial class FrmInicioSesion : Form
    {

        Usuario usuario;


        public FrmInicioSesion()
        {
            InitializeComponent();
            CenterToScreen();

            //string contrasenna = GetSHA256("2141996");

           // MessageBox.Show(contrasenna);

            /*new CNUsuario().insertarUsuario("Juan Castro Kell",204350479,34,"123456");
            new CNUsuario().insertarUsuario("Pedro Ramirez Shang", 202550479, 29, "123457");
            new CNUsuario().insertarUsuario("Ramiro Ramirez Shang", 202550479, 26, "123458");
            new CNUsuario().insertarUsuario("Pedro Soto Hang", 202550479, 46, "123459");
            new CNUsuario().insertarUsuario("Admin", 207530479, 24, "2141996");
            new CNUsuario().insertarUsuario("este", 203330444, 24, "12345678");
            */
            // new CNUsuario().insertarUsuario("Jose Luis Sequeira", 1, 20, "1");
        }


        public static string GetSHA256(string str)
        {
            SHA256 sha256 = SHA256Managed.Create();
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] stream = null;
            StringBuilder sb = new StringBuilder();
            stream = sha256.ComputeHash(encoding.GetBytes(str));
            for (int i = 0; i < stream.Length; i++) sb.AppendFormat("{0:x2}", stream[i]);
            return sb.ToString();
        }



        private void FrmInicioSesion_Load(object sender, EventArgs e)
        {

        }

        private void lblRegistrarse_Click(object sender, EventArgs e)
        {
            FrmRegistro frm = new FrmRegistro(this);
            frm.Visible = true;
            this.Hide();
        }

        private void btnInicio_Click(object sender, EventArgs e)
        {

            int usuarioSesion = int.Parse(txtUsuario.Text);
            string contrasenna = txtContrasenna.Text;



            this.usuario= new CNUsuario().seleccionarUsuario(usuarioSesion, contrasenna);

    

            if (this.usuario.Nombre !="" && this.usuario.Tipo=="Admin") {

                FrmMantenimiento frm = new FrmMantenimiento(this);
                frm.Visible = true;
                this.Hide();

            }

            if (this.usuario.Nombre != "" && this.usuario.Tipo == "Pasajero")
            {

                FrmPasajeroMenu frm = new FrmPasajeroMenu(this,this.usuario);
                frm.Visible = true;
                this.Hide();

            }

        }
    }
}
