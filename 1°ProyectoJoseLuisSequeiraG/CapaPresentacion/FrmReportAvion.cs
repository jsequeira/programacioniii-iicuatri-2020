﻿using CapaNegocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion
{
    public partial class FrmReportAvion : Form
    {
        FrmReportMenu menu;
        public FrmReportAvion(FrmReportMenu menu)
        {
            InitializeComponent();
            this.menu = menu;
            definirAvion();
            seleccionarAvioncantidad();
            CenterToParent();

        }


        public void seleccionarAvioncantidad(){

            new CNVuelo().seleccionarPorvuelos(dgvAvion);
           
        }

        public void definirAvion()
        {

            DataTable dataAvion = new DataTable();

            dataAvion.Columns.Add("Id_avion");

            dataAvion.Columns.Add("Modelo");

            dataAvion.Columns.Add("Annio_construccion");

            dataAvion.Columns.Add("Capacidad");

            dataAvion.Columns.Add("Estado");

            dataAvion.Columns.Add("Numero de vuelos");


            dgvAvion.DataSource = dataAvion;

  

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            menu.Visible=true;
        }
    }
}
