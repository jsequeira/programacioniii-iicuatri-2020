﻿using CapaNegocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion
{
    public partial class FrmMantTripulacion : Form
    {

        FrmMantenimiento mant;

        string estado="";
        int id_aerolinea = 0;

        public FrmMantTripulacion(FrmMantenimiento mant)
        {

            InitializeComponent();
            CenterToParent();

            this.mant = mant;
            cbRol.SelectedIndex = 0;
            cbRol.DropDownStyle = ComboBoxStyle.DropDownList;
            definirdatagridviewAerolinea();
            definirdatagridviewTripulante();
            cargarAerolineas();
            cargarTripulacion();
            dgvAerolinea.SelectionMode= DataGridViewSelectionMode.FullRowSelect;//Establecer escoger una fila compreta
        }


        public void definirdatagridviewAerolinea()
        {

            DataTable dataTable = new DataTable();
            dataTable.Columns.Add("id_aerolinea");
            dataTable.Columns.Add("Nombre");
            dataTable.Columns.Add("Año");
            dataTable.Columns.Add("Tipo");
            dgvAerolinea.DataSource = dataTable;


        }

        public void definirdatagridviewTripulante()
        {

            DataTable dataTable = new DataTable();   
            dataTable.Columns.Add("Nombre");
            dataTable.Columns.Add("Cedula");
            dataTable.Columns.Add("Fecha de nacimiento");
            dataTable.Columns.Add("Id_aerolinea");
            dataTable.Columns.Add("Rol");
            dataTable.Columns.Add("Estado");
            dgvTripulacion.DataSource = dataTable;




        }
        public void cargarAerolineas()
        {

            new CNAerolinea().seleccionarAerolineas(dgvAerolinea);
            
        }



        public void cargarTripulacion()
        {

            new CNTripulacion().seleccionarTripulacion(dgvTripulacion);

        }


        private void btnAceptarAerolinea_Click(object sender, EventArgs e)
        {
            dgvAerolinea.Columns.Clear();

            definirdatagridviewAerolinea();

            new CNAerolinea().buscarAerolinea(dgvAerolinea, txtBuscaraerolinea.Text);
        }

        private void btnAceptartripulante_Click(object sender, EventArgs e)
        {
            dgvTripulacion.Columns.Clear();

            definirdatagridviewTripulante();

            new CNTripulacion().buscarTripulacion(dgvTripulacion, txtBuscartripulante.Text);
        }

        private void btnRegistrar_Click(object sender, EventArgs e)

        {
            string estado;

            if (ckbDisponible.Checked == true) {

                estado= "Disponible";

            }
            else {
                estado = "En servicio";

            }


            try
            {
                if (txtNombre.Text != "" || txtCedula.Text != "")
                {


                    new CNTripulacion().insertarTripulacion(txtNombre.Text, int.Parse(txtCedula.Text), dtpAnnio.Value.ToString(), id_aerolinea, cbRol.SelectedItem.ToString(), estado);

                    dgvAerolinea.Columns.Clear();
                    dgvTripulacion.Columns.Clear();

                    definirdatagridviewAerolinea();
                    cargarAerolineas();
                    definirdatagridviewTripulante();
                    cargarTripulacion();
                    txtNombre.Text = "";
                    txtCedula.Text = "";
                    cbRol.SelectedIndex = 0;
                    ckbDisponible.Checked = true;
                    dtpAnnio = new DateTimePicker();

                }
                else
                {
                    MessageBox.Show("Rellenar todos los campos");
                }

            }
            catch {

                MessageBox.Show("Digitar números enteros en el campo[Cedula]");
            }
           
        }

     

        private void btnAtras_Click(object sender, EventArgs e)
        {
            mant.Visible = true;
            this.Hide();
        }

        private void dgvAerolinea_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {

                this.id_aerolinea = int.Parse(dgvAerolinea.Rows[e.RowIndex].Cells[0].Value.ToString());

            }
        }
    }
}