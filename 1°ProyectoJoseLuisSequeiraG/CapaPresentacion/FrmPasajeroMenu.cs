﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades;
namespace CapaPresentacion
{
    public partial class FrmPasajeroMenu : Form
    {
        Usuario usuario;
        FrmInicioSesion ini;
        public FrmPasajeroMenu(FrmInicioSesion ini,Usuario usuario)
        {
            InitializeComponent();
            this.usuario = usuario;
            this.ini = ini;
            CenterToParent();
            lblNombre.Text = usuario.Nombre;
            lblCedula.Text = usuario.Cedula.ToString();
        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            ini.Visible = true;
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FrmHistorial frm = new FrmHistorial(this,usuario);
            frm.Visible = true;
            this.Hide();


        }

        private void button1_Click(object sender, EventArgs e)
        {
            FrmPasajerorRegVuelo frm=  new FrmPasajerorRegVuelo(this,usuario);
            frm.Visible = true;
            this.Hide();

        }
    }
}
