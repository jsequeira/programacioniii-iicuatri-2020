﻿namespace CapaPresentacion
{
    partial class FrmReportTripulacion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvAerolinea = new System.Windows.Forms.DataGridView();
            this.dgvTripulacion = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAerolinea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTripulacion)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvAerolinea
            // 
            this.dgvAerolinea.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAerolinea.Location = new System.Drawing.Point(12, 30);
            this.dgvAerolinea.Name = "dgvAerolinea";
            this.dgvAerolinea.RowHeadersWidth = 51;
            this.dgvAerolinea.RowTemplate.Height = 24;
            this.dgvAerolinea.Size = new System.Drawing.Size(696, 301);
            this.dgvAerolinea.TabIndex = 0;
            this.dgvAerolinea.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAerolinea_CellClick);
            // 
            // dgvTripulacion
            // 
            this.dgvTripulacion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTripulacion.Location = new System.Drawing.Point(12, 367);
            this.dgvTripulacion.Name = "dgvTripulacion";
            this.dgvTripulacion.RowHeadersWidth = 51;
            this.dgvTripulacion.RowTemplate.Height = 24;
            this.dgvTripulacion.Size = new System.Drawing.Size(696, 301);
            this.dgvTripulacion.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Aerolinea";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 347);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Tripulacion";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(15, 687);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Atras";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // FrmReportTripulacion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(722, 716);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgvTripulacion);
            this.Controls.Add(this.dgvAerolinea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FrmReportTripulacion";
            this.Text = "FrmReportTripulacion";
            ((System.ComponentModel.ISupportInitialize)(this.dgvAerolinea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTripulacion)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvAerolinea;
        private System.Windows.Forms.DataGridView dgvTripulacion;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
    }
}