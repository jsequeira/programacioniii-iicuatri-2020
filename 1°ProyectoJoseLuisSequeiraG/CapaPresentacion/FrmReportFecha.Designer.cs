﻿namespace CapaPresentacion
{
    partial class FrmReportFecha
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvVuelo = new System.Windows.Forms.DataGridView();
            this.dtpFechainicial = new System.Windows.Forms.DateTimePicker();
            this.dtpFechafinal = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnAtras = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVuelo)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvVuelo
            // 
            this.dgvVuelo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVuelo.Location = new System.Drawing.Point(12, 65);
            this.dgvVuelo.Name = "dgvVuelo";
            this.dgvVuelo.RowHeadersWidth = 51;
            this.dgvVuelo.RowTemplate.Height = 24;
            this.dgvVuelo.Size = new System.Drawing.Size(776, 294);
            this.dgvVuelo.TabIndex = 0;
            // 
            // dtpFechainicial
            // 
            this.dtpFechainicial.CustomFormat = "dd/MM/yyyy";
            this.dtpFechainicial.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFechainicial.Location = new System.Drawing.Point(123, 9);
            this.dtpFechainicial.Name = "dtpFechainicial";
            this.dtpFechainicial.Size = new System.Drawing.Size(200, 22);
            this.dtpFechainicial.TabIndex = 1;
            this.dtpFechainicial.ValueChanged += new System.EventHandler(this.dtpFechainicial_ValueChanged);
            // 
            // dtpFechafinal
            // 
            this.dtpFechafinal.CustomFormat = "dd/MM/yyyy";
            this.dtpFechafinal.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFechafinal.Location = new System.Drawing.Point(446, 9);
            this.dtpFechafinal.Name = "dtpFechafinal";
            this.dtpFechafinal.Size = new System.Drawing.Size(200, 22);
            this.dtpFechafinal.TabIndex = 2;
            this.dtpFechafinal.ValueChanged += new System.EventHandler(this.dtpFechafinal_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Fecha Inicio:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(363, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Fecha final";
            // 
            // btnAtras
            // 
            this.btnAtras.Location = new System.Drawing.Point(13, 378);
            this.btnAtras.Name = "btnAtras";
            this.btnAtras.Size = new System.Drawing.Size(75, 23);
            this.btnAtras.TabIndex = 5;
            this.btnAtras.Text = "Atras";
            this.btnAtras.UseVisualStyleBackColor = true;
            this.btnAtras.Click += new System.EventHandler(this.btnAtras_Click);
            // 
            // FrmReportFecha
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(799, 416);
            this.Controls.Add(this.btnAtras);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtpFechafinal);
            this.Controls.Add(this.dtpFechainicial);
            this.Controls.Add(this.dgvVuelo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FrmReportFecha";
            this.Text = "FrmReportFecha";
            ((System.ComponentModel.ISupportInitialize)(this.dgvVuelo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvVuelo;
        private System.Windows.Forms.DateTimePicker dtpFechainicial;
        private System.Windows.Forms.DateTimePicker dtpFechafinal;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnAtras;
    }
}