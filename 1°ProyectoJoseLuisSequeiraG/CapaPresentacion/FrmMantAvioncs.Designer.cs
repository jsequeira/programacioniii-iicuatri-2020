﻿namespace CapaPresentacion
{
    partial class FrmMantAvioncs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtCapacidad = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnBuscarmodelo = new System.Windows.Forms.Button();
            this.txtBuscarmodelo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvModelo = new System.Windows.Forms.DataGridView();
            this.btnBuscaraerolinea = new System.Windows.Forms.Button();
            this.txBuscaraerolinea = new System.Windows.Forms.TextBox();
            this.ckbDisponible = new System.Windows.Forms.CheckBox();
            this.dgvAerolinea = new System.Windows.Forms.DataGridView();
            this.dtpAnnio = new System.Windows.Forms.DateTimePicker();
            this.btnRegistrar = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.dgvAvion = new System.Windows.Forms.DataGridView();
            this.btnBuscaravion = new System.Windows.Forms.Button();
            this.txtBuscaravion = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnAtras = new System.Windows.Forms.Button();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvModelo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAerolinea)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAvion)).BeginInit();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.txtCapacidad);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.btnBuscarmodelo);
            this.panel3.Controls.Add(this.txtBuscarmodelo);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.dgvModelo);
            this.panel3.Controls.Add(this.btnBuscaraerolinea);
            this.panel3.Controls.Add(this.txBuscaraerolinea);
            this.panel3.Controls.Add(this.ckbDisponible);
            this.panel3.Controls.Add(this.dgvAerolinea);
            this.panel3.Controls.Add(this.dtpAnnio);
            this.panel3.Controls.Add(this.btnRegistrar);
            this.panel3.Controls.Add(this.label12);
            this.panel3.Controls.Add(this.label13);
            this.panel3.Location = new System.Drawing.Point(12, 12);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(550, 582);
            this.panel3.TabIndex = 11;
            // 
            // txtCapacidad
            // 
            this.txtCapacidad.Location = new System.Drawing.Point(156, 96);
            this.txtCapacidad.Name = "txtCapacidad";
            this.txtCapacidad.Size = new System.Drawing.Size(66, 22);
            this.txtCapacidad.TabIndex = 20;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(71, 96);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 17);
            this.label3.TabIndex = 19;
            this.label3.Text = "Capacidad:";
            // 
            // btnBuscarmodelo
            // 
            this.btnBuscarmodelo.Location = new System.Drawing.Point(433, 154);
            this.btnBuscarmodelo.Name = "btnBuscarmodelo";
            this.btnBuscarmodelo.Size = new System.Drawing.Size(75, 23);
            this.btnBuscarmodelo.TabIndex = 18;
            this.btnBuscarmodelo.Text = "Buscar";
            this.btnBuscarmodelo.UseVisualStyleBackColor = true;
            this.btnBuscarmodelo.Click += new System.EventHandler(this.btnBuscarmodelo_Click);
            // 
            // txtBuscarmodelo
            // 
            this.txtBuscarmodelo.Location = new System.Drawing.Point(37, 154);
            this.txtBuscarmodelo.Name = "txtBuscarmodelo";
            this.txtBuscarmodelo.Size = new System.Drawing.Size(390, 22);
            this.txtBuscarmodelo.TabIndex = 17;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(34, 134);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 17);
            this.label1.TabIndex = 16;
            this.label1.Text = "Modelo de avion:";
            // 
            // dgvModelo
            // 
            this.dgvModelo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvModelo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvModelo.Location = new System.Drawing.Point(37, 181);
            this.dgvModelo.MultiSelect = false;
            this.dgvModelo.Name = "dgvModelo";
            this.dgvModelo.ReadOnly = true;
            this.dgvModelo.RowHeadersWidth = 51;
            this.dgvModelo.RowTemplate.Height = 24;
            this.dgvModelo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvModelo.Size = new System.Drawing.Size(252, 109);
            this.dgvModelo.TabIndex = 15;
            this.dgvModelo.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvModelo_CellClick);
            // 
            // btnBuscaraerolinea
            // 
            this.btnBuscaraerolinea.Location = new System.Drawing.Point(433, 335);
            this.btnBuscaraerolinea.Name = "btnBuscaraerolinea";
            this.btnBuscaraerolinea.Size = new System.Drawing.Size(75, 23);
            this.btnBuscaraerolinea.TabIndex = 6;
            this.btnBuscaraerolinea.Text = "Buscar";
            this.btnBuscaraerolinea.UseVisualStyleBackColor = true;
            this.btnBuscaraerolinea.Click += new System.EventHandler(this.btnBuscaraerolinea_Click);
            // 
            // txBuscaraerolinea
            // 
            this.txBuscaraerolinea.Location = new System.Drawing.Point(37, 336);
            this.txBuscaraerolinea.Name = "txBuscaraerolinea";
            this.txBuscaraerolinea.Size = new System.Drawing.Size(390, 22);
            this.txBuscaraerolinea.TabIndex = 5;
            this.txBuscaraerolinea.TextChanged += new System.EventHandler(this.txBuscaraerolinea_TextChanged);
            // 
            // ckbDisponible
            // 
            this.ckbDisponible.AutoSize = true;
            this.ckbDisponible.Checked = true;
            this.ckbDisponible.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckbDisponible.Location = new System.Drawing.Point(244, 64);
            this.ckbDisponible.Name = "ckbDisponible";
            this.ckbDisponible.Size = new System.Drawing.Size(96, 21);
            this.ckbDisponible.TabIndex = 14;
            this.ckbDisponible.Text = "Disponible";
            this.ckbDisponible.UseVisualStyleBackColor = true;
            // 
            // dgvAerolinea
            // 
            this.dgvAerolinea.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAerolinea.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2;
            this.dgvAerolinea.Location = new System.Drawing.Point(37, 364);
            this.dgvAerolinea.MultiSelect = false;
            this.dgvAerolinea.Name = "dgvAerolinea";
            this.dgvAerolinea.ReadOnly = true;
            this.dgvAerolinea.RowHeadersWidth = 51;
            this.dgvAerolinea.RowTemplate.Height = 24;
            this.dgvAerolinea.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAerolinea.Size = new System.Drawing.Size(471, 178);
            this.dgvAerolinea.TabIndex = 11;
            this.dgvAerolinea.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAerolinea_CellClick);
            // 
            // dtpAnnio
            // 
            this.dtpAnnio.CustomFormat = "yyyy";
            this.dtpAnnio.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpAnnio.Location = new System.Drawing.Point(156, 63);
            this.dtpAnnio.Name = "dtpAnnio";
            this.dtpAnnio.ShowUpDown = true;
            this.dtpAnnio.Size = new System.Drawing.Size(66, 22);
            this.dtpAnnio.TabIndex = 7;
            this.dtpAnnio.Value = new System.DateTime(2020, 7, 8, 0, 0, 0, 0);
            // 
            // btnRegistrar
            // 
            this.btnRegistrar.Location = new System.Drawing.Point(214, 545);
            this.btnRegistrar.Name = "btnRegistrar";
            this.btnRegistrar.Size = new System.Drawing.Size(75, 32);
            this.btnRegistrar.TabIndex = 6;
            this.btnRegistrar.Text = "Registrar";
            this.btnRegistrar.UseVisualStyleBackColor = true;
            this.btnRegistrar.Click += new System.EventHandler(this.btnRegistrar_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(21, 63);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(129, 17);
            this.label12.TabIndex = 1;
            this.label12.Text = "Fecha de creacion:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(23, 316);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(127, 17);
            this.label13.TabIndex = 2;
            this.label13.Text = "Escoger aerolinea:";
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.dgvAvion);
            this.panel4.Controls.Add(this.btnBuscaravion);
            this.panel4.Controls.Add(this.txtBuscaravion);
            this.panel4.Location = new System.Drawing.Point(584, 70);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(482, 223);
            this.panel4.TabIndex = 15;
            // 
            // dgvAvion
            // 
            this.dgvAvion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAvion.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2;
            this.dgvAvion.Location = new System.Drawing.Point(3, 36);
            this.dgvAvion.MultiSelect = false;
            this.dgvAvion.Name = "dgvAvion";
            this.dgvAvion.ReadOnly = true;
            this.dgvAvion.RowHeadersWidth = 51;
            this.dgvAvion.RowTemplate.Height = 24;
            this.dgvAvion.Size = new System.Drawing.Size(474, 178);
            this.dgvAvion.TabIndex = 3;
            // 
            // btnBuscaravion
            // 
            this.btnBuscaravion.Location = new System.Drawing.Point(399, 7);
            this.btnBuscaravion.Name = "btnBuscaravion";
            this.btnBuscaravion.Size = new System.Drawing.Size(75, 23);
            this.btnBuscaravion.TabIndex = 2;
            this.btnBuscaravion.Text = "Buscar";
            this.btnBuscaravion.UseVisualStyleBackColor = true;
            this.btnBuscaravion.Click += new System.EventHandler(this.btnBuscaravion_Click);
            // 
            // txtBuscaravion
            // 
            this.txtBuscaravion.Location = new System.Drawing.Point(3, 8);
            this.txtBuscaravion.Name = "txtBuscaravion";
            this.txtBuscaravion.Size = new System.Drawing.Size(390, 22);
            this.txtBuscaravion.TabIndex = 1;
            this.txtBuscaravion.TextChanged += new System.EventHandler(this.txtBuscaravion_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(585, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(137, 17);
            this.label2.TabIndex = 16;
            this.label2.Text = "Aviones registrados:";
            // 
            // btnAtras
            // 
            this.btnAtras.Location = new System.Drawing.Point(12, 609);
            this.btnAtras.Name = "btnAtras";
            this.btnAtras.Size = new System.Drawing.Size(75, 23);
            this.btnAtras.TabIndex = 17;
            this.btnAtras.Text = "Atras";
            this.btnAtras.UseVisualStyleBackColor = true;
            this.btnAtras.Click += new System.EventHandler(this.btnAtras_Click);
            // 
            // FrmMantAvioncs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1078, 644);
            this.Controls.Add(this.btnAtras);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FrmMantAvioncs";
            this.Text = "FrmMantAvionescs";
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvModelo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAerolinea)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAvion)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnBuscaraerolinea;
        private System.Windows.Forms.TextBox txBuscaraerolinea;
        private System.Windows.Forms.CheckBox ckbDisponible;
        private System.Windows.Forms.DataGridView dgvAerolinea;
        private System.Windows.Forms.DateTimePicker dtpAnnio;
        private System.Windows.Forms.Button btnRegistrar;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DataGridView dgvModelo;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.DataGridView dgvAvion;
        private System.Windows.Forms.Button btnBuscaravion;
        private System.Windows.Forms.TextBox txtBuscaravion;
        private System.Windows.Forms.Button btnBuscarmodelo;
        private System.Windows.Forms.TextBox txtBuscarmodelo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCapacidad;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnAtras;
    }
}