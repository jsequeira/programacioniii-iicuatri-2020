﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion
{
    public partial class FrmMantenimiento : Form
    {

        FrmInicioSesion ini;

        public FrmMantenimiento(FrmInicioSesion ini)
        {
            InitializeComponent();
            CenterToScreen();
            this.ini = ini;
        }

        public FrmMantenimiento()
        {
            InitializeComponent();
            CenterToScreen();
            this.ini = ini;
        }

        private void FrmMantenimiento_Load(object sender, EventArgs e)
        {

        }

       

        private void btnAerolineas_Click(object sender, EventArgs e)
        {
            FrmMantAerolinea frm = new FrmMantAerolinea(this);
            frm.Visible = true;
            this.Hide();
        }

        private void btnTripulaciones_Click(object sender, EventArgs e)
        {
            FrmMantTripulacion frm = new FrmMantTripulacion(this);
            frm.Visible = true;
            this.Hide();
        }

        private void btnAviones_Click(object sender, EventArgs e)
        {
            FrmMantAvioncs frm = new FrmMantAvioncs(this);
            frm.Visible=true;
            this.Hide();
        }

        private void btnAeropuertos_Click(object sender, EventArgs e)
        {
            FrmMantAeropuerto frm= new FrmMantAeropuerto(this);
            frm.Visible=true;
            this.Hide();

        }

        private void btnVuelos_Click(object sender, EventArgs e)
        {
            FrmMantVueloReg frm= new FrmMantVueloReg(this);
            frm.Visible=true;
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            FrmReportMenu frm = new FrmReportMenu(this);
            frm.Visible = true;
            this.Hide();

        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            FrmInicioSesion FRM = new FrmInicioSesion();
            FRM.Visible = true;
            this.Hide();
        }
    }
}
