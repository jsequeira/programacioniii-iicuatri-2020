﻿namespace CapaPresentacion
{
    partial class FrmMantTripulacion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnAtras = new System.Windows.Forms.Button();
            this.btnAceptarAerolinea = new System.Windows.Forms.Button();
            this.txtBuscaraerolinea = new System.Windows.Forms.TextBox();
            this.ckbDisponible = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cbRol = new System.Windows.Forms.ComboBox();
            this.dgvAerolinea = new System.Windows.Forms.DataGridView();
            this.label4 = new System.Windows.Forms.Label();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.dtpAnnio = new System.Windows.Forms.DateTimePicker();
            this.btnRegistrar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCedula = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dgvTripulacion = new System.Windows.Forms.DataGridView();
            this.btnAceptartripulante = new System.Windows.Forms.Button();
            this.txtBuscartripulante = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAerolinea)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTripulacion)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.btnAtras);
            this.panel2.Controls.Add(this.btnAceptarAerolinea);
            this.panel2.Controls.Add(this.txtBuscaraerolinea);
            this.panel2.Controls.Add(this.ckbDisponible);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.cbRol);
            this.panel2.Controls.Add(this.dgvAerolinea);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.txtNombre);
            this.panel2.Controls.Add(this.dtpAnnio);
            this.panel2.Controls.Add(this.btnRegistrar);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.txtCedula);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Location = new System.Drawing.Point(12, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(550, 416);
            this.panel2.TabIndex = 10;
            // 
            // btnAtras
            // 
            this.btnAtras.Location = new System.Drawing.Point(43, 380);
            this.btnAtras.Name = "btnAtras";
            this.btnAtras.Size = new System.Drawing.Size(75, 23);
            this.btnAtras.TabIndex = 15;
            this.btnAtras.Text = "Atras";
            this.btnAtras.UseVisualStyleBackColor = true;
            this.btnAtras.Click += new System.EventHandler(this.btnAtras_Click);
            // 
            // btnAceptarAerolinea
            // 
            this.btnAceptarAerolinea.Location = new System.Drawing.Point(439, 152);
            this.btnAceptarAerolinea.Name = "btnAceptarAerolinea";
            this.btnAceptarAerolinea.Size = new System.Drawing.Size(75, 23);
            this.btnAceptarAerolinea.TabIndex = 6;
            this.btnAceptarAerolinea.Text = "Aceptar";
            this.btnAceptarAerolinea.UseVisualStyleBackColor = true;
            this.btnAceptarAerolinea.Click += new System.EventHandler(this.btnAceptarAerolinea_Click);
            // 
            // txtBuscaraerolinea
            // 
            this.txtBuscaraerolinea.Location = new System.Drawing.Point(333, 153);
            this.txtBuscaraerolinea.Name = "txtBuscaraerolinea";
            this.txtBuscaraerolinea.Size = new System.Drawing.Size(100, 22);
            this.txtBuscaraerolinea.TabIndex = 5;
            // 
            // ckbDisponible
            // 
            this.ckbDisponible.AutoSize = true;
            this.ckbDisponible.Checked = true;
            this.ckbDisponible.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckbDisponible.Location = new System.Drawing.Point(439, 91);
            this.ckbDisponible.Name = "ckbDisponible";
            this.ckbDisponible.Size = new System.Drawing.Size(96, 21);
            this.ckbDisponible.TabIndex = 14;
            this.ckbDisponible.Text = "Disponible";
            this.ckbDisponible.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(275, 156);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 17);
            this.label7.TabIndex = 4;
            this.label7.Text = "Buscar";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(115, 91);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 17);
            this.label6.TabIndex = 13;
            this.label6.Text = "Rol:";
            // 
            // cbRol
            // 
            this.cbRol.FormattingEnabled = true;
            this.cbRol.Items.AddRange(new object[] {
            "Piloto",
            "servicio al cliente"});
            this.cbRol.Location = new System.Drawing.Point(154, 89);
            this.cbRol.Name = "cbRol";
            this.cbRol.Size = new System.Drawing.Size(263, 24);
            this.cbRol.TabIndex = 12;
            // 
            // dgvAerolinea
            // 
            this.dgvAerolinea.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAerolinea.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2;
            this.dgvAerolinea.Location = new System.Drawing.Point(43, 181);
            this.dgvAerolinea.MultiSelect = false;
            this.dgvAerolinea.Name = "dgvAerolinea";
            this.dgvAerolinea.ReadOnly = true;
            this.dgvAerolinea.RowHeadersWidth = 51;
            this.dgvAerolinea.RowTemplate.Height = 24;
            this.dgvAerolinea.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAerolinea.Size = new System.Drawing.Size(471, 178);
            this.dgvAerolinea.TabIndex = 11;
            this.dgvAerolinea.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAerolinea_CellClick);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(86, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 17);
            this.label4.TabIndex = 8;
            this.label4.Text = "Nombre:";
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(154, 12);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(263, 22);
            this.txtNombre.TabIndex = 9;
            // 
            // dtpAnnio
            // 
            this.dtpAnnio.CustomFormat = "dd/MM/yyyy";
            this.dtpAnnio.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpAnnio.Location = new System.Drawing.Point(154, 63);
            this.dtpAnnio.Name = "dtpAnnio";
            this.dtpAnnio.Size = new System.Drawing.Size(263, 22);
            this.dtpAnnio.TabIndex = 7;
            this.dtpAnnio.Value = new System.DateTime(2020, 7, 8, 0, 0, 0, 0);
            // 
            // btnRegistrar
            // 
            this.btnRegistrar.Location = new System.Drawing.Point(235, 380);
            this.btnRegistrar.Name = "btnRegistrar";
            this.btnRegistrar.Size = new System.Drawing.Size(75, 23);
            this.btnRegistrar.TabIndex = 6;
            this.btnRegistrar.Text = "Registrar";
            this.btnRegistrar.UseVisualStyleBackColor = true;
            this.btnRegistrar.Click += new System.EventHandler(this.btnRegistrar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(92, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Cedula:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(143, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Fecha de nacimiento:";
            // 
            // txtCedula
            // 
            this.txtCedula.Location = new System.Drawing.Point(154, 38);
            this.txtCedula.Name = "txtCedula";
            this.txtCedula.Size = new System.Drawing.Size(263, 22);
            this.txtCedula.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 152);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(127, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Escoger aerolinea:";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.dgvTripulacion);
            this.panel1.Controls.Add(this.btnAceptartripulante);
            this.panel1.Controls.Add(this.txtBuscartripulante);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Location = new System.Drawing.Point(597, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(479, 221);
            this.panel1.TabIndex = 9;
            // 
            // dgvTripulacion
            // 
            this.dgvTripulacion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTripulacion.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2;
            this.dgvTripulacion.Location = new System.Drawing.Point(3, 35);
            this.dgvTripulacion.MultiSelect = false;
            this.dgvTripulacion.Name = "dgvTripulacion";
            this.dgvTripulacion.ReadOnly = true;
            this.dgvTripulacion.RowHeadersWidth = 51;
            this.dgvTripulacion.RowTemplate.Height = 24;
            this.dgvTripulacion.Size = new System.Drawing.Size(471, 178);
            this.dgvTripulacion.TabIndex = 3;
            // 
            // btnAceptartripulante
            // 
            this.btnAceptartripulante.Location = new System.Drawing.Point(167, 9);
            this.btnAceptartripulante.Name = "btnAceptartripulante";
            this.btnAceptartripulante.Size = new System.Drawing.Size(75, 23);
            this.btnAceptartripulante.TabIndex = 2;
            this.btnAceptartripulante.Text = "Aceptar";
            this.btnAceptartripulante.UseVisualStyleBackColor = true;
            this.btnAceptartripulante.Click += new System.EventHandler(this.btnAceptartripulante_Click);
            // 
            // txtBuscartripulante
            // 
            this.txtBuscartripulante.Location = new System.Drawing.Point(61, 8);
            this.txtBuscartripulante.Name = "txtBuscartripulante";
            this.txtBuscartripulante.Size = new System.Drawing.Size(100, 22);
            this.txtBuscartripulante.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 17);
            this.label5.TabIndex = 0;
            this.label5.Text = "Buscar";
            // 
            // FrmMantTripulacion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1087, 438);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FrmMantTripulacion";
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAerolinea)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTripulacion)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnAceptarAerolinea;
        private System.Windows.Forms.TextBox txtBuscaraerolinea;
        private System.Windows.Forms.CheckBox ckbDisponible;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbRol;
        private System.Windows.Forms.DataGridView dgvAerolinea;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.DateTimePicker dtpAnnio;
        private System.Windows.Forms.Button btnRegistrar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCedula;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dgvTripulacion;
        private System.Windows.Forms.Button btnAceptartripulante;
        private System.Windows.Forms.TextBox txtBuscartripulante;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnAtras;
    }
}