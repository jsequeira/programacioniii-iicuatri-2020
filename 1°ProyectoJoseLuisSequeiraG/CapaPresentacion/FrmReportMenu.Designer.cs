﻿namespace CapaPresentacion
{
    partial class FrmReportMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnFecha = new System.Windows.Forms.Button();
            this.btnAerolinea = new System.Windows.Forms.Button();
            this.btnAvion = new System.Windows.Forms.Button();
            this.btnReportmimtri = new System.Windows.Forms.Button();
            this.btnAtras = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnFecha
            // 
            this.btnFecha.Location = new System.Drawing.Point(57, 30);
            this.btnFecha.Name = "btnFecha";
            this.btnFecha.Size = new System.Drawing.Size(229, 23);
            this.btnFecha.TabIndex = 0;
            this.btnFecha.Text = "Reporte por rango de fecha";
            this.btnFecha.UseVisualStyleBackColor = true;
            this.btnFecha.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnAerolinea
            // 
            this.btnAerolinea.Location = new System.Drawing.Point(57, 86);
            this.btnAerolinea.Name = "btnAerolinea";
            this.btnAerolinea.Size = new System.Drawing.Size(229, 23);
            this.btnAerolinea.TabIndex = 1;
            this.btnAerolinea.Text = "Reporte por aerolinea especifico";
            this.btnAerolinea.UseVisualStyleBackColor = true;
            this.btnAerolinea.Click += new System.EventHandler(this.btnAerolinea_Click);
            // 
            // btnAvion
            // 
            this.btnAvion.Location = new System.Drawing.Point(57, 143);
            this.btnAvion.Name = "btnAvion";
            this.btnAvion.Size = new System.Drawing.Size(229, 23);
            this.btnAvion.TabIndex = 2;
            this.btnAvion.Text = "Reporte por cantidad";
            this.btnAvion.UseVisualStyleBackColor = true;
            this.btnAvion.Click += new System.EventHandler(this.btnAvion_Click);
            // 
            // btnReportmimtri
            // 
            this.btnReportmimtri.Location = new System.Drawing.Point(57, 197);
            this.btnReportmimtri.Name = "btnReportmimtri";
            this.btnReportmimtri.Size = new System.Drawing.Size(229, 23);
            this.btnReportmimtri.TabIndex = 3;
            this.btnReportmimtri.Text = "Miembros Tripulacion";
            this.btnReportmimtri.UseVisualStyleBackColor = true;
            this.btnReportmimtri.Click += new System.EventHandler(this.btnReportmimtri_Click);
            // 
            // btnAtras
            // 
            this.btnAtras.Location = new System.Drawing.Point(128, 245);
            this.btnAtras.Name = "btnAtras";
            this.btnAtras.Size = new System.Drawing.Size(75, 23);
            this.btnAtras.TabIndex = 4;
            this.btnAtras.Text = "Atras";
            this.btnAtras.UseVisualStyleBackColor = true;
            this.btnAtras.Click += new System.EventHandler(this.btnAtras_Click);
            // 
            // FrmReportMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(329, 285);
            this.Controls.Add(this.btnAtras);
            this.Controls.Add(this.btnReportmimtri);
            this.Controls.Add(this.btnAvion);
            this.Controls.Add(this.btnAerolinea);
            this.Controls.Add(this.btnFecha);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FrmReportMenu";
            this.Text = "FrmReportMenu";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnFecha;
        private System.Windows.Forms.Button btnAerolinea;
        private System.Windows.Forms.Button btnAvion;
        private System.Windows.Forms.Button btnReportmimtri;
        private System.Windows.Forms.Button btnAtras;
    }
}