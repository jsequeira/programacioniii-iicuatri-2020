﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaNegocio;
namespace CapaPresentacion
{
    public partial class FrmMantAerolinea : Form
    {
        FrmMantenimiento mant;

        public FrmMantAerolinea(FrmMantenimiento mant)
        {
            InitializeComponent();
            this.mant = mant;

            CenterToParent();
            cbTipo.SelectedIndex = 0;
            cbTipo.DropDownStyle=ComboBoxStyle.DropDownList;
            definirDataGridview();
            cargarAerolineas();
       


        }



        public void definirDataGridview() {

            DataTable dataTable = new DataTable();

          //  DataColumn edad = new DataColumn("Edad");
          //  edad.DataType = Type.GetType("System.Int32");

            dataTable.Columns.Add("Id_aerolinea");
            dataTable.Columns.Add("Nombre");
            dataTable.Columns.Add("Año");
            dataTable.Columns.Add("Tipo");

            dgv.DataSource = dataTable;


              //DataTable datatable = (DataTable)dgv.DataSource;
             //DataRow datarow1 = dataTable.NewRow();
            //datarow1["Nombre"] = "Nombre";
           //datarow1["Año"] ="año";
          //datarow1["Sexo"] = "sexo";
         // dataTable.Rows.Add(datarow1);
          


            
        }


  

            public void cargarAerolineas() {

            new CNAerolinea().seleccionarAerolineas(dgv);
;        
        }





        private void btnRegistrar_Click(object sender, EventArgs e)
        {

            if (txtNombre.Text != "")
            {

                string nombre = txtNombre.Text;
                string annio = dtpAnnio.Value.Year.ToString();
                string tipo = cbTipo.SelectedItem.ToString();
                new CNAerolinea().insertarAerolinea(nombre, annio, tipo);

                dgv.Columns.Clear();

                definirDataGridview();

                cargarAerolineas();
            }
            else {
                MessageBox.Show("Rellene todos los campos");
            
            }
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {

            dgv.Columns.Clear();

            definirDataGridview();

            new CNAerolinea().buscarAerolinea(dgv,txtBuscar.Text);
        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            this.mant.Visible= true;
            this.Hide();
        }
    }
}
