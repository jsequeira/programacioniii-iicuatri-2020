﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaNegocio;

namespace CapaPresentacion
{
    public partial class FrmReportAerolinea : Form
    {
        FrmReportMenu frmReport;
        DataTable dataTableVuelo;
        DataTable dataTableAero;
        public FrmReportAerolinea(FrmReportMenu frmReport)
        {
            InitializeComponent();
            CenterToScreen();
            this.frmReport = frmReport;
            definirdgvAero();
            definirdgvVuelo();
            cargarAerolinea();
            cargarVuelo();
        }

        private void dgvAerolinea_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }
        public void definirdgvAero()
        {

            dataTableAero = new DataTable();

            dataTableAero.Columns.Add("Id_aerolinea");
            dataTableAero.Columns.Add("Nombre");
            dataTableAero.Columns.Add("Año");
            dataTableAero.Columns.Add("Tipo");

            dgvAerolinea.DataSource = dataTableAero;

        }

        public void cargarAerolinea() {

            new CNAerolinea().seleccionarAerolineas(dgvAerolinea);

        }

        public void cargarVuelo()
        {

            new CNVuelo().seleccionaVuelos(dgvVuelo);

        }

        public void definirdgvVuelo()
            {


            dataTableVuelo = new DataTable();

            dataTableVuelo.Columns.Add("Id_aerolinea");
            dataTableVuelo.Columns.Add("Precio");
            dataTableVuelo.Columns.Add("Fecha Salida");
            dataTableVuelo.Columns.Add("Aeropuerto salida");
            dataTableVuelo.Columns.Add("Fecha llegada");
            dataTableVuelo.Columns.Add("Aeropuerto llegada");
            dataTableVuelo.Columns.Add("Id_avion");
            dataTableVuelo.Columns.Add("Id_piloto1");
            dataTableVuelo.Columns.Add("Id_piloto2");
            dataTableVuelo.Columns.Add("Id_servicio1");
            dataTableVuelo.Columns.Add("Id_servicio2");
            dataTableVuelo.Columns.Add("Id_servicio3");

                dgvVuelo.DataSource = dataTableVuelo;


            }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            this.frmReport = new FrmReportMenu();
            frmReport.Visible = true;
            this.Hide();
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {

        }

        private void dgvAerolinea_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                dataTableVuelo.DefaultView.RowFilter = $"Id_aerolinea LIKE '%{dgvAerolinea.Rows[e.RowIndex].Cells[0].Value.ToString()}%'";
            }
        }
    }
    }

