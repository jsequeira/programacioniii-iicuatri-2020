﻿namespace CapaPresentacion
{
    partial class FrmMantVueloReg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvAerolinea = new System.Windows.Forms.DataGridView();
            this.dgvaeroSalida = new System.Windows.Forms.DataGridView();
            this.dgvaeroLlegada = new System.Windows.Forms.DataGridView();
            this.IATA = new System.Windows.Forms.Label();
            this.txtPrecio = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpFechasalida = new System.Windows.Forms.DateTimePicker();
            this.dtpHorasalida = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.btnRegistrar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpHorallegada = new System.Windows.Forms.DateTimePicker();
            this.dtpFechallegada = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.dgvAvion = new System.Windows.Forms.DataGridView();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtDuracion = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.btnAtras = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAerolinea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvaeroSalida)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvaeroLlegada)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAvion)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvAerolinea
            // 
            this.dgvAerolinea.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvAerolinea.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAerolinea.Location = new System.Drawing.Point(24, 168);
            this.dgvAerolinea.Name = "dgvAerolinea";
            this.dgvAerolinea.RowHeadersWidth = 51;
            this.dgvAerolinea.RowTemplate.Height = 24;
            this.dgvAerolinea.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAerolinea.Size = new System.Drawing.Size(553, 150);
            this.dgvAerolinea.TabIndex = 0;
            this.dgvAerolinea.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAerolinea_CellClick);
            // 
            // dgvaeroSalida
            // 
            this.dgvaeroSalida.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvaeroSalida.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvaeroSalida.Location = new System.Drawing.Point(24, 344);
            this.dgvaeroSalida.Name = "dgvaeroSalida";
            this.dgvaeroSalida.RowHeadersWidth = 51;
            this.dgvaeroSalida.RowTemplate.Height = 24;
            this.dgvaeroSalida.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvaeroSalida.Size = new System.Drawing.Size(553, 150);
            this.dgvaeroSalida.TabIndex = 1;
            this.dgvaeroSalida.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvaeroSalida_CellClick);
            // 
            // dgvaeroLlegada
            // 
            this.dgvaeroLlegada.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvaeroLlegada.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvaeroLlegada.Location = new System.Drawing.Point(583, 344);
            this.dgvaeroLlegada.Name = "dgvaeroLlegada";
            this.dgvaeroLlegada.RowHeadersWidth = 51;
            this.dgvaeroLlegada.RowTemplate.Height = 24;
            this.dgvaeroLlegada.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvaeroLlegada.Size = new System.Drawing.Size(553, 150);
            this.dgvaeroLlegada.TabIndex = 2;
            this.dgvaeroLlegada.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvaeroLlegada_CellClick);
            // 
            // IATA
            // 
            this.IATA.AutoSize = true;
            this.IATA.Location = new System.Drawing.Point(65, 41);
            this.IATA.Name = "IATA";
            this.IATA.Size = new System.Drawing.Size(52, 17);
            this.IATA.TabIndex = 3;
            this.IATA.Text = "Precio:";
            // 
            // txtPrecio
            // 
            this.txtPrecio.Location = new System.Drawing.Point(135, 36);
            this.txtPrecio.Name = "txtPrecio";
            this.txtPrecio.Size = new System.Drawing.Size(200, 22);
            this.txtPrecio.TabIndex = 4;
            this.txtPrecio.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "Fecha salida:";
            // 
            // dtpFechasalida
            // 
            this.dtpFechasalida.CustomFormat = "dd-MM-yyyy";
            this.dtpFechasalida.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFechasalida.Location = new System.Drawing.Point(135, 64);
            this.dtpFechasalida.Name = "dtpFechasalida";
            this.dtpFechasalida.Size = new System.Drawing.Size(200, 22);
            this.dtpFechasalida.TabIndex = 6;
            this.dtpFechasalida.Value = new System.DateTime(2020, 7, 16, 0, 0, 0, 0);
            // 
            // dtpHorasalida
            // 
            this.dtpHorasalida.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpHorasalida.Location = new System.Drawing.Point(135, 92);
            this.dtpHorasalida.Name = "dtpHorasalida";
            this.dtpHorasalida.ShowUpDown = true;
            this.dtpHorasalida.Size = new System.Drawing.Size(200, 22);
            this.dtpHorasalida.TabIndex = 7;
            this.dtpHorasalida.Value = new System.DateTime(2020, 7, 14, 22, 48, 0, 0);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(35, 92);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 17);
            this.label3.TabIndex = 8;
            this.label3.Text = "hora salida:";
            // 
            // btnRegistrar
            // 
            this.btnRegistrar.Location = new System.Drawing.Point(535, 510);
            this.btnRegistrar.Name = "btnRegistrar";
            this.btnRegistrar.Size = new System.Drawing.Size(75, 34);
            this.btnRegistrar.TabIndex = 9;
            this.btnRegistrar.Text = "Registar";
            this.btnRegistrar.UseVisualStyleBackColor = true;
            this.btnRegistrar.Click += new System.EventHandler(this.btnRegistrar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(375, 97);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 17);
            this.label1.TabIndex = 15;
            this.label1.Text = "hora llegada:";
            // 
            // dtpHorallegada
            // 
            this.dtpHorallegada.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpHorallegada.Location = new System.Drawing.Point(475, 92);
            this.dtpHorallegada.Name = "dtpHorallegada";
            this.dtpHorallegada.ShowUpDown = true;
            this.dtpHorallegada.Size = new System.Drawing.Size(200, 22);
            this.dtpHorallegada.TabIndex = 14;
            // 
            // dtpFechallegada
            // 
            this.dtpFechallegada.CustomFormat = "dd-MM-yyyy";
            this.dtpFechallegada.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFechallegada.Location = new System.Drawing.Point(475, 64);
            this.dtpFechallegada.Name = "dtpFechallegada";
            this.dtpFechallegada.Size = new System.Drawing.Size(200, 22);
            this.dtpFechallegada.TabIndex = 13;
            this.dtpFechallegada.Value = new System.DateTime(2020, 7, 16, 0, 0, 0, 0);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(365, 69);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 17);
            this.label4.TabIndex = 12;
            this.label4.Text = "Fecha llegada:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 148);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 17);
            this.label5.TabIndex = 16;
            this.label5.Text = "Aerolinea:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(21, 321);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(120, 17);
            this.label6.TabIndex = 17;
            this.label6.Text = "Aeropuerto salida";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(612, 321);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(133, 17);
            this.label7.TabIndex = 18;
            this.label7.Text = "Aeropuerto llegada:";
            // 
            // dgvAvion
            // 
            this.dgvAvion.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvAvion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAvion.Location = new System.Drawing.Point(583, 168);
            this.dgvAvion.Name = "dgvAvion";
            this.dgvAvion.RowHeadersWidth = 51;
            this.dgvAvion.RowTemplate.Height = 24;
            this.dgvAvion.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAvion.Size = new System.Drawing.Size(553, 150);
            this.dgvAvion.TabIndex = 19;
            this.dgvAvion.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAvion_CellClick);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(580, 148);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 17);
            this.label8.TabIndex = 20;
            this.label8.Text = "Avion:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(392, 36);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(69, 17);
            this.label9.TabIndex = 21;
            this.label9.Text = "Duración:";
            // 
            // txtDuracion
            // 
            this.txtDuracion.Location = new System.Drawing.Point(475, 36);
            this.txtDuracion.Name = "txtDuracion";
            this.txtDuracion.Size = new System.Drawing.Size(200, 22);
            this.txtDuracion.TabIndex = 22;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(681, 41);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(44, 17);
            this.label10.TabIndex = 23;
            this.label10.Text = "horas";
            // 
            // btnAtras
            // 
            this.btnAtras.Location = new System.Drawing.Point(24, 520);
            this.btnAtras.Name = "btnAtras";
            this.btnAtras.Size = new System.Drawing.Size(75, 23);
            this.btnAtras.TabIndex = 24;
            this.btnAtras.Text = "Atras";
            this.btnAtras.UseVisualStyleBackColor = true;
            this.btnAtras.Click += new System.EventHandler(this.btnAtras_Click);
            // 
            // FrmMantVueloReg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1195, 559);
            this.Controls.Add(this.btnAtras);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtDuracion);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.dgvAvion);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtpHorallegada);
            this.Controls.Add(this.dtpFechallegada);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnRegistrar);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dtpHorasalida);
            this.Controls.Add(this.dtpFechasalida);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtPrecio);
            this.Controls.Add(this.IATA);
            this.Controls.Add(this.dgvaeroLlegada);
            this.Controls.Add(this.dgvaeroSalida);
            this.Controls.Add(this.dgvAerolinea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FrmMantVueloReg";
            this.Text = "FrmVuelo";
            this.Load += new System.EventHandler(this.FrmVuelo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAerolinea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvaeroSalida)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvaeroLlegada)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAvion)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvAerolinea;
        private System.Windows.Forms.DataGridView dgvaeroSalida;
        private System.Windows.Forms.DataGridView dgvaeroLlegada;
        private System.Windows.Forms.Label IATA;
        private System.Windows.Forms.TextBox txtPrecio;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpFechasalida;
        private System.Windows.Forms.DateTimePicker dtpHorasalida;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnRegistrar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtpHorallegada;
        private System.Windows.Forms.DateTimePicker dtpFechallegada;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridView dgvAvion;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtDuracion;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnAtras;
    }
}