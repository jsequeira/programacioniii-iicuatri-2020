﻿using CapaNegocio;
using Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion
{
    public partial class FrmHistorial : Form
    {
        Usuario usuario;
        FrmPasajeroMenu menu;
        public FrmHistorial(FrmPasajeroMenu menu, Usuario usuario)
        {
            InitializeComponent();
            this.menu = menu;
            this.usuario = usuario;
            CenterToScreen();
            definirDgv();
            cargarHistorial();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            menu.Visible = true;
            this.Hide();
        }



        public void definirDgv()
        {

                DataTable dataTable = new DataTable();

                dataTable.Columns.Add("Cedula");
                dataTable.Columns.Add("Pais_salida");
                dataTable.Columns.Add("Pais_llegada");
                dataTable.Columns.Add("Pais_escala1");
                dataTable.Columns.Add("Pais_escala2");
                dataTable.Columns.Add("Fecha/hora compra");
                dataTable.Columns.Add("Horas");
                dataTable.Columns.Add("Costo");        

                dgvHistorial.DataSource = dataTable;        

        }

        public void cargarHistorial() {

            new CNHistorial().seleccionarHistorial(dgvHistorial,usuario.Cedula);
        
        }
    
    }
}
