﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDatos;
namespace CapaNegocio
{
    public class CNUsuario
    {

        public Usuario seleccionarUsuario( int usuarioSesion, string contrasenna)
        {

            return new CDUsuario().seleccionarUsuario(usuarioSesion, contrasenna);

        }


        public void insertarUsuario(string nombre, int cedula, int edad, string contrasenna)
        {

            new CDUsuario().registrarUsuario(nombre, cedula, edad, contrasenna);

        }
    }
}
