﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaDatos;


namespace CapaNegocio
{
    public class CNVuelo
    {


        public void insertarAerolinea(int id_aerolinea, int precio, string fechahora_sda, string aeropuerto_sda, string fechahora_lda, string aeropuerto_lda,int duracion, int idPiloto1, int idPiloto2, int idServicio1, int idServicio2, int idServicio3, int id_avion) => new CDVuelo().insertarAerolinea(id_aerolinea, precio, fechahora_sda, aeropuerto_sda, fechahora_lda, aeropuerto_lda, duracion,idPiloto1, idPiloto2, idServicio1, idServicio2, idServicio3, id_avion);

        public void reportePorfecha(DataGridView dgv, string fechaInicial, string fecha_Final)
        {

            new CDVuelo().reportePorfecha(dgv, fechaInicial, fecha_Final);

        }

        public void seleccionaVuelos(DataGridView dgv)
        {

            new CDVuelo().seleccionarVuelos(dgv);

        }

        public void seleccionarPorvuelos(DataGridView dgv)
        {

            new CDVuelo().seleccionarPorvuelos(dgv);




        }


        public void seleccionarVuelosBusqueda(CheckBox ckb, DataGridView dgvNormal, DataGridView dgvEscala1, DataGridView dgvEscala2, DataGridView dgvEscala3, Panel panelNormal,Panel panelEscala ,string aeroSda, string aeroLda,string fechaSalida, string fechaVuelta)
        {

            new CDVuelo().seleccionarVuelosBusqueda(ckb, dgvNormal, dgvEscala1, dgvEscala2,dgvEscala3 ,panelNormal, panelEscala, aeroSda, aeroLda,fechaSalida,fechaVuelta);
     
        }


        public void seleccionaVueloInteligen(DataGridView dgv, string aeroSda, string aeroLda)
        {

          new CDVuelo().seleccionaVuelosInteligente(dgv,aeroSda, aeroLda);

        }

        public int obtenerPreciomenor() {

            return new CDVuelo().obtenerPrecioMenor();
        }

    }
}
