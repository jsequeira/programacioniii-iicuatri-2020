﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaDatos;

namespace CapaNegocio
{
    public class CNTripulacion
    {
        public void seleccionarTripulacion(DataGridView dgv) => new CDTripulacion().seleccionarTripulacion(dgv);

        public void buscarTripulacion(DataGridView dgv, string palabra) => new CDTripulacion().buscarTripulacion(dgv, palabra);


        public void insertarTripulacion(string nombre, int cedula, string fecha_nacimiento,int id_aerolinea, string rol, string estado) => new CDTripulacion().insertarTripulacion(nombre, cedula, fecha_nacimiento,id_aerolinea, rol, estado);


        public void seleccionarPiloto(List<string> lista) => new CDTripulacion().soloPiloto(lista);
        public void seleccionarServicio(List<string> lista) => new CDTripulacion().soloServicio(lista);

        public void modificarTripulacion(int id_Aerouerto) => new CDTripulacion().modificarTripulacion(id_Aerouerto);

        public void  cagarTripulacionAerolinea(DataGridView dgvTripulacion,string nombreAerolinea ) => new CDTripulacion().cargarTripulantesAerolinea(dgvTripulacion, nombreAerolinea);

        
    }
}
