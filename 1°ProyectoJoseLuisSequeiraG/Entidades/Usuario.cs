﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{






    public class Usuario
    {

        int id;
        int cedula;
        string nombre;
        int edad;
        string contrasenna;
        string tipo;

        public Usuario()
        {
        }

        public Usuario(int id, int cedula, string nombre, int edad, string contrasenna, string tipo)
        {
            this.Id = id;
            this.Cedula = cedula;
            this.Nombre = nombre;
            this.Edad = edad;
            this.Contrasenna = contrasenna;
            this.Tipo = tipo;
        }

        public int Id { get => id; set => id = value; }
        public int Cedula { get => cedula; set => cedula = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public int Edad { get => edad; set => edad = value; }
        public string Contrasenna { get => contrasenna; set => contrasenna = value; }
        public string Tipo { get => tipo; set => tipo = value; }
    }
}
