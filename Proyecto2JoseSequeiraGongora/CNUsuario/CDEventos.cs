﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace CapaDatos
{
    public class CDEventos
    {
        XmlDocument doc = new XmlDocument();

        string ruta = Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent. FullName, "gestion.xml");

        public void cargarCodigos(string usuCedula, DataGridView dgv, List<string> codigos, List<string> placas)
        {

            DataTable datatable = (DataTable)dgv.DataSource;


            doc.Load(ruta);

            XmlNodeList eventos = doc.SelectNodes("gestion/eventos/evento");

            XmlNode unEvento;

            for (int i = 0; i < eventos.Count; i++)
            {
                unEvento = eventos.Item(i);

                string cedula = unEvento.SelectSingleNode("cedula").InnerText;
                string estado = unEvento.SelectSingleNode("estado").InnerText;
                string placa = unEvento.SelectSingleNode("placa").InnerText;
                placas.Add(placa);

                if (cedula == usuCedula && estado=="Abierto")
                {



                    string codigo = unEvento.SelectSingleNode("codigo").InnerText;
                   
                    string lugar = unEvento.SelectSingleNode("lugar").InnerText;
                    string fecha = unEvento.SelectSingleNode("fecha").InnerText;
                    string multa = unEvento.SelectSingleNode("multa").InnerText;


                    codigos.Add(codigo);
                    


                    DataRow row = datatable.NewRow();
                    row["Codigo"] = codigo;
                    row["Placa"] = placa;
                    row["Lugar"] = lugar;
                    row["Fecha"] = fecha;
                    row["Multa"] = multa;
                    datatable.Rows.Add(row);
                    dgv.DataSource = datatable;

                }

            }

        }

        public void cargarSoloeventos(int operacion, DataGridView dgv)
        {

            DataTable datatable = (DataTable)dgv.DataSource;


            doc.Load(ruta);

            XmlNodeList eventos = doc.SelectNodes("gestion/eventos/evento");

            XmlNode unEvento;

            for (int i = 0; i < eventos.Count; i++)
            {
                unEvento = eventos.Item(i);

                string estado = unEvento.SelectSingleNode("estado").InnerText;


                if (estado == "Abierto" && operacion == 1)
                {



                    string codigo = unEvento.SelectSingleNode("codigo").InnerText;
                    string placa = unEvento.SelectSingleNode("placa").InnerText;
                    string lugar = unEvento.SelectSingleNode("lugar").InnerText;
                    string fecha = unEvento.SelectSingleNode("fecha").InnerText;
                    string multa = unEvento.SelectSingleNode("multa").InnerText;
                    string numeroParte = unEvento.SelectSingleNode("numeroparte").InnerText;




                    DataRow row = datatable.NewRow();
                    row["Codigo"] = codigo;
                    row["Placa"] = placa;
                    row["Lugar"] = lugar;
                    row["Fecha"] = fecha;
                    row["Multa"] = multa;
                    datatable.Rows.Add(row);
                    dgv.DataSource = datatable;

                }
                if (estado == "Por aprobar" && operacion == 2)
                {



                    string codigo = unEvento.SelectSingleNode("codigo").InnerText;
                    string placa = unEvento.SelectSingleNode("placa").InnerText;
                    string lugar = unEvento.SelectSingleNode("lugar").InnerText;
                    string fecha = unEvento.SelectSingleNode("fecha").InnerText;
                    string multa = unEvento.SelectSingleNode("multa").InnerText;





                    DataRow row = datatable.NewRow();
                    row["Codigo"] = codigo;
                    row["Placa"] = placa;
                    row["Lugar"] = lugar;
                    row["Fecha"] = fecha;
                    row["Multa"] = multa;
                    datatable.Rows.Add(row);
                    dgv.DataSource = datatable;

                }

            }

        }


        public void agregarEvento(string codigo, string cedula, string lugar, string placa, string estado, string fecha, string multa)
        {
            doc.Load(ruta);

            XmlNode evento = agregarEvento2(codigo, cedula, lugar, placa, estado, fecha, multa);

            XmlNode node = doc.SelectSingleNode("gestion/eventos");

            node.InsertAfter(evento, node.LastChild);

            doc.Save(ruta);
        }
        public XmlNode agregarEvento2(string codigo, string cedula, string lugar, string placa, string estado, string fecha, string multa)
        {

            XmlNode evento = doc.CreateElement("evento");

            XmlElement xcodigo = doc.CreateElement("codigo");
            xcodigo.InnerText = codigo;
            evento.AppendChild(xcodigo);

            XmlElement xcedula = doc.CreateElement("cedula");
            xcedula.InnerText = cedula;
            evento.AppendChild(xcedula);

            XmlElement xlugar = doc.CreateElement("lugar");
            xlugar.InnerText = lugar;
            evento.AppendChild(xlugar);

            XmlElement xplaca = doc.CreateElement("placa");
            xplaca.InnerText = placa;
            evento.AppendChild(xplaca);


            XmlElement xcedulaOfi = doc.CreateElement("cedulaoficial");
            xcedulaOfi.InnerText = "NO ASIGNADO";
            evento.AppendChild(xcedulaOfi);

            XmlElement xnumeroparte = doc.CreateElement("numeroparte");
            xnumeroparte.InnerText = "NO ASIGNADO";
            evento.AppendChild(xnumeroparte);

            XmlElement xcedulaOfijuzgado = doc.CreateElement("cedulaofijuzgado");
            xcedulaOfijuzgado.InnerText = "NO ASIGNADO";
            evento.AppendChild(xcedulaOfijuzgado);

            XmlElement xnumeroregistro = doc.CreateElement("numeroregistro");
            xnumeroregistro.InnerText = "NO ASIGNADO";
            evento.AppendChild(xnumeroregistro);

            XmlElement xestado = doc.CreateElement("estado");
            xestado.InnerText = estado;
            evento.AppendChild(xestado);

            XmlElement xfecha = doc.CreateElement("fecha");
            xfecha.InnerText = fecha;
            evento.AppendChild(xfecha);

            XmlElement xmulta = doc.CreateElement("multa");
            xmulta.InnerText = multa;
            evento.AppendChild(xmulta);




            return evento;
        }
        public void eliminarEvento(string codigo)
        {
            doc.Load(ruta);

            XmlNodeList listaEventos = doc.SelectNodes("gestion/eventos/evento");

            XmlNode raiz = doc.SelectSingleNode("gestion/eventos");

            foreach (XmlNode item in listaEventos)
            {
                if (item.SelectSingleNode("codigo").InnerText == codigo)
                {
                    XmlNode hijo = item;
                    raiz.RemoveChild(hijo);
                }
            }

            doc.Save(ruta);
        }


        public void modificarEventos(int operacion ,string codigo,string cedula,string numero,string fecha)
        {
            doc.Load(ruta);

            XmlNodeList listaEventos = doc.SelectNodes("gestion/eventos/evento");

            foreach (XmlNode item in listaEventos)
            {
                if (item.FirstChild.InnerText == codigo && operacion==1)
                {
                    item.SelectSingleNode("cedulaoficial").InnerText = cedula;
                    item.SelectSingleNode("numeroparte").InnerText = numero;          
                    item.SelectSingleNode("fecha").InnerText = fecha;
                    item.SelectSingleNode("estado").InnerText = "Por aprobar";
                }

                if (item.FirstChild.InnerText == codigo && operacion == 2)
                {
                    item.SelectSingleNode("cedulaofijuzgado").InnerText = cedula;
                    item.SelectSingleNode("numeroregistro").InnerText = numero;
                    item.SelectSingleNode("fecha").InnerText = fecha;
                    item.SelectSingleNode("estado").InnerText = "Completo";
                }
            }
            doc.Save(ruta);
        }

        public void cargarparteOregistro(int operacion, List<string> lista)
        {

            doc.Load(ruta);

            XmlNodeList eventos = doc.SelectNodes("gestion/eventos/evento");

            XmlNode unEvento;

            for (int i = 0; i < eventos.Count; i++)
            {
                unEvento = eventos.Item(i);


                if (operacion == 1)
                {

                    string parte = unEvento.SelectSingleNode("numeroparte").InnerText;

                    lista.Add(parte);
                }

                if (operacion == 2)
                {

                    string registro = unEvento.SelectSingleNode("numeroregistro").InnerText;

                    lista.Add(registro);
                }
            }
        }
            public void cargarReportes(int operacion, DataGridView dgv)
            {

                DataTable datatable = (DataTable)dgv.DataSource;


                doc.Load(ruta);

                XmlNodeList eventos = doc.SelectNodes("gestion/eventos/evento");

                XmlNode unEvento;

                for (int i = 0; i < eventos.Count; i++)
                {
                    unEvento = eventos.Item(i);

                    string estado = unEvento.SelectSingleNode("estado").InnerText;


                    if (estado == "Por aprobar" && operacion == 1)
                    {


                        string codigo = unEvento.SelectSingleNode("codigo").InnerText;
                        string cedula = unEvento.SelectSingleNode("cedulaoficial").InnerText;
                        int multa = int.Parse(unEvento.SelectSingleNode("multa").InnerText);
                    

                        DataRow row = datatable.NewRow();
                        row["Codigo"] = codigo;
                        row["Cedula del oficial"] = cedula;
                        row["Multa"] = multa;
                        datatable.Rows.Add(row);
                        dgv.DataSource = datatable;

                    }
                    if (estado == "Completo" && operacion == 2)
                    {

                        string cedula = unEvento.SelectSingleNode("cedula").InnerText;
                        string numeroparte = unEvento.SelectSingleNode("numeroparte").InnerText;
                        string lugar = unEvento.SelectSingleNode("lugar").InnerText;
                        string fecha = unEvento.SelectSingleNode("fecha").InnerText;
                        string placaE = unEvento.SelectSingleNode("placa").InnerText;
                        string marca;
                        string color;
                        string tipo;

                    XmlNodeList vehivulos = doc.SelectNodes("gestion/vehiculos/vehiculo");

                    XmlNode unVehiculo;

                    for ( int j = 0; j < vehivulos.Count; j++)
                    {
                        unVehiculo = vehivulos.Item(j);

                        string placaV = unVehiculo.SelectSingleNode("placa").InnerText;

                        if (placaE == placaV)
                        {
                            marca = unVehiculo.SelectSingleNode("marca").InnerText;
                            color = unVehiculo.SelectSingleNode("color").InnerText;
                            tipo = unVehiculo.SelectSingleNode("tipo").InnerText;
                            DataRow row = datatable.NewRow();
                            row["Cedula ciudadano"] = cedula;
                            row["numero parte"] = numeroparte;
                            row["Lugar"] = lugar;
                            row["Fecha"] = fecha;
                            row["Marca"] = marca;
                            row["Color"] = color;
                            row["Tipo Vehiculo"] = tipo;
                            datatable.Rows.Add(row);
                            dgv.DataSource = datatable;
                        }                

                    }                    

                    
                }

                
           }

            
        }

        
    }
    
}
