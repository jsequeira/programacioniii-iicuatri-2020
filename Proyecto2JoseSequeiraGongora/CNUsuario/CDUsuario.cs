﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using Entidades;

namespace CapaDatos
{
    public class CDUsuario
    {


        XmlDocument doc = new XmlDocument();

        string ruta = Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent. FullName, "gestion.xml");


        public Usuario loginUsuario(Usuario usuario,string iniUsuario,string iniContrasenna)
        {
            usuario = new Usuario();

            doc.Load(ruta);

            XmlNodeList usuarios = doc.SelectNodes("gestion/usuarios/usuario");

            XmlNode unUsuario;

            for (int i = 0; i < usuarios.Count; i++)
            {


                unUsuario = usuarios.Item(i);


                string cedula = unUsuario.SelectSingleNode("cedula").InnerText;

                string contrasenna = unUsuario.SelectSingleNode("contrasenna").InnerText;


                if (cedula==iniUsuario && contrasenna==iniContrasenna) {
                usuario.Id = unUsuario.Attributes["id"].Value.ToString();          
                usuario.Nombre= unUsuario.SelectSingleNode("nombre").InnerText;
                usuario.Cedula= unUsuario.SelectSingleNode("cedula").InnerText;
                usuario.Contrasenna = unUsuario.SelectSingleNode("contrasenna").InnerText;
                usuario.Tipo = unUsuario.SelectSingleNode("tipo").InnerText;
                
             

                }


            }

            return usuario;
        }




    }
}



        
