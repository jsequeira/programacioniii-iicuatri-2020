﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Windows.Forms;
using CapaDatos;
namespace CapaNegocio
{
    public class CNEventos
    {

        public void cargarCodigos(string usuCedula, DataGridView dgv, List<string> codigos, List<string> placas)
        {

            new CDEventos().cargarCodigos(usuCedula, dgv, codigos, placas);

        }
        public void agregarDatos(string codigo, string cedula, string lugar, string placa, string estado, string fecha, string multa)
        {
            new CDEventos().agregarEvento(codigo, cedula, lugar, placa, estado, fecha, multa);

        }


        public void cargarSoloeventos(int operacion, DataGridView dgv)
        {

            new CDEventos().cargarSoloeventos(operacion, dgv);

        }


        public void borrar(string codigo)
        {

            new CDEventos().eliminarEvento(codigo);

        }


        public void cargarparteOregistro(int operacion, List<string> lista)
        {
            new CDEventos().cargarparteOregistro(operacion,lista);
        }
        public void modificarEvento(int operacion, string codigo, string cedula, string numero, string fecha)
        {
            new CDEventos().modificarEventos(operacion,codigo,cedula,numero,fecha);
        

        }

        public void cargarReportes(int operacion, DataGridView dgv)
        {

            new CDEventos().cargarReportes(operacion,dgv);

        }


        }

}
