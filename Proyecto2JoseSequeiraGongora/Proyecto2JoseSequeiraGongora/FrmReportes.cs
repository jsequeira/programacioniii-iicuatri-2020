﻿using CapaNegocio;
using Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto2JoseSequeiraGongora
{
    public partial class FrmReportes : Form
    {
        Usuario usuario;
        public FrmReportes(Usuario usuario)
        {
            InitializeComponent();
            CenterToScreen();
            DefinirDgv();
            new CNEventos().cargarReportes(1,dgvReporte1);
            new CNEventos().cargarReportes(2, dgvReporte2);
            this.usuario = usuario;
        }

        public void DefinirDgv() {
            DataColumn multa = new DataColumn("Multa");
            multa.DataType = System.Type.GetType("System.Int32");
            DataTable dt = new DataTable();
            dt.Columns.Add("Codigo");
            dt.Columns.Add("Cedula del oficial");
            dt.Columns.Add(multa);
            dgvReporte1.DataSource = dt;

            DataTable dt1 = new DataTable();
            dt1.Columns.Add("Cedula ciudadano");
            dt1.Columns.Add("numero parte");
            dt1.Columns.Add("Lugar");
            dt1.Columns.Add("Fecha");
            dt1.Columns.Add("Marca");
            dt1.Columns.Add("Color");
            dt1.Columns.Add("Tipo Vehiculo");
            dgvReporte2.DataSource = dt1;



        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            FrmEventoOfiJuzgado frm = new FrmEventoOfiJuzgado(this.usuario);
            frm.Visible = true;
            this.Hide();
        }
    }
}
