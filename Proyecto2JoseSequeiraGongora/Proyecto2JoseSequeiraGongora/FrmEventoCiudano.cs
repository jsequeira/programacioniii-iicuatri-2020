﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades;
using CapaNegocio;
using System.Xml;

namespace Proyecto2JoseSequeiraGongora
{
    public partial class FrmEventoCiudano : Form
    {

        string codigo;

       
        Usuario usuario;
        List<string> listaCodigos;
        List<string> listaPlacas;
        Dictionary<string, string> placaTipo;
        Dictionary<string, string> placaAnnio;

        public FrmEventoCiudano(Usuario usuario)
        {

            InitializeComponent();
            CenterToScreen();
            definirDgv();
            this.usuario = usuario;
            cargarDatos();
            listaCodigos = new List<string>();
            listaPlacas = new List<string>();
            placaAnnio = new Dictionary<string, string>();
            placaTipo = new Dictionary<string, string>();

            new CNVehiculo().cargarPlacas(usuario.Cedula, cbPlacas, this.placaTipo, this.placaAnnio);
            new CNEventos().cargarCodigos(usuario.Cedula, dgvEventos, listaCodigos, listaPlacas);

            establecerFilas();

            cbPlacas.SelectedIndex = 0;
            cbLugar.SelectedIndex = 0; ;




        }



        public void establecerFilas() {

            dgvEventos.DefaultCellStyle.SelectionBackColor = Color.White;
            dgvEventos.DefaultCellStyle.SelectionForeColor = Color.Black;

        }


        public void definirDgv() {

            DataTable dt = new DataTable();
            dt.Columns.Add("Codigo");        
            dt.Columns.Add("Placa");
            dt.Columns.Add("Lugar");
            dt.Columns.Add("Fecha");
            dt.Columns.Add("Multa");
            dgvEventos.DataSource = dt;

        }




        public void cargarDatos() {

            lblNombre.Text = this.usuario.Nombre;
            lblCedula.Text = this.usuario.Cedula;
            
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
         
            double multa = 0;
            string hoy = DateTime.Now.ToLocalTime().ToString();



            if (listaPlacas.Contains(cbPlacas.SelectedItem.ToString()))
            {
                MessageBox.Show("La placa numero: " + cbPlacas.SelectedItem.ToString() + " Se encuentra registrada");
            }
            else
            {

                int codigo = new Random().Next(0, 10000);

                while (listaCodigos.Contains(codigo.ToString()))
                {

                    codigo = new Random().Next(0, 10000);

                    MessageBox.Show("Número Repetido" + codigo);


                }

                string lugar = cbLugar.SelectedItem.ToString();
                string placa = cbPlacas.SelectedItem.ToString();

           

                string modelo = placaTipo[cbPlacas.SelectedItem.ToString()];

                    int annio =int.Parse( placaAnnio[cbPlacas.Text]);

                if (modelo=="Motocicleta") 
                {                  
                    multa = 10000 + (10000 * 0.15);

                    if (annio < 2000) { 

                        multa = 10000 + (10000*0.10);
                    }
                }
                if (modelo == "Automóvil")
                {
                    multa = 25000 + (25000 * 0.30);

                    if (annio < 2000)
                    {

                        multa = 25000 + (25000 * 0.10);
                    }
                }
                if (modelo == "Bus")
                {
                    multa = 45000 + (45000 * 0.45);

                    if (annio < 2000)
                    {

                        multa = 45000 + (45000* 0.10);
                    }
                }
                if (modelo == "Camión")
                {
                    multa = 65000 + (65000 * 0.70);

                    if (annio < 2000)
                    {

                        multa = 65000 + (65000 * 0.10);
                    
                    }
                }

                new CNEventos().agregarDatos(codigo.ToString(), usuario.Cedula, lugar, placa, "Abierto", hoy.ToString(), multa.ToString());
                
                listaCodigos.Clear();
                listaPlacas.Clear();
                dgvEventos.Columns.Clear();
                definirDgv();
                new CNEventos().cargarCodigos(usuario.Cedula, dgvEventos, listaCodigos, listaPlacas);
                establecerFilas();
            }

            


            }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            FrmMain frm = new FrmMain();
            frm.Visible=true;
            this.Hide();
        }

      
 

     
        private void btnBorrar_Click_1(object sender, EventArgs e) {

            new CNEventos().borrar(this.codigo);
            MessageBox.Show("Se han borrado los datos corréctamente");
            dgvEventos.Columns.Clear();
            listaCodigos.Clear();
            listaPlacas.Clear();
            definirDgv();
            new CNEventos().cargarCodigos(usuario.Cedula, dgvEventos, listaCodigos, listaPlacas);
            establecerFilas();
        }

        private void dgvEventos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                dgvEventos.DefaultCellStyle.SelectionBackColor = Color.BlueViolet;
                dgvEventos.DefaultCellStyle.SelectionForeColor = Color.White;
                this.codigo = dgvEventos.Rows[e.RowIndex].Cells[0].Value.ToString();

            }
        }
    }
    }

