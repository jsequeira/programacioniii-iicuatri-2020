﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaNegocio;
using Entidades;
namespace Proyecto2JoseSequeiraGongora
{
    public partial class FrmEventoOfiTransito : Form
    {
        Usuario usuario;
        string codigo;
        List<string> listaPartes;
      
        public FrmEventoOfiTransito(Usuario usuario)
        {
            InitializeComponent();
            this.usuario = usuario;
            CenterToScreen();
            definirDgv();
            cargarEventos();
            lblNombre.Text = usuario.Nombre;
            lblCedula.Text = usuario.Cedula;
            listaPartes = new List<string>();
            establecerFilas();

        }

        public void definirDgv()
        {

            DataTable dt = new DataTable();
            dt.Columns.Add("Codigo");
            dt.Columns.Add("Placa");
            dt.Columns.Add("Lugar");
            dt.Columns.Add("Fecha");
            dt.Columns.Add("Multa");
            dgvEventos.DataSource = dt;

        }
        public void establecerFilas()
        {

            dgvEventos.DefaultCellStyle.SelectionBackColor = Color.White;
            dgvEventos.DefaultCellStyle.SelectionForeColor = Color.Black;

        }
        public void cargarEventos() {

            new CNEventos().cargarSoloeventos(1,dgvEventos);

}

        private void dgvEventos_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
              

                dgvEventos.DefaultCellStyle.SelectionBackColor = Color.BlueViolet;
                dgvEventos.DefaultCellStyle.SelectionForeColor = Color.White;
                this.codigo = dgvEventos.Rows[e.RowIndex].Cells[0].Value.ToString();

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FrmMain frm = new FrmMain();
            frm.Visible=true;
            this.Hide();

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void btnTramitar_Click(object sender, EventArgs e)
        {
            new CNEventos().cargarparteOregistro(1, listaPartes);
            string hoy = DateTime.Now.ToLocalTime().ToString();
            int numeroreporte = new Random().Next(0, 10000);

            while (listaPartes.Contains(codigo.ToString()))
            {

                numeroreporte = new Random().Next(0, 10000);

                MessageBox.Show("Número Repetido" + codigo);
            }

            new CNEventos().modificarEvento(1, this.codigo, this.usuario.Cedula, numeroreporte.ToString(), hoy);
            dgvEventos.Columns.Clear();
            listaPartes.Clear();
            definirDgv();
            cargarEventos();
            establecerFilas();
        }
    }
}
