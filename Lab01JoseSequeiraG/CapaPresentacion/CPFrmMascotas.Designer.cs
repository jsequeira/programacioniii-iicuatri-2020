﻿namespace CapaPresentacion
{
    partial class CPFrmMascotas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvInicio = new System.Windows.Forms.DataGridView();
            this.pImagen = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCerrar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnImg4 = new System.Windows.Forms.Button();
            this.btnImg3 = new System.Windows.Forms.Button();
            this.btnImg2 = new System.Windows.Forms.Button();
            this.btnImg1 = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.txtCedula = new System.Windows.Forms.TextBox();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.btnRegistar = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.Inicio = new System.Windows.Forms.TabPage();
            this.Reportes = new System.Windows.Forms.TabPage();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.dgvReporte2 = new System.Windows.Forms.DataGridView();
            this.dgvReportes = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInicio)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.Inicio.SuspendLayout();
            this.Reportes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReporte2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReportes)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvInicio
            // 
            this.dgvInicio.AllowUserToResizeColumns = false;
            this.dgvInicio.AllowUserToResizeRows = false;
            this.dgvInicio.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvInicio.Location = new System.Drawing.Point(2, 3);
            this.dgvInicio.Name = "dgvInicio";
            this.dgvInicio.RowHeadersWidth = 51;
            this.dgvInicio.RowTemplate.Height = 24;
            this.dgvInicio.Size = new System.Drawing.Size(1418, 473);
            this.dgvInicio.TabIndex = 0;
            this.dgvInicio.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvInicio_CellMouseClick);
            this.dgvInicio.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvInicio_DataError);
            // 
            // pImagen
            // 
            this.pImagen.Location = new System.Drawing.Point(691, 97);
            this.pImagen.Name = "pImagen";
            this.pImagen.Size = new System.Drawing.Size(293, 302);
            this.pImagen.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btnCerrar);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btnImg4);
            this.panel1.Controls.Add(this.btnImg3);
            this.panel1.Controls.Add(this.btnImg2);
            this.panel1.Controls.Add(this.btnImg1);
            this.panel1.Location = new System.Drawing.Point(986, 97);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(280, 302);
            this.panel1.TabIndex = 2;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // btnCerrar
            // 
            this.btnCerrar.Location = new System.Drawing.Point(50, 267);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(173, 23);
            this.btnCerrar.TabIndex = 5;
            this.btnCerrar.Text = "Guardar";
            this.btnCerrar.UseVisualStyleBackColor = true;
            this.btnCerrar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Location = new System.Drawing.Point(69, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(130, 19);
            this.label1.TabIndex = 4;
            this.label1.Text = "Escoja una imagen";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // btnImg4
            // 
            this.btnImg4.Location = new System.Drawing.Point(133, 149);
            this.btnImg4.Name = "btnImg4";
            this.btnImg4.Size = new System.Drawing.Size(126, 112);
            this.btnImg4.TabIndex = 3;
            this.btnImg4.UseVisualStyleBackColor = true;
            this.btnImg4.Click += new System.EventHandler(this.btnImg4_Click);
            // 
            // btnImg3
            // 
            this.btnImg3.Location = new System.Drawing.Point(3, 149);
            this.btnImg3.Name = "btnImg3";
            this.btnImg3.Size = new System.Drawing.Size(127, 112);
            this.btnImg3.TabIndex = 2;
            this.btnImg3.UseVisualStyleBackColor = true;
            this.btnImg3.Click += new System.EventHandler(this.btnImg3_Click);
            // 
            // btnImg2
            // 
            this.btnImg2.Location = new System.Drawing.Point(133, 31);
            this.btnImg2.Name = "btnImg2";
            this.btnImg2.Size = new System.Drawing.Size(126, 109);
            this.btnImg2.TabIndex = 1;
            this.btnImg2.UseVisualStyleBackColor = true;
            this.btnImg2.Click += new System.EventHandler(this.btnImg2_Click);
            // 
            // btnImg1
            // 
            this.btnImg1.Location = new System.Drawing.Point(3, 31);
            this.btnImg1.Name = "btnImg1";
            this.btnImg1.Size = new System.Drawing.Size(127, 109);
            this.btnImg1.TabIndex = 0;
            this.btnImg1.UseVisualStyleBackColor = true;
            this.btnImg1.Click += new System.EventHandler(this.btnImg1_Click);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.btnCancelar);
            this.panel2.Controls.Add(this.txtCedula);
            this.panel2.Controls.Add(this.txtNombre);
            this.panel2.Controls.Add(this.btnRegistar);
            this.panel2.Location = new System.Drawing.Point(207, 97);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(339, 302);
            this.panel2.TabIndex = 3;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 57);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(109, 17);
            this.label5.TabIndex = 8;
            this.label5.Text = "Datos Personas";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label4.Location = new System.Drawing.Point(88, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(160, 19);
            this.label4.TabIndex = 7;
            this.label4.Text = "Registro de adopciones";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.Location = new System.Drawing.Point(22, 136);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 19);
            this.label3.TabIndex = 6;
            this.label3.Text = "Cedula:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label2.Location = new System.Drawing.Point(16, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 19);
            this.label2.TabIndex = 5;
            this.label2.Text = "Nombre:";
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(195, 268);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(104, 23);
            this.btnCancelar.TabIndex = 4;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // txtCedula
            // 
            this.txtCedula.Location = new System.Drawing.Point(91, 133);
            this.txtCedula.Name = "txtCedula";
            this.txtCedula.Size = new System.Drawing.Size(208, 22);
            this.txtCedula.TabIndex = 3;
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(91, 86);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(208, 22);
            this.txtNombre.TabIndex = 2;
            // 
            // btnRegistar
            // 
            this.btnRegistar.Location = new System.Drawing.Point(37, 268);
            this.btnRegistar.Name = "btnRegistar";
            this.btnRegistar.Size = new System.Drawing.Size(104, 23);
            this.btnRegistar.TabIndex = 0;
            this.btnRegistar.Text = "Registrar";
            this.btnRegistar.UseVisualStyleBackColor = true;
            this.btnRegistar.Click += new System.EventHandler(this.btnRegistar_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.Inicio);
            this.tabControl1.Controls.Add(this.Reportes);
            this.tabControl1.Location = new System.Drawing.Point(12, 61);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1431, 511);
            this.tabControl1.TabIndex = 4;
            // 
            // Inicio
            // 
            this.Inicio.Controls.Add(this.panel1);
            this.Inicio.Controls.Add(this.pImagen);
            this.Inicio.Controls.Add(this.panel2);
            this.Inicio.Controls.Add(this.dgvInicio);
            this.Inicio.Location = new System.Drawing.Point(4, 25);
            this.Inicio.Name = "Inicio";
            this.Inicio.Padding = new System.Windows.Forms.Padding(3);
            this.Inicio.Size = new System.Drawing.Size(1423, 482);
            this.Inicio.TabIndex = 0;
            this.Inicio.Text = "Inicio";
            this.Inicio.UseVisualStyleBackColor = true;
            // 
            // Reportes
            // 
            this.Reportes.Controls.Add(this.label7);
            this.Reportes.Controls.Add(this.label6);
            this.Reportes.Controls.Add(this.dgvReporte2);
            this.Reportes.Controls.Add(this.dgvReportes);
            this.Reportes.Location = new System.Drawing.Point(4, 25);
            this.Reportes.Name = "Reportes";
            this.Reportes.Padding = new System.Windows.Forms.Padding(3);
            this.Reportes.Size = new System.Drawing.Size(1423, 482);
            this.Reportes.TabIndex = 1;
            this.Reportes.Text = "Reportes";
            this.Reportes.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 266);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(82, 17);
            this.label7.TabIndex = 3;
            this.label7.Text = "Adopciones";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 15);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 17);
            this.label6.TabIndex = 2;
            this.label6.Text = "Dueños";
            // 
            // dgvReporte2
            // 
            this.dgvReporte2.AllowUserToResizeColumns = false;
            this.dgvReporte2.AllowUserToResizeRows = false;
            this.dgvReporte2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvReporte2.Location = new System.Drawing.Point(3, 286);
            this.dgvReporte2.Name = "dgvReporte2";
            this.dgvReporte2.ReadOnly = true;
            this.dgvReporte2.RowHeadersWidth = 51;
            this.dgvReporte2.RowTemplate.Height = 24;
            this.dgvReporte2.Size = new System.Drawing.Size(737, 173);
            this.dgvReporte2.TabIndex = 1;
            this.dgvReporte2.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvReporte2_CellContentClick);
            // 
            // dgvReportes
            // 
            this.dgvReportes.AllowUserToResizeColumns = false;
            this.dgvReportes.AllowUserToResizeRows = false;
            this.dgvReportes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvReportes.Location = new System.Drawing.Point(3, 38);
            this.dgvReportes.Name = "dgvReportes";
            this.dgvReportes.ReadOnly = true;
            this.dgvReportes.RowHeadersWidth = 51;
            this.dgvReportes.RowTemplate.Height = 24;
            this.dgvReportes.Size = new System.Drawing.Size(604, 198);
            this.dgvReportes.TabIndex = 0;
            this.dgvReportes.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvReportes_CellContentClick);
            // 
            // CPFrmMascotas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1449, 584);
            this.Controls.Add(this.tabControl1);
            this.Name = "CPFrmMascotas";
            this.Text = "CPFrmMascotas";
            this.Load += new System.EventHandler(this.CPFrmMascotas_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvInicio)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.Inicio.ResumeLayout(false);
            this.Reportes.ResumeLayout(false);
            this.Reportes.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReporte2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReportes)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvInicio;
        private System.Windows.Forms.Panel pImagen;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnImg4;
        private System.Windows.Forms.Button btnImg3;
        private System.Windows.Forms.Button btnImg2;
        private System.Windows.Forms.Button btnImg1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnCerrar;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.TextBox txtCedula;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Button btnRegistar;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage Inicio;
        private System.Windows.Forms.TabPage Reportes;
        private System.Windows.Forms.DataGridView dgvReportes;
        private System.Windows.Forms.DataGridView dgvReporte2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
    }
}