﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaNegocio;

namespace CapaPresentacion
{
    public partial class CPFrmMascotas : Form
    {

        DateTimePicker dtp;
        DateTimePicker dtpModificar;

        DateTime fechaAdopcion;

        int idMascota=0;
        
        int row = 0;
        int cell = 0;
        List<string> listaImagenes = new List<string>();




        public CPFrmMascotas()
        {
            listaImagenes.Add(@"Img\empty.jpg");



            InitializeComponent();

            panel1.Visible = false;
            panel2.Visible = false;
            pImagen.Visible = false;

            this.CenterToScreen();


        }


        public void valorImagenes()
        {
            string startupPath1 = Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent.FullName, @"Img\perrobt.jpg");
            string startupPath2 = Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent.FullName, @"Img\perropg.jpg");
            string startupPath3 = Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent.FullName, @"Img\perroptb.jpg");
            string startupPath4 = Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent.FullName, @"Img\perrortw.jpg");

            Image image1 = Image.FromFile(startupPath1);
            Image image2 = Image.FromFile(startupPath2);
            Image image3 = Image.FromFile(startupPath3);
            Image image4 = Image.FromFile(startupPath4);


            btnImg1.BackgroundImage = image1;
            btnImg2.BackgroundImage = image2;
            btnImg3.BackgroundImage = image3;
            btnImg4.BackgroundImage = image4;



            btnImg1.BackgroundImageLayout = ImageLayout.Stretch;
            btnImg2.BackgroundImageLayout = ImageLayout.Stretch;
            btnImg3.BackgroundImageLayout = ImageLayout.Stretch;
            btnImg4.BackgroundImageLayout = ImageLayout.Stretch;

            dgvInicio.Enabled = false;







        }


        public void propiedadesInsert()
        {
            

            DataGridViewButtonCell bcFecha = new DataGridViewButtonCell();
            bcFecha.ToolTipText = "click para Abrir";

            DataGridViewButtonCell bcInsertar = new DataGridViewButtonCell();
            bcInsertar.ToolTipText = "Insertar una mascota";


            DataGridViewButtonCell bcLimpiar = new DataGridViewButtonCell();
            bcLimpiar.ToolTipText = "Limpiar celdas";

            DataGridViewTextBoxCell tbcAdopcion = new DataGridViewTextBoxCell();

            DataGridViewComboBoxCell cbcColor = new DataGridViewComboBoxCell();
            cbcColor.Items.AddRange(new string[] { "Blanco", "Negro", "Gris", "Café" });

            DataGridViewComboBoxCell cbcTamanno = new DataGridViewComboBoxCell();
            cbcTamanno.Items.AddRange(new string[] { "Grande", "Mediano", "Pequeño" });

            DataGridViewCheckBoxCell cbcSexo = new DataGridViewCheckBoxCell();
            cbcSexo.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;


            dtp = new DateTimePicker();
            dtp.Visible = false;
            dgvInicio[6, 0].Value = "21-04-1996";

            dgvInicio.Controls.Add(dtp);

            dtp.Format = DateTimePickerFormat.Custom;
            dtp.CustomFormat = "dd-MM-yyyy'";

            dtp.Value = DateTime.Parse(dgvInicio.Rows[0].Cells[6].Value.ToString());

            dtp.Location = dgvInicio.GetCellDisplayRectangle(6, 0, true).Location;

            dtp.Size = dgvInicio.GetCellDisplayRectangle(6, 0, true).Size;


            string startupPath5 = Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent.FullName, listaImagenes[0]);

            Image image5 = Image.FromFile(startupPath5);

            dgvInicio[9, 0].Value = image5;





            dgvInicio[8, 0] = bcLimpiar;
            dgvInicio[8, 0].Value = "Limpiar";
            dgvInicio[6, 0] = bcFecha;
            dgvInicio[7, 0] = bcInsertar;
            dgvInicio[7, 0].Value = "Insertar";
            dgvInicio[0, 0] = tbcAdopcion;
            dgvInicio[0, 0].ReadOnly = true;
            dgvInicio[0, 0].Value = "Ninguna";
            dgvInicio[1, 0].ReadOnly = true;
            dgvInicio[1, 0].Value = "Automatico";
            dgvInicio[2, 0] = cbcColor;
            dgvInicio[3, 0] = cbcTamanno;
            dgvInicio[4, 0] = cbcSexo;
            dgvInicio[4, 0].Value = false;
        }
        public void propiedadesDgv()
        {

            dgvInicio.EditMode = DataGridViewEditMode.EditOnEnter;

            dgvInicio.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;
            dgvInicio.Columns[1].SortMode = DataGridViewColumnSortMode.NotSortable;
            dgvInicio.Columns[2].SortMode = DataGridViewColumnSortMode.NotSortable;
            dgvInicio.Columns[3].SortMode = DataGridViewColumnSortMode.NotSortable;
            dgvInicio.Columns[4].SortMode = DataGridViewColumnSortMode.NotSortable;
            dgvInicio.Columns[5].SortMode = DataGridViewColumnSortMode.NotSortable;
            dgvInicio.Columns[6].SortMode = DataGridViewColumnSortMode.NotSortable;
            dgvInicio.Columns[7].SortMode = DataGridViewColumnSortMode.NotSortable;
            dgvInicio.Columns[8].SortMode = DataGridViewColumnSortMode.NotSortable;
            dgvInicio.Columns[9].SortMode = DataGridViewColumnSortMode.NotSortable;
            //dgvInicio.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            //dgvInicio.RowTemplate.Resizable = DataGridViewTriState.True;
            //dgvInicio.RowTemplate.Height = 90;



        }

        private void CPFrmMascotas_Load(object sender, EventArgs e)
        {

            new CNDuenno().reporteDuenno (dgvReportes);
           
            new CNMascotas().mostrarMascotas(dgvInicio,listaImagenes);
            propiedadesInsert();
            propiedadesDgv();
        }

        private void dgvInicio_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {

                if (dgvInicio.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.Equals("Insertar"))
                {

                    //MessageBox.Show(Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent.FullName, @"Img\" + "descarga.jpg"));




                    // OpenFileDialog dlg = new OpenFileDialog();
                    //DialogResult dr = dlg.ShowDialog();
                    //  if (dr == System.Windows.Forms.DialogResult.OK)
                    //{
                    //  string filename = dlg.FileName;
                    //((DataGridViewImageCell)dataGridView1.Rows[e.RowIndex].Cells[1]).Value = Image.FromFile(filename);
                    //}

                    //  }

                    if (dgvInicio[2, e.RowIndex].Value.ToString() != "" && dgvInicio[3, e.RowIndex].Value.ToString() != "")
                    {



                        string color = dgvInicio[2, e.RowIndex].Value.ToString();

                        string tamanno = dgvInicio[3, e.RowIndex].Value.ToString();

                        string sexo;
                        if (bool.Parse(dgvInicio[4, e.RowIndex].Value.ToString()))
                        {
                            sexo = "Macho";
                        }
                        else
                        {
                            sexo = "Hembra";
                        }

                        int edad = int.Parse(dgvInicio[5, e.RowIndex].Value.ToString());

                        DateTime fecha = DateTime.Parse(dgvInicio.Rows[e.RowIndex].Cells[6].Value.ToString());

                        string foto = listaImagenes[e.RowIndex];

                        new CNMascotas().insertarMascotas(color, tamanno, sexo, edad, fecha, foto);


                        dgvInicio.DataSource = null;

                        dgvInicio.Columns.Clear();

                        listaImagenes.Clear();

                        listaImagenes.Add(@"Img\empty.jpg");

                        new CNMascotas().mostrarMascotas(dgvInicio, listaImagenes);

                        propiedadesInsert();

                        propiedadesDgv();





                    }
                    else
                    {
                        MessageBox.Show("Rellene todos los espacios");
                    }


                }
                else if (dgvInicio.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.Equals("Limpiar"))
                {

                    dgvInicio[2, 0].Value = "";

                    dgvInicio[3, 0].Value = "";
                    dgvInicio[4, 0].Value = false;
                    dgvInicio[5, 0].Value = 0;
                    dgvInicio[6, 0].Value = "21-04-1996";
                    string startupPath5 = Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent.FullName, listaImagenes[0]);

                    Image image5 = Image.FromFile(startupPath5);

                    dgvInicio[9, 0].Value = image5;



                }
                else if (dgvInicio.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.Equals("Adoptar"))

                {
                    idMascota = int.Parse(dgvInicio.Rows[e.RowIndex].Cells[1].Value.ToString());

                    fechaAdopcion = DateTime.Parse(dgvInicio.Rows[e.RowIndex].Cells[6].Value.ToString());


                    panel2.Visible = true;

                    dgvInicio.Enabled = false;


                }







                else if (dgvInicio.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.Equals("Modificar"))
                {

                    int id = int.Parse(dgvInicio[1, e.RowIndex].Value.ToString());

                    string color = dgvInicio[2, e.RowIndex].Value.ToString();

                    string tamanno = dgvInicio[3, e.RowIndex].Value.ToString();

                    string sexo;
                    if (bool.Parse(dgvInicio[4, e.RowIndex].Value.ToString()))
                    {
                        sexo = "Macho";
                    }
                    else
                    {
                        sexo = "Hembra";
                    }

                    int edad = int.Parse(dgvInicio[5, e.RowIndex].Value.ToString());

                    DateTime fecha = DateTime.Parse(dgvInicio.Rows[e.RowIndex].Cells[6].Value.ToString());

                    string foto = listaImagenes[e.RowIndex];

                    new CNMascotas().modificarMascotas(id, color, tamanno, sexo, edad, fecha, foto);


                }

                else if (dgvInicio.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.Equals("Eliminar"))
                {

                    int id = int.Parse(dgvInicio.Rows[e.RowIndex].Cells[1].Value.ToString());
                    new CNMascotas().eliminarMascotas(id);

                    dgvInicio.DataSource = null;

                    dgvInicio.Columns.Clear();

                    listaImagenes.Clear();

                    listaImagenes.Add(@"Img\empty.jpg");

                    new CNMascotas().mostrarMascotas(dgvInicio, listaImagenes);

                    propiedadesInsert();

                    propiedadesDgv();
                }



                else if (e.ColumnIndex.Equals(6))
                {
                    if (dtpModificar != null)
                    {
                        this.dtpModificar.Visible = false;
                    }
                    dtpModificar = new DateTimePicker();

                    dgvInicio.Controls.Add(dtpModificar);

                    dtpModificar.Format = DateTimePickerFormat.Custom;
                    dtpModificar.CustomFormat = "dd-MM-yyyy'";

                    dtpModificar.Value = DateTime.Parse(dgvInicio.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());

                    dtpModificar.Location = dgvInicio.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, true).Location;

                    dtpModificar.Size = dgvInicio.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, true).Size;

                    dtpModificar.TextChanged += new EventHandler(dateTimePicker1_OnTextChange);

                    dtpModificar.CloseUp += new EventHandler(dateTimePicker1_CloseUp);

                }

                else
                {
                    Console.WriteLine("HolaMundo");
                }






                if (!e.ColumnIndex.Equals(6))
                {

                    if (dtpModificar != null)

                    {
                        this.dtpModificar.Visible = false;

                    }

                }







                if (e.ColumnIndex.Equals(9))
                {

                    valorImagenes();


                    panel1.Visible = true;



                    this.row = e.RowIndex;


                    this.cell = e.ColumnIndex;

                }
            }
            catch
            {
                MessageBox.Show("Este area no cumple ninguna función");


            }
            {

                
            }
            
        }



        private void dateTimePicker1_OnTextChange(object sender, EventArgs e)
        {

            dgvInicio.CurrentCell.Value = dtpModificar.Text.ToString();
        }
        void dateTimePicker1_CloseUp(object sender, EventArgs e)
        {

            dtpModificar.Visible = false;

        }

        private void dgvInicio_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {



            MessageBox.Show("La columna edad solo acepta digitos enteros!");







        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }



        private void btnGuardar_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
            dgvInicio.Enabled = true;
            pImagen.Visible = false;
        }

        private void btnImg1_Click(object sender, EventArgs e)
        {
            string startupPath1 = Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent.FullName, @"Img\perrobt.jpg");

            Image image1 = Image.FromFile(startupPath1);

            dgvInicio.Rows[this.row].Cells[this.cell].Value = image1;

            pImagen.BackgroundImage = image1;
            pImagen.BackgroundImageLayout = ImageLayout.Stretch;
            listaImagenes.RemoveAt(0);
            listaImagenes.Insert(this.row,@"Img\perrobt.jpg");
            pImagen.Visible = true;
          
        }

        private void btnImg2_Click(object sender, EventArgs e)
        {
            string startupPath2 = Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent.FullName, @"Img\perropg.jpg");

            Image image2 = Image.FromFile(startupPath2);

            dgvInicio.Rows[this.row].Cells[this.cell].Value = image2;

            pImagen.BackgroundImage = image2;
            pImagen.BackgroundImageLayout = ImageLayout.Stretch;
            listaImagenes.RemoveAt(row);
            listaImagenes.Insert(this.row, @"Img\perropg.jpg");
            pImagen.Visible = true;
        }

        private void btnImg3_Click(object sender, EventArgs e)
        {
            string startupPath3 = Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent.FullName, @"Img\perroptb.jpg");

            Image image3 = Image.FromFile(startupPath3);

            dgvInicio.Rows[this.row].Cells[this.cell].Value = image3;

            pImagen.BackgroundImage = image3;
            pImagen.BackgroundImageLayout = ImageLayout.Stretch;
            listaImagenes.RemoveAt(row);
            listaImagenes.Insert(this.row, @"Img\perroptb.jpg");
            pImagen.Visible = true;
        }

        private void btnImg4_Click(object sender, EventArgs e)
        {
            string startupPath4 = Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent.FullName, @"Img\perrortw.jpg");

            Image image4 = Image.FromFile(startupPath4);

            dgvInicio.Rows[this.row].Cells[this.cell].Value = image4;

            pImagen.BackgroundImage = image4;
            pImagen.BackgroundImageLayout = ImageLayout.Stretch;
            listaImagenes.RemoveAt(row);
            listaImagenes.Insert(row, @"Img\perrortw.jpg");
            pImagen.Visible = true;

        }

        private void btnRegistar_Click(object sender, EventArgs e)
        {
            string nombrePersona;
            string cedulaPersona;

            if (txtCedula.Text != "" && txtNombre.Text != "")
            {
                nombrePersona = txtNombre.Text;
                cedulaPersona = txtCedula.Text;


                DateTime fechaHoy= DateTime.UtcNow.Date;
                fechaHoy.ToString("dd-MM-yyyy");

                new CNDuenno().insertarDuenno (cedulaPersona, nombrePersona); 

                new CNDuennoMascota().insertarDuennoMascota(cedulaPersona, idMascota,this.fechaAdopcion);


                new CNMascotas().updtpestadoMascota(idMascota);

                txtCedula.Text = "";
                txtNombre.Text = "";

                panel2.Visible = false;
                dgvInicio.Enabled = true;

                dgvReporte2.DataSource = null;

                dgvReportes.DataSource = null;

                new CNDuenno().reporteDuenno(dgvReportes);


                dgvInicio.DataSource = null;

                dgvInicio.Columns.Clear();

                listaImagenes.Clear();

                listaImagenes.Add(@"Img\empty.jpg");

                new CNMascotas().mostrarMascotas(dgvInicio, listaImagenes);

                propiedadesInsert();




            }
            else {
                MessageBox.Show("Llene todos los espacios");
            }

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            panel2.Visible = false;
            dgvInicio.Enabled = true;
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dgvReportes_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

                if (e.ColumnIndex == 3)
                {

                    dgvReporte2.DataSource = null;

                    new CNMascotas().reporteMascota(dgvReporte2, dgvReportes.Rows[e.RowIndex].Cells[1].Value.ToString());

                }
            }
            catch {
                MessageBox.Show("Este area no cumple ninguna función");
            }
        }

        private void dgvReporte2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

     
    }
}

