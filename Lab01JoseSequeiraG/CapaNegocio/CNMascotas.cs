﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaDatos;

namespace CapaNegocio
{
    public class CNMascotas
    {


        public void mostrarMascotas(DataGridView dgv,List<string>listaImagenes) => new CDMetodos().MostrarMascotas(dgv,listaImagenes);

        public void insertarMascotas(string color, string tamanno, string sexo, int edad,DateTime fecha, string foto) => new CDMetodos().insertarMascotas(color,tamanno,sexo,edad,fecha,foto);

        public void modificarMascotas(int id,string color, string tamanno, string sexo, int edad, DateTime fecha, string foto) => new CDMetodos().modificarMascotas(id,color, tamanno, sexo, edad, fecha, foto);

        public void eliminarMascotas(int id) => new CDMetodos().eliminarMascostas(id);

        public void updtpestadoMascota(int id) => new CDMetodos().updtestadoMascostas(id);

        public void reporteMascota(DataGridView dgvReporte2,string id) => new CDMetodos().mostrarMascota(dgvReporte2,id);

    }
}
