﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaDatos;
namespace CapaNegocio
{
    public class CNDuenno
    {

        public void insertarDuenno(string cedulaPersona,string nombrePersona ) => new CDMetodos().insertarDuenno(cedulaPersona,nombrePersona);
        public void reporteDuenno(DataGridView dvgReporte) => new CDMetodos().mostrarDuenno(dvgReporte);
    }
}
