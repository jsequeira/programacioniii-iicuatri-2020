﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaDatos
{
    public class CDMetodos
    {




        public NpgsqlCommand cmd;
        public NpgsqlConnection conexionRetorno;
        Conexion conexion = new Conexion();


        public void mostrarMascota(DataGridView dgvReporte2,string id)
        {
            conexionRetorno = conexion.ConexionBD();

            DataTable dataTable = new DataTable();

            DataColumn edad = new DataColumn("Edad");
            edad.DataType = Type.GetType("System.Int32");

           
            dataTable.Columns.Add("Color");
            dataTable.Columns.Add("Tamanno");
            dataTable.Columns.Add("Sexo");
            dataTable.Columns.Add("Edad");
            dataTable.Columns.Add("Fecha_adopcion");

            NpgsqlCommand cmd = new NpgsqlCommand($"SELECT m.color,m.tamanno, m.sexo, m.edad, dm.fecha_adopcion FROM duennomascota dm, mascota m WHERE dm.identificador = m.identificador and dm.cedula='{id}'", conexionRetorno);

            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {       

                while (dr.Read())
                {

                    DataRow datarow1 = dataTable.NewRow();
                    datarow1["Color"] = dr.GetString(0);
                    datarow1["Tamanno"] = dr.GetString(1);
                    datarow1["Sexo"] = dr.GetString(2);
                    datarow1["Edad"] = dr.GetString(3);
                    datarow1["Fecha_adopcion"] = DateTime.Parse(dr.GetDate(4).ToString());

                    dataTable.Rows.Add(datarow1);
                    dgvReporte2.DataSource = dataTable;



                }

            }
            dgvReporte2.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;
            dgvReporte2.Columns[1].SortMode = DataGridViewColumnSortMode.NotSortable;
            dgvReporte2.Columns[2].SortMode = DataGridViewColumnSortMode.NotSortable;
            dgvReporte2.Columns[3].SortMode = DataGridViewColumnSortMode.NotSortable;
            dgvReporte2.Columns[4].SortMode = DataGridViewColumnSortMode.NotSortable;

        }


        public void mostrarDuenno(DataGridView dgvReporte)
        {
            conexionRetorno = conexion.ConexionBD();

            DataTable dataTable = new DataTable();

    

            dataTable.Columns.Add("Identificador");
            dataTable.Columns.Add("Cedula");
            dataTable.Columns.Add("Nombre");
            dataTable.Columns.Add("Accion");


            NpgsqlCommand cmd = new NpgsqlCommand("SELECT identificador, cedula, nombre FROM duenno ORDER BY identificador", conexionRetorno);

            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                
                int contadorN= 0;
               
                while (dr.Read())
                {


                    DataRow datarow1 = dataTable.NewRow();
                    datarow1["Identificador"] = dr.GetInt32(0).ToString();
                    datarow1["Cedula"] = dr.GetString(1).ToString();
                    datarow1["Nombre"] = dr.GetString(2).ToString();
                    datarow1["Accion"] = "******Mostrar******";

                    dataTable.Rows.Add(datarow1);

                

                    dgvReporte.DataSource = dataTable;


                    DataGridViewButtonCell bcModificar = new DataGridViewButtonCell();
                    
                    dgvReporte[3, contadorN] = bcModificar;

                 

                    contadorN = contadorN + 1;
                }
                dgvReporte.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;
                dgvReporte.Columns[1].SortMode = DataGridViewColumnSortMode.NotSortable;
                dgvReporte.Columns[2].SortMode = DataGridViewColumnSortMode.NotSortable;
                dgvReporte.Columns[3].SortMode = DataGridViewColumnSortMode.NotSortable;
            }
        }

          

        public void modificarMascotas(int id, string color, string tamanno, string sexo, int edad, DateTime fecha, string foto) {

         
                conexionRetorno = conexion.ConexionBD();
                cmd = new NpgsqlCommand($"UPDATE mascota SET  color='{color}', tamanno='{tamanno}', sexo='{sexo}', edad='{edad}', fecha_ingreso='{fecha}', foto='{foto}' WHERE identificador='{id}';", conexionRetorno);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Modificación Exitosa");              
        }

        public void insertarDuenno(string cedulaPersona, string nombrePersona) {
           
                conexionRetorno = conexion.ConexionBD();
                cmd = new NpgsqlCommand($"select *from duenno where cedula='{cedulaPersona}'", conexionRetorno);
                cmd.ExecuteNonQuery();

            NpgsqlDataReader dr = cmd.ExecuteReader();

            if (dr.Read())
            {
                Console.WriteLine("No pasa nada");
            }
            else
            {
                conexionRetorno = conexion.ConexionBD();
                cmd = new NpgsqlCommand($"INSERT INTO duenno (cedula,nombre) VALUES " + $"( '{cedulaPersona}', '{nombrePersona}')", conexionRetorno);
                cmd.ExecuteNonQuery();
               

            }
        }

        public void insertarDuennoMascota(string cedulaPersona,int idMascota,DateTime fecha)
        {
            conexionRetorno = conexion.ConexionBD();
            cmd = new NpgsqlCommand($"INSERT INTO duennomascota (cedula,identificador,fecha_adopcion) VALUES " + $"( '{cedulaPersona}', '{idMascota}', '{fecha}')", conexionRetorno);
            cmd.ExecuteNonQuery();
       


        }

        public void updtestadoMascostas(int id)
        {

            conexionRetorno = conexion.ConexionBD();
            cmd = new NpgsqlCommand($"update  mascota set estado='false' WHERE identificador='{id}'", conexionRetorno);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Modificación correcta");

        }
        public void eliminarMascostas(int id) {

            conexionRetorno = conexion.ConexionBD();
            cmd = new NpgsqlCommand($"Delete from  mascota WHERE identificador='{id}'", conexionRetorno);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Eliminación correcta");

        }



    
        public void insertarMascotas (string color , string tamanno, string sexo, int edad,DateTime fecha,string foto) {
            try
            {
                conexionRetorno = conexion.ConexionBD();
                cmd = new NpgsqlCommand("INSERT INTO mascota (color, tamanno, sexo,edad, estado,fecha_ingreso,foto) VALUES " + $"( '{color}', '{tamanno}'  ,  '{sexo}' , '{edad}','true','{ fecha}','{foto}' )", conexionRetorno);
                cmd.ExecuteNonQuery();
                MessageBox.Show("insertación Exitosa");
                
            }
            catch (NpgsqlException ex)
            {
               MessageBox.Show(ex.ToString());
            }
           }
    

        public void MostrarMascotas(DataGridView dgv,List<string> listaImagen)
        {

            

             conexionRetorno = conexion.ConexionBD();

            DataTable dataTable = new DataTable();

            DataColumn edad = new DataColumn("Edad");
            edad.DataType = Type.GetType("System.Int32");


            dataTable.Columns.Add("Adopcion");
            dataTable.Columns.Add("id");
            dataTable.Columns.Add("Color");
            dataTable.Columns.Add("Tamaño");
            dataTable.Columns.Add("Sexo");
            dataTable.Columns.Add(edad);
            dataTable.Columns.Add("fecha");
            dataTable.Columns.Add("Accion1");
            dataTable.Columns.Add("Accion2");
           


            DataRow datarow = dataTable.NewRow();
            datarow["id"] = "";
            datarow["Sexo"] = "";
            datarow["Edad"] = 0;
            datarow["Fecha"] = "";
            
            dataTable.Rows.Add(datarow);

            dgv.DataSource = dataTable;

          

                DataGridViewImageColumn vic = new DataGridViewImageColumn();
                vic.Name = "imagen";
                vic.HeaderText = "Imagen Column";
                vic.ValuesAreIcons = false;
                vic.ImageLayout = DataGridViewImageCellLayout.Stretch;
                vic.ImageLayout = DataGridViewImageCellLayout.Normal;
                vic.ImageLayout = DataGridViewImageCellLayout.Zoom;
                dgv.Columns.Add(vic);
           
          

            DataGridViewButtonCell button = new DataGridViewButtonCell();

          

            NpgsqlCommand cmd = new NpgsqlCommand("SELECT identificador, color, tamanno, sexo, edad,estado,fecha_ingreso,foto FROM mascota ORDER BY identificador", conexionRetorno);

            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                int contador = 1;
                int numeroFila = 1;
                while (dr.Read())
                {
                    

                    if (dr.GetBoolean(5))
                    {
                        DataRow datarow1 = dataTable.NewRow();
                        datarow1["id"] = dr.GetInt32(0).ToString();
                        datarow1["Edad"] = dr.GetInt32(4).ToString();
                        datarow1["Fecha"] = dr.GetDate(6).ToString();


                        dataTable.Rows.Add(datarow1);
                        dgv.DataSource = dataTable;

                      

                        if (dr.GetString(3).Equals("Macho"))
                        {
                            DataGridViewCheckBoxCell cbcSexo = new DataGridViewCheckBoxCell();
                            cbcSexo.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                            dgv[4, numeroFila] = cbcSexo;
                            dgv[4, numeroFila].Value = true;

                        }
                        else
                        {

                            DataGridViewCheckBoxCell cbcSexo1 = new DataGridViewCheckBoxCell();
                            cbcSexo1.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                            dgv[4, numeroFila] = cbcSexo1;
                            dgv[4, numeroFila].Value = false;

                        }

                        if (dr.GetString(7) == @"Img\perrobt.jpg")
                        {
                            listaImagen.Add(@"Img\perrobt.jpg");
                        }
                        else if (dr.GetString(7) == @"Img\perropg.jpg")
                        {
                            listaImagen.Add(@"Img\perropg.jpg");
                        }
                        else if (dr.GetString(7) == @"Img\perroptb.jpg")
                        {
                            listaImagen.Add(@"Img\perroptb.jpg");
                        }
                        else if (dr.GetString(7) == @"Img\perrortw.jpg")
                        {
                            listaImagen.Add(@"Img\perrortw.jpg");
                        }

                        else
                        {
                            listaImagen.Add(@"Img\empty.jpg");
                        }



                        DataGridViewComboBoxCell cbcColor = new DataGridViewComboBoxCell();
                        cbcColor.Items.AddRange(new string[] { "Blanco", "Negro", "Gris", "Café" });

                        DataGridViewComboBoxCell cbcTamanno = new DataGridViewComboBoxCell();
                        cbcTamanno.Items.AddRange(new string[] { "Grande", "Mediano", "Pequeño" });


                        DataGridViewButtonCell bcModificar = new DataGridViewButtonCell();
                        bcModificar.ToolTipText = ("Modificar Datos");
                        dgv[7, numeroFila].Value = "Modificar";

                        DataGridViewButtonCell bcEliminar = new DataGridViewButtonCell();
                        bcEliminar.ToolTipText = ("Eliminar  Datos");
                        dgv[8, numeroFila].Value = "Eliminar";


                        DataGridViewButtonCell bcfecha = new DataGridViewButtonCell();


                        DataGridViewButtonCell bcAdoptar = new DataGridViewButtonCell();
                        bcAdoptar.ToolTipText = ("Proceso de adopción de un perro");
                        dgv[0, numeroFila].Value = "Adoptar";





                        // string startupPath = Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent.FullName, @"Img\" + "labrador.ico");

                        // Bitmap img = new Bitmap(startupPath);

                        //Icon icon1 = new System.Drawing.Icon(startupPath,1,1);

                        // img = icon1.ToBitmap();

                        //Image image = Image.FromFile(startupPath);
                        //imagen.Image = image;


                        string startupPath = Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent.FullName, listaImagen[numeroFila]);

                        Image image = Image.FromFile(startupPath);



                        dgv[0, numeroFila] = bcAdoptar;
                        dgv[2, numeroFila] = cbcColor;
                        dgv[3, numeroFila] = cbcTamanno;
                        dgv[6, numeroFila] = bcfecha;

                        dgv[7, numeroFila] = bcModificar;
                        dgv[8, numeroFila] = bcEliminar;

                        dgv[9, numeroFila].Value = image;

                        for (int x1 = 0; x1 < cbcColor.Items.Count; x1++)
                        {
                            if (cbcColor.Items[x1].Equals(dr.GetString(1)))
                            {

                                dgv[2, numeroFila].Value = cbcColor.Items[x1];

                            }
                            for (int x2 = 0; x2 < cbcTamanno.Items.Count; x2++)
                            {
                                if (cbcTamanno.Items[x2].Equals(dr.GetString(2)))
                                {

                                    dgv[3, numeroFila].Value = cbcTamanno.Items[x2];

                                }
                            }

                        }

                        contador = contador + 1;
                        numeroFila = numeroFila + 1;
                    }




                    //.Add("Nombre:" + dr.GetString(0) + " edad:" + dr.GetInt32(1).ToString());

                }

                
            }
            }
        }
    }

