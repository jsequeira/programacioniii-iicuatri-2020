create database vinicultura ;
create schema vc;

CREATE TABLE vc.persona;
(
    id serial primary key,
    cedula text primary key,
    nombre text NOT NULL,
    apellidos text NOT NULL
);


CREATE TABLE vc.precio;
(
   id serial primary key,
   precio numeric NOT NULL;
);

  
CREATE TABLE vc.detalle_producto;
(
    id serial primary key,
    cedula text primary key,
    kilogramos numeric NOT NULL,
    tipo text NOT NULL,
    tamanno text NOT NULL,
    valor numeric NOT NULL,
    
);

INSERT INTO vc.precio(precio) VALUES (1550);

INSERT INTO vc.persona(cedula, nombre, apellidos) VALUES ('1234567', 'juan jose', ' Artoña Juarez');