﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class Principal : Form
    {
        public Principal()
        {
            InitializeComponent();
            this.CenterToScreen();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Insertar frmInsertar = new Insertar(this);
            frmInsertar.Show();
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            ConColaborador frmColaborador = new ConColaborador(this);
            frmColaborador.Show();

          
        }


        //select * from emp.colaborador
       // where fecha_ingreso BETWEEN '2011-01-25'::DATE AND '2030-01-25'::DATE

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            ConInglescs coingles = new ConInglescs (this);
            coingles.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Hide();
            ConFecha confecha = new ConFecha(this);
            confecha.Show();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            ConEdad conedad = new ConEdad(this);
            conedad.Show();
            this.Hide();
        }
    }
}
