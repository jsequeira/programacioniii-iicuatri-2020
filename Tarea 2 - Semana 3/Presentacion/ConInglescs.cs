﻿using Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class ConInglescs : Form
    {

        Principal principal;
        public ConInglescs(Principal principal)
        {

            InitializeComponent();
            this.CenterToScreen();
            this.principal = principal;
            cbIngles.Items.Add("A1");
            cbIngles.Items.Add("A2");
            cbIngles.Items.Add("B1");
            cbIngles.Items.Add("B2");
            cbIngles.Items.Add("C1");
            cbGenero.Items.Add('M');
            cbGenero.Items.Add('F');

            cbGenero.SelectedIndex = 0;
            cbIngles.SelectedIndex = 0;
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            List<string> colaboradores = new Ncolaboradores().obtenerColaboradorNingles(char.Parse(cbGenero.Text),cbIngles.Text);
            listView1.Items.Clear();
            for (int x = 0; x < colaboradores.Count(); x++) {

                listView1.Items.Add(colaboradores[x]);
            
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.principal.Show();
        }
    }
}
