﻿namespace Presentacion
{
    partial class ConFecha
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dtmF1 = new System.Windows.Forms.DateTimePicker();
            this.dtmF2 = new System.Windows.Forms.DateTimePicker();
            this.listView1 = new System.Windows.Forms.ListView();
            this.datos = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnBuscar = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // dtmF1
            // 
            this.dtmF1.CustomFormat = "dd/mm/yyyy";
            this.dtmF1.Location = new System.Drawing.Point(124, 62);
            this.dtmF1.Name = "dtmF1";
            this.dtmF1.Size = new System.Drawing.Size(200, 22);
            this.dtmF1.TabIndex = 0;
            this.dtmF1.Value = new System.DateTime(2020, 6, 23, 0, 0, 0, 0);
            this.dtmF1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // dtmF2
            // 
            this.dtmF2.CustomFormat = "dd/mm/yyyy";
            this.dtmF2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtmF2.Location = new System.Drawing.Point(444, 62);
            this.dtmF2.Name = "dtmF2";
            this.dtmF2.Size = new System.Drawing.Size(200, 22);
            this.dtmF2.TabIndex = 1;
            this.dtmF2.Value = new System.DateTime(2020, 6, 23, 0, 0, 0, 0);
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.datos});
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(12, 208);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(776, 189);
            this.listView1.TabIndex = 2;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // datos
            // 
            this.datos.Text = "Datos";
            this.datos.Width = 771;
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(124, 173);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(75, 23);
            this.btnBuscar.TabIndex = 3;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(339, 403);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(81, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Regresar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // ConFecha
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnBuscar);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.dtmF2);
            this.Controls.Add(this.dtmF1);
            this.Name = "ConFecha";
            this.Text = "ConFecha";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dtmF1;
        private System.Windows.Forms.DateTimePicker dtmF2;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader datos;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.Button button1;
    }
}