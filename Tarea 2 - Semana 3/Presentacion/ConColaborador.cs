﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Negocio;

namespace Presentacion
{
    public partial class ConColaborador : Form
    {

        Principal principal;
        int tiopOperacion = 0;
        public ConColaborador(Principal principal)
        {

            InitializeComponent();
            this.CenterToScreen();
            this.principal = principal;

            cargarDepartamentos();
            cbDepartamento.Items.Add("Buscar con cedula");

        }

        public void cargarDepartamentos()
        {

            List<string> departamentos = new Ndepartamento().Obtenerdepartamentos();

            for (int x = 0; x < departamentos.Count(); x++)
            {
                cbDepartamento.Items.Add(departamentos[x]);
            }
        }

  

        private void cbDepartamento_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!cbDepartamento.Text.Equals("")) {

                txtCedula.Enabled=false;
                this.tiopOperacion = 1;
            
            }

            if(cbDepartamento.Text.Equals("Buscar con cedula"))
            {

                txtCedula.Enabled = true;
                this.tiopOperacion = 2;
            }
        }










        private void txtCedula_TextChanged(object sender, EventArgs e)
        {
            



            }

        private void txtCedula_Click(object sender, EventArgs e)
        {
         
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {

            
            string dato = "";

            if (this.tiopOperacion == 1)
            {
                dato = cbDepartamento.Text;

            }

            else {

                dato = txtCedula.Text;

            }


            List<String> colaboradores = new Ncolaboradores().obtenerColaborador(this.tiopOperacion,dato);
            ListViewItem items = new ListViewItem();
            listView1.Items.Clear();

            for (int x =0;x<colaboradores.Count();x++) {

                
             

                items = listView1.Items.Add(colaboradores[x]);


                
            }

        }

        private void Regresar_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.principal.Show();
        }
    }
 }
