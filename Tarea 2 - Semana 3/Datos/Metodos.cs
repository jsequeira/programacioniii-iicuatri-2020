﻿using System;
using System.Collections.Generic;
using System.Text;
using Npgsql;
namespace Datos
{
   public  class Metodos
    {

        public NpgsqlCommand cmd;
        public NpgsqlConnection conexionRetorno;
        Conexion conexion = new Conexion();


        public List<string> Obtenerdepartamentos()
        {

            conexionRetorno = conexion.ConexionBD();

            List<string> departamentos = new List<string>();
            NpgsqlCommand cmd = new NpgsqlCommand($"SELECT  nombre FROM emp.departamento ", conexionRetorno);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    departamentos.Add(  dr.GetString(0));

                }

            }
            return departamentos;
        }


        public List<string> ObtenerPuesto()
        {

            conexionRetorno = conexion.ConexionBD();

            List<string> puestos = new List<string>();
            NpgsqlCommand cmd = new NpgsqlCommand($"SELECT nombre FROM emp.puesto ", conexionRetorno);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    puestos.Add(dr.GetString(0));

                }

            }
            return puestos;
        }

    

        public void insertaColaborador(string cedula, string nombre, char genero, string idDepartamento, string fechaNacimiento, int edad, string fechaIngreso, string nivelIngles, int idPuesto)
        {

            conexionRetorno = conexion.ConexionBD();
            cmd = new NpgsqlCommand("INSERT INTO emp.colaborador (cedula, nombre, genero,id_departamento, fecha_nacimiento,edad,fecha_ingreso,nivel_ingles,id_puesto) VALUES "+ $"( '{cedula}', '{nombre}'  ,  '{genero}' , '{idDepartamento}' ,'{ fechaNacimiento}','{edad}','{fechaIngreso}','{nivelIngles}','{idPuesto}'  )" , conexionRetorno);
            cmd.ExecuteNonQuery();


        }

        public List<string> obtenerColaborador(int tipoOperacion,string dato) {
            string tabla = "";

            if (tipoOperacion == 1)
            {

        
                tabla = "de.nombre=";

            }
            else {


                tabla = "co.cedula=";

            }



            conexionRetorno = conexion.ConexionBD();

            List<string> colaboradores = new List<string>();
            NpgsqlCommand cmd = new NpgsqlCommand($"SELECT co.cedula, co. nombre, de.nombre,co.Fecha_nacimiento,co.Edad,co.Fecha_ingreso,pu.abreviacion " +
                $"FROM emp.Departamento de, emp.Puesto pu, emp.Colaborador co " +
                $"WHERE co.id_departamento = de.id_departamento and pu.id_puesto = co.id_puesto and {tabla} '{dato}'", conexionRetorno);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    colaboradores.Add("Cedula:" + dr.GetString(0) + " Nombre:" + dr.GetString(1) + " Depar:" + dr.GetString(2) + " Fe_naci:" + dr.GetDate(3).ToString() + " Edad:" + dr.GetInt32(4) + " Fec_ingr:" + dr.GetDate(5).ToString() + " Puesto:" + dr.GetString(6));

                }

            }
            
            return colaboradores;

        }



        public List<string> obtenerColaboradorNingles(char genero, string dato)
        {


            conexionRetorno = conexion.ConexionBD();

            List<string> colaboradores = new List<string>();
            NpgsqlCommand cmd = new NpgsqlCommand($"SELECT co.nombre,co.genero, nivel_ingles,de.nombre,pu.nombre " +
                $"FROM emp.Departamento de, emp.Puesto pu, emp.Colaborador co " +
                $"WHERE co.id_departamento = de.id_departamento and pu.id_puesto = co.id_puesto and co.genero='{genero}' and co.nivel_ingles= '{dato}'", conexionRetorno);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    colaboradores.Add("Nombre:" + dr.GetString(0) + " Genero:" + dr.GetString(1) + " Nivel ingles:" + dr.GetString(2) + " nombre_depar:" + dr.GetString(3)+ "nombre_pues:" + dr.GetString(4));

                }

            }
            return colaboradores;

        }

        public List<string> obtenerColaboradorfecha(string fechaUno, string fechaDos)
        {
            //select * from emp.colaborador
            // where fecha_ingreso BETWEEN '2011-01-25'::DATE AND '2030-01-25'::DATE

            conexionRetorno = conexion.ConexionBD();

            List<string> colaboradores = new List<string>();
            NpgsqlCommand cmd = new NpgsqlCommand($"SELECT nombre,fecha_ingreso FROM emp.Colaborador  WHERE fecha_ingreso BETWEEN '{fechaUno}'::DATE AND '{fechaDos}'::DATE",conexionRetorno);

            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    colaboradores.Add("Nombre:" + dr.GetString(0) + " Fecha Ingreso:" + dr.GetDate(1).ToString());

                }

            }
            return colaboradores;

        }

        public List<string> obtenerColaboradoredad(int edadUno, int edadDos)
        {
            //select * from emp.colaborador
            // where fecha_ingreso BETWEEN '2011-01-25'::DATE AND '2030-01-25'::DATE

            conexionRetorno = conexion.ConexionBD();

            List<string> colaboradores = new List<string>();
            NpgsqlCommand cmd = new NpgsqlCommand($"SELECT nombre , edad FROM emp.colaborador WHERE edad Between {edadUno} And {edadDos};", conexionRetorno);

            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    colaboradores.Add("Nombre:" + dr.GetString(0) + " edad:" + dr.GetInt32(1).ToString());

                }

            }
            return colaboradores;

        }


    }
}
