﻿using System;
using System.Collections.Generic;
using System.Text;
using Datos;
namespace Negocio
{
    public class Ncolaboradores
    {

        public void insertaColaborador(string cedula, string nombre, char genero, string idDepartamento, string fechaNacimiento, int edad, string fechaIngreso, string Ningles, int idPuesto) {

            new Metodos().insertaColaborador(cedula, nombre, genero, idDepartamento, fechaNacimiento, edad, fechaIngreso, Ningles, idPuesto);


        }

        public List<string> obtenerColaborador(int tipoOperacion,string dato)=>new Metodos().obtenerColaborador(tipoOperacion,dato);

     

        public List<string> obtenerColaboradorNingles(char genero, string dato) => new Metodos().obtenerColaboradorNingles(genero, dato);

        public List<string> obtenerColaboradorfecha(string fechaUno, string fechaDos) => new Metodos().obtenerColaboradorfecha(fechaUno, fechaDos);

        public List<string> obtenerColaboradoredad(int edadUno, int edadDos) => new Metodos().obtenerColaboradoredad(edadUno, edadDos);

    }
}
