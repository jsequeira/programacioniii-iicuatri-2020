﻿using System;
using System.Collections;

namespace Ejercicio_5
{
    class Program
    {
        static void Main(string[] args)
        {
            ArrayList registroChoferes = new ArrayList();




            int numero = 0;

            while (numero != 3)
            {

                int cedula = 0;

                String strCamion = "";

                String strconfirmacion = "";
                Console.Write(" ");
                Console.Write("\n***Menu***\n**Digite  una opcion**\n1-Realizar Registro \n2-Consultar\n3-Salir");
                Console.Write(" ");
                try
                {
                    numero = int.Parse(Console.ReadLine());

                    switch (numero)
                    {

                        case 1:


                            Console.Write("\n***Realizar Registro***");

                            Console.Write("\n**Inserte el nombre completo del chofer**");

                            String nombreCom = Console.ReadLine();


                            Console.Write("\n**Inserte la cedula del chofer**");

                            int contador3 = 0;
                            while (contador3 == 0)
                            {
                                try
                                {
                                    cedula = int.Parse(Console.ReadLine());

                                    contador3 = 1;
                                }
                                catch
                                {
                                    Console.Write("\n** A digitado letras en vez de numeros,¡intente de nuevo!**");
                                }
                            }



                            int contador1 = 0;

                            int tipoCamion = 0;

                            while (contador1 == 0)
                            {

                                Console.Write("\n**Digite el tipo de camion**" + "\n1-Carga pesada" + "\n2-Carga ancha" + "\n3-Carga liviana" + "\n4-Carga media" + "\n5-Carga jumbo");

                                try
                                {
                                    tipoCamion = int.Parse(Console.ReadLine());

                                    if (tipoCamion == 1)
                                    {
                                        strCamion = "Carga pesada";
                                        contador1 = 1;
                                    }
                                    else if (tipoCamion == 2)
                                    {
                                        strCamion = "Carga ancha";
                                        contador1 = 1;
                                    }
                                    else if (tipoCamion == 3)
                                    {
                                        strCamion = "Carga liviana";
                                        contador1 = 1;
                                    }
                                    else if (tipoCamion == 4)
                                    {
                                        strCamion = "Carga media";
                                        contador1 = 1;
                                    }

                                    else if (tipoCamion == 5)
                                    {
                                        strCamion = "Carga jumbo";
                                        contador1 = 1;
                                    }

                                    else
                                    {
                                        Console.Write("\n¡Digite un numero!");
                                    }
                                }
                                catch
                                {

                                    Console.Write("\n**$$Error#767-A$$ A digitado letras en vez de numeros,¡intente de nuevo!**");

                                }
                            }

                            int contador = 0;

                            while (contador == 0)
                            {

                                Console.Write("\n**¿La carga es peligrosa? Digite (s/Sí - n/NO)**");

                                String confirmacion = Console.ReadLine();

                                if (confirmacion.Equals("s") || confirmacion.Equals("S"))
                                {
                                    strconfirmacion = "Sí";
                                    contador = 1;
                                }

                                else if (confirmacion.Equals("n") || confirmacion.Equals("N"))
                                {
                                    strconfirmacion = "No";
                                    contador = 1;
                                }
                                else
                                {
                                    Console.Write("\n¡Digite (s/Sí - n/NO)!");
                                }

                            }

                            Registro registro1 = new Registro(nombreCom, cedula, strCamion, strconfirmacion);

                            registroChoferes.Add(registro1);


                            break;

                        case 2:
                            Console.Write(" ");
                            Console.Write("\n***Consultar****");
                            Console.Write("\n**Digite el numero de cedula**");

                            int contador4 = 0;
                            while (contador4 == 0)
                            {
                                try
                                {
                                    cedula = int.Parse(Console.ReadLine());

                                    contador4 = 1;


                                    foreach (Registro reg in registroChoferes)
                                    {
                                        if (reg.getCedula() == cedula)
                                        {
                                            Console.Write(" ");
                                            Console.Write("\n***Registro del chofer***");
                                            Console.Write(" ");
                                            Console.Write(reg.gettodaInformacion());
                                            Console.Write(" ");

                                        }
                                        else
                                        {
                                            Console.Write(" ");
                                            Console.Write("\n¡Registro no encontrado!");
                                        }
                                        Console.Write(" ");
                                        contador4 = 1;

                                    }
                                }
                                catch
                                {
                                    Console.Write("\n** A digitado letras en vez de numeros,¡intente de nuevo!**");
                                }
                            }




                            break;

                        case 3:

                            Console.Write("\n*****************Fin********************");
                            break;

                        default:

                            { Console.Write("\n¡Digite correctamente!"); }
                            break;

                    }
                }
                catch { Console.Write("\n¡Digite correctamente!"); }

            }
        }
    }
    
    class Registro
    {
        String Nombre;
        int Cedula;
        String tipoCamion;
        String tipoCarga;


        public Registro() { }

        public Registro(String Nombre, int Cedula, String tipoCamion, String tipoCarga)
        {
            this.Nombre = Nombre;
            this.Cedula = Cedula;
            this.tipoCamion = tipoCamion;
            this.tipoCarga = tipoCarga;
        }

        public String gettodaInformacion()
        {
            return "\nNombre:" + this.Nombre + "\nCedula:" + this.Cedula + "\nTipo camion:" + this.tipoCamion + "\ntipo Carga/¿peligrosa?:" + this.tipoCarga;
        }
        public int getCedula()
        {

            return this.Cedula;
        }


    }
}
